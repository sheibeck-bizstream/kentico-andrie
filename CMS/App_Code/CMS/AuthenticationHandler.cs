﻿using System.Data;

using CMS.Base;
using CMS.Membership;
using CMS.DataEngine;
using System.Web;
using CMS.Helpers;
using System;

[AuthenticationHandler]
public partial class CMSModuleLoader
{
    /// <summary>
    /// Custom attribute class.
    /// </summary>
    private class AuthenticationHandler : CMSLoaderAttribute
    {
        /// <summary>
        /// Called automatically when the application starts
        /// </summary>
        public override void Init()
        {
            // Assigns a handler to the SecurityEvents.Authenticate.Execute event
            // This event occurs when users attempt to log in on the website
            RequestEvents.Begin.Execute += Begin_Execute;            
            SecurityEvents.Authenticate.Execute += OnAuthentication;
            SecurityEvents.SignOut.After += OnAfterSignOut;
        }

        /// <summary>
        /// Handles the Execute event of the Begin control.
        /// If the user is not logged in yet
        /// then get an auth cookie we can use for viewing without firing the "login" popup window
        /// until the user actually logs in.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        void Begin_Execute(object sender, EventArgs e)
        {
            string userinfo;
            byte[] authinfo;
            string hashValue;

            if (String.IsNullOrEmpty(CookieHelper.GetValue(".APMADVAUTHTOKEN")))
            {
                //authenticate our rest user that has some advanced permissions. We mostly use this for
                //pulling information out of objects for read only purposes when our current user doesn't
                //have permissin because kentico won't let us give it to them
                userinfo = String.Format("{0}:{1}", "RestAPI", "umbAjt7hsAw9");
                authinfo = System.Text.Encoding.UTF8.GetBytes(userinfo);
                hashValue = System.Convert.ToBase64String(authinfo);
                CookieHelper.SetValue(".APMADVAUTHTOKEN", hashValue, "/", DateTime.Now.AddDays(30), false);
            }
        }        

        /// <summary>
        /// Called when [authentication].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="AuthenticationEventArgs"/> instance containing the event data.</param>
        private void OnAuthentication(object sender, AuthenticationEventArgs e)
        {
            string userinfo;
            byte[] authinfo;
            string hashValue;

            // If the user is authenticated drop a 64 bit encoded hash to a cookie
            // for doing REST API calls
            if (e.User != null)
            {
                userinfo = String.Format("{0}:{1}", e.UserName, e.Password);
                authinfo = System.Text.Encoding.UTF8.GetBytes(userinfo);
                hashValue = System.Convert.ToBase64String(authinfo);
                CookieHelper.SetValue(".APMBASICAUTHTOKEN", hashValue, "/", DateTime.Now.AddDays(30), false);
                CookieHelper.SetValue(".APMCURRENTUSER", e.User.FullName, "/", DateTime.Now.AddDays(30), false);
            }            
        }

        private void OnAfterSignOut(object sender, SignOutEventArgs e)
        {
            // Kill our 64 bit encoded hash to a cookie   
            CookieHelper.Remove(".APMBASICAUTHTOKEN");
            CookieHelper.Remove(".APMCURRENTUSER");
        }
    }
}