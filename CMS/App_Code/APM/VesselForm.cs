﻿using CMS.CustomTables;
using CMS.DataEngine;
using CMS.ExtendedControls;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace APM
{
    /// <summary>
    /// Vessel Form
    /// </summary>
    public static class VesselForm
    {
        /// <summary>
        /// Exports the specified form unique identifier.
        /// </summary>
        /// <param name="FormGuid">The form unique identifier.</param>
        /// <param name="TripNumber">The trip number.</param>
        /// <param name="Date">The date.</param>
        /// <param name="ItemGuid">The item unique identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Report data not found.
        /// or
        /// </exception>
        public static List<string> Export(string FormGuid, string TripNumber, string Date, string ItemGuid)
        {
            try
            {
                // get the form
                var dsForm = CustomTableItemProvider.GetItems("APM.Form").WhereEquals("ItemGUID", FormGuid).FirstOrDefault();

                //get these from Kentico custom settings
                string FormPath = SettingsKeyInfoProvider.GetStringValue(new SettingsKeyName("APM_FormPath"));
                string FormPathOutput = SettingsKeyInfoProvider.GetStringValue(new SettingsKeyName("APM_FormOutputPath"));

                // find the folder tied to the report. If we don't have an explicit folder name
                // then use the name of the file.
                var folderName = ValidationHelper.GetString(dsForm["FormFolderName"], string.Empty);
               
                if (String.IsNullOrEmpty(folderName)) {
                    folderName = ValidationHelper.GetString(dsForm["FormName"], string.Empty);
                }

                // make sure the foldername is valid windows name                
                foreach (char c in Path.GetInvalidFileNameChars())
                {
                    folderName = folderName.Replace(c, '_');
                }

                // add the trailing slash to the output path if necessary
                if (!FormPathOutput.Last<char>().Equals('/'))
                {
                    FormPathOutput += "/";
                }

                FormPathOutput += folderName;

                //get the full physical path to the directories
                string filePath = FileHelper.GetFullFolderPhysicalPath(FormPath);
                string filePathOutput = FileHelper.GetFullFolderPhysicalPath(FormPathOutput);                

                List<string> missingColumnList = new List<string>();
               
                //create directories if they don't already exist
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                
                if (!Directory.Exists(filePathOutput))
                {
                    Directory.CreateDirectory(filePathOutput);
                }

                //if they didn't specify a date then assume today
                if (Date != String.Empty)
                    Date = ValidationHelper.GetDateTime(Date, DateTime.Now).ToString();
                
                string fileName = String.Format("{0}{1}", filePath, ValidationHelper.GetString(dsForm["FormFileName"], String.Empty));
                                
                if (!File.Exists(fileName))
                {
                    string msg = String.Format("{0} does not exist.", Path.GetFileName(fileName));
                    throw new Exception(msg);
                }
                
                //create a new file
                FileInfo fi = new FileInfo(fileName);
                ExcelPackage pck = new ExcelPackage(fi); //need this to be global so we'll assign here but overwrite later
                            
                // run a kentico query that will contain our data
                string queryName = String.Format("APM.Form.Get{0}", Path.GetFileNameWithoutExtension(fi.FullName).Replace("_", ""));
                var query = new DataQuery(queryName);

                //setup the parameters we need for the data
                QueryDataParameters parms = new QueryDataParameters();
                foreach (string parm in ValidationHelper.GetString(dsForm["FormParameters"], String.Empty).Split('|'))
                {
                    switch (parm)
                    {
                        case "ItemGuid":
                            parms.Add(new DataParameter("@ItemGuid", ItemGuid));                                
                            break;

                        case "TripNumber":
                            parms.Add(new DataParameter("@TripNumber", TripNumber));                                
                            break;

                        case "ItemDateTime":
                            parms.Add(new DataParameter("@ItemDateTime", Date));                                
                            break;
                    }
                }
                query.Parameters = parms;
                DataSet dsData = query.Result;
                List<string> VesselForms = new List<string>();
                
                //process the data
                if (!DataHelper.DataSourceIsEmpty(dsData))
                {
                    //determine if we have vertical data (like a log sheet)
                    bool isVertical = ValidationHelper.GetBoolean(dsForm["FormIsVertical"], false);

                    // process horizontal data if we dont have vertical data
                    if (!isVertical)
                    {
                        //allow for multiple datasets so we can kickout multiple forms in one pass
                        // for example: multiple discharges per load.
                        foreach (DataTable dt in dsData.Tables)
                        {
                            //allow for multiple rows
                            foreach (DataRow data in dt.Rows)
                            {
                                //make a copy of the excel sheet for each row
                                pck = new ExcelPackage(fi);

                                //Create the worksheet
                                var ws = pck.Workbook.Worksheets[1];

                                //Find the named range
                                // Named ranges MUST be scoped to the WORKBOOK.
                                ExcelNamedRangeCollection nrc = pck.Workbook.Names;                                

                                foreach (ExcelNamedRange nr in nrc)
                                {
                                    //go get the value we need to fill this named range
                                    string namedRangeValue = GetNamedValue(missingColumnList, data, nr, Date, TripNumber);

                                    //Set the value of each cell in the named range. 
                                    for (int rowIndex = nr.Start.Row; rowIndex <= nr.End.Row; rowIndex++)
                                    {
                                       
                                        for (int columnIndex = nr.Start.Column; columnIndex <= nr.End.Column; columnIndex++)
                                        {
                                            double value;
                                            if (double.TryParse(namedRangeValue, System.Globalization.NumberStyles.Float, CMS.Base.CMSActionContext.CurrentCulture, out value))
                                            {
                                                ws.Cells[rowIndex, columnIndex].Value = value;
                                            }
                                            else
                                            {
                                                ws.Cells[rowIndex, columnIndex].Value = namedRangeValue;
                                            }
                                        }
                                    }
                                }

                                VesselForms.Add(SaveVesselForm(filePathOutput, folderName, TripNumber, fi, pck));

                                pck.Dispose();
                            }


                        }
                    }
                    else //process vertical data (i.e. logs)
                    {
                        //multiple datasets
                        foreach (DataTable dt in dsData.Tables)
                        {
                            //make a copy of the excel sheet for each dataset
                            pck = new ExcelPackage(fi);

                            //Create the worksheet
                            var ws = pck.Workbook.Worksheets[1];

                            //Find the named range
                            ExcelNamedRangeCollection nrc = pck.Workbook.Names;

                            // track what row of vertical data we're on
                            int lastVerticalRow = 0;

                            foreach (DataRow data in dt.Rows)
                            {
                                // track the current vertical row we are going to populate
                                lastVerticalRow++;

                                //process any named ranges, for any horizontal data that exists on the sheet
                                //this data will get set each time. So this data will exist in each row of the vertical data
                                // for example:
                                //   row1: tripnumber, vesselnumber, logdataA1, logdataB1
                                //   row2: tripnumber, vesselnumber, logdataA2, logdataB2
                                // 
                                //  tripnumber and vesselnumber values will get set every time we process a row. This isn't super
                                //  effiecient, but is OK since these values should be the same on every row
                                foreach (ExcelNamedRange nr in nrc)
                                {
                                    //go get the value we need to fill this named range
                                    string namedRangeValue = GetNamedValue(missingColumnList, data, nr, Date, TripNumber);

                                    //Set the value of each cell in the named range. 
                                    for (int rowIndex = nr.Start.Row; rowIndex <= nr.End.Row; rowIndex++)
                                    {
                                        // if we find a NamedRange that starts with "Vertical_" then we know our vertical data starts here
                                        //  so we'll need to find the excel coordinates for the cell we're going to populate.
                                        //  We're looking for V#_.
                                        //      V = vertical data
                                        //      # = the offset that our row data population needs (0 = no offset, 1 = data starts 1 additional row after cell), etc
                                        int verticalRowIndex = rowIndex;
                                        Match m = RegexHelper.GetRegex("V[0-9]?_", true).Match(nr.Name);
                                        if (m.Length > 0)
                                        {
                                            //get offset
                                            int offset = ValidationHelper.GetInteger(m.Value.Substring(1,1), 0);
                                            verticalRowIndex = rowIndex + lastVerticalRow + offset;
                                        }

                                        for (int columnIndex = nr.Start.Column; columnIndex <= nr.End.Column; columnIndex++)
                                        {
                                            double value;
                                            if (double.TryParse(namedRangeValue, System.Globalization.NumberStyles.Float, CMS.Base.CMSActionContext.CurrentCulture, out value))
                                            {
                                                ws.Cells[verticalRowIndex, columnIndex].Value = value;
                                            }
                                            else
                                            {
                                                ws.Cells[verticalRowIndex, columnIndex].Value = namedRangeValue;
                                            }
                                        }
                                    }


                                }
                            }

                            // Save the form
                            VesselForms.Add(SaveVesselForm(filePathOutput, folderName, TripNumber, fi, pck));
                            pck.Dispose();
                        }
                    }
                }
                else
                {
                    var ex = new Exception("The file named " + fi.Name + " was generated, but the report was missing one or more pieces the data.");
                    CMS.EventLog.EventLogProvider.LogException("VesselForm", "Export", ex);
                    
                    VesselForms.Add(SaveVesselForm(filePathOutput, folderName, TripNumber, fi, pck));                        
                }

                //Log a warning of any missing fields
                if (missingColumnList.Count > 0)
                {                  
                    CMS.EventLog.EventLogProvider.LogWarning("VesselForm", "FORM WARNING", new Exception("Missing columns in Excel Form"), SiteContext.CurrentSiteID
                                                , String.Format("The following columns could not be found in form {0}: {1}", fileName, missingColumnList.Join(",")));

                }
                return VesselForms;
            }
            catch
            {
                throw; //throw any exception up to the calling method
            }
        }

        /// <summary>
        /// Gets the named value.
        /// </summary>
        /// <param name="missingColumnList">The missing column list.</param>
        /// <param name="data">The data.</param>
        /// <param name="nr">The nr.</param>
        /// <returns></returns>
        private static string GetNamedValue(List<string> missingColumnList, DataRow data, ExcelNamedRange nr, string Date = null, string TripNumber = null)
        {
            string namedRangeValue;
            string dataColumn = nr.Name;

            //see if this is a vertical column
            Match m = RegexHelper.GetRegex("V[0-9]?_", true).Match(nr.Name);
            if (m.Length > 0)
            {
                //remove the special characters from vertical named range so we get the right row
                dataColumn = dataColumn.Replace(m.Value, String.Empty);
            }
            
            //get the data
            switch (dataColumn)
            {
                case "Date":
                case "Date1":
                case "CurrentDate":
                case "CurrentDate1":
                    string curDate = String.Format("{0:MM/dd/yyyy}", DateTime.Now);
                    namedRangeValue = curDate;
                    break;
                case "Time":
                case "Time1":
                case "CurrentTime":
                case "CurrentTime1":
                    string curTime = String.Format("{0:HH:mm}", DateTime.Now);
                    namedRangeValue = curTime;
                    break;
                case "DateTime":
                case "TimeDate":
                case "DateTime1":
                case "CurrentDateTime":
                case "CurrentDateTime1":
                    string curDateTime = String.Format("{0:MM/dd/yyyy HH:mm}", DateTime.Now);
                    namedRangeValue = curDateTime;
                    break;
                case "CrewMaster":
                case "CrewMaster1":
                    string currentCaptain = APMHelper.GetCrewMemberNameForRole("Captain", 1, Date, TripNumber);
                    namedRangeValue = currentCaptain;
                    break;
                case "CrewEngineer":
                case "CrewChiefEngineer":
                    string currentChiefEngineer = APMHelper.GetCrewMemberNameForRole("Chief Engineer", 1, Date, TripNumber);
                    namedRangeValue = currentChiefEngineer;
                    break;
                case "CrewAssistantEngineer":
                case "CrewAssistantEningeer":
                    string currentAssistantEngineer = APMHelper.GetCrewMemberNameForRole("Assistant Engineer", 1, Date, TripNumber);
                    namedRangeValue = currentAssistantEngineer;
                    break;
                case "CrewOrdinarySeaman":
                    string currentOrdinarySeamen = APMHelper.GetCrewMemberNameForRole("Ordinary Seamen", 1, Date, TripNumber);
                    namedRangeValue = currentOrdinarySeamen;
                    break;
                case "CrewMember":
                case "VesselCrewName":
                    string currentUser = CMS.Base.CMSActionContext.CurrentUser.FullName;
                    namedRangeValue = currentUser;
                    break;
                case "CrewMate":
                case "Mate":
                    string currentMate = APMHelper.GetCrewMemberNameForRole("Mate", 1, Date, TripNumber);
                    namedRangeValue = currentMate;
                    break;
                case "CrewMate1":
                    string mate2 = APMHelper.GetCrewMemberNameForRole("Mate", 2, Date, TripNumber);
                    namedRangeValue = mate2;
                    break;
                case "CrewAbleSeaman":
                    string able1 = APMHelper.GetCrewMemberNameForRole("Able Body Seamen", 1, Date, TripNumber);
                    namedRangeValue = able1;
                    break;
                case "CrewAbleSeaman1":
                    string able2 = APMHelper.GetCrewMemberNameForRole("Able Body Seamen", 2, Date, TripNumber);
                    namedRangeValue = able2;
                    break;
                case "CrewExtra":
                    string extra1 = APMHelper.GetCrewMemberNameForRole("Extra", 1, Date, TripNumber);
                    namedRangeValue = extra1;
                    break;
                case "CrewExtra1":
                    string extra2 = APMHelper.GetCrewMemberNameForRole("Extra", 2, Date, TripNumber);
                    namedRangeValue = extra2;
                    break;
                case "CrewABPIC":
                    string abpic = APMHelper.GetCrewMemberNameForRole("AB-PIC", 1, Date, TripNumber);
                    namedRangeValue = abpic;
                    break;
                case "VesselCrewRole":
                case "CrewMemberRole":
                    string crewRole = String.Empty;
                    UserInfo ui = UserInfoProvider.GetUserInfo(CMS.Base.CMSActionContext.CurrentUser.UserID);
                    DataTable ut = UserInfoProvider.GetUserRoles(ui, "RoleName like 'APM_%'", "", 1, "*");
                    if (ut.Rows.Count > 0)
                    {
                        RoleInfo ri = RoleInfoProvider.GetRoleInfo(ValidationHelper.GetString(ut.Rows[0]["RoleName"], string.Empty), CMS.Base.CMSActionContext.CurrentSite.SiteName);
                        crewRole = ri.DisplayName.Replace("* ", string.Empty);
                    }
                    namedRangeValue = crewRole;
                    break;
                default:
                    //if we can't find a value in the dataset then log an error but keep on going
                    try
                    {
                        namedRangeValue = ValidationHelper.GetString(data[dataColumn], String.Empty);
                    }
                    catch (Exception ex)
                    {
                        missingColumnList.Add(dataColumn);
                        namedRangeValue = String.Empty;
                    }
                    break;
            }
            return namedRangeValue;
        }

        /// <summary>
        /// Saves the vessel form.
        /// </summary>
        /// <param name="filePathOutput">The file path output.</param>
        /// <param name="tripNumber">The trip number.</param>
        /// <param name="fi">The file info.</param>
        /// <param name="pck">The excel package.</param>
        /// <returns></returns>
        private static string SaveVesselForm(string filePathOutput, string folderName, string tripNumber, FileInfo fi, ExcelPackage pck)
        {
            //save the file out as a copy
            string sDate = DateTime.Now.ToString("MMddyyyyHHmm");
            FileInfo newFile = new FileInfo(String.Format("{0}{1}_{2}_{3}{4}", filePathOutput, Path.GetFileNameWithoutExtension(fi.FullName), tripNumber, sDate, Path.GetExtension(fi.FullName)));

            int fileCount = 1;
            while (File.Exists(newFile.FullName))
            {
                fileCount++;
                newFile = new FileInfo(String.Format("{0}{1}_{2}_{3}_{4}{5}", filePathOutput, Path.GetFileNameWithoutExtension(fi.FullName), tripNumber, sDate, fileCount.ToString(), Path.GetExtension(fi.FullName)));
            }

            pck.SaveAs(newFile);

            string UNCPath = SettingsKeyInfoProvider.GetStringValue(new SettingsKeyName("APM_FormOutputUNCPath"));

            // add the trailing slash to the output path if necessary
            if (!UNCPath.Last<char>().Equals('/'))
            {
                UNCPath += "/";
            }

            UNCPath += folderName + "/";

            return String.Format(@"{0}{1}", UNCPath, newFile.Name); //we'll need to convert this to a UNC path so the user opens the file from the server
        }
    }
}
