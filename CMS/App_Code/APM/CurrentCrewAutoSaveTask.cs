﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using CMS;
using CMS.CustomTables;
using CMS.EventLog;
using CMS.Scheduler;
using CMS.DataEngine;
using CMS.SiteProvider;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System.Text.RegularExpressions;
using System.Data.Linq.Mapping;


/// <summary>
/// Summary description for CurrentCrewAutoSaveTask
/// </summary>
[assembly: RegisterCustomClass("APM.CurrentCrewAutoSaveTask", typeof(APM.CurrentCrewAutoSaveTask))]
namespace APM
{
    /// <summary>
    /// Schedule task to auto save the crew on a daily basis
    /// </summary>
    public class CurrentCrewAutoSaveTask : ITask
    {       

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <param name="ti">Info object representing the scheduled task</param>
        public string Execute(TaskInfo ti)
        {
            DateTime currentDate = DateTime.Now;
            List<string> crewList = CurrentCrew.AutoSave(currentDate);
            return String.Format("Autosaved {0} people to current crew for {1}.", crewList.Count(), currentDate.ToShortDateString());
        }     
    }
}