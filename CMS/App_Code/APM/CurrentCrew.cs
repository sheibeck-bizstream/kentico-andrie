﻿using CMS.CustomTables;
using CMS.DataEngine;
using CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


/// <summary>
/// Current Crew related operations
/// </summary>
public static class CurrentCrew
{
    /// <summary>
    /// Automates setting up the crew on a daily basis based on the previous crew entry.
    /// </summary>
    /// <param name="date">The dt.</param>
    /// <returns></returns>
    /// <exception cref="Exception">No active trip was found. Please make sure this boat is properly setup and is on an active trip.</exception>
    public static List<string> AutoSave(DateTime date)
    {       
        List<string> result = new List<string>();
        List<APM.CrewMember> crew = APMHelper.GetCrewForDate(date.ToString());
        Dictionary<Guid, string> userIds = new Dictionary<Guid, string>();

        try {
            //if there is no crew specified for the day then go get the last crew
            // and add them to today
            if (crew.Count() == 0)
            {
                DataSet mostRecentCrew = new DataQuery("APM.CurrentCrew.ListMostRecentCrew")
                    .OrderBy("APM_CurrentCrew.CurrentCrewRole")
                    .Execute();

                if (!DataHelper.DataSourceIsEmpty(mostRecentCrew))
                {
                    // get the current active trip or the most recent active trip.
                    DataSet currentTrip = new DataQuery("APM.TripInformation.selectall")
                        .WhereEquals("IsCurrent", 1)                                    
                        .Execute();

                    DataSet vesselInformation = new DataQuery("APM.VesselInformation.selectall")
                        .OrderBy("VesselBoatType")
                        .Execute();

                    if (!DataHelper.DataSourceIsEmpty(currentTrip))
                    {
                       
                        foreach (DataRow member in mostRecentCrew.Tables[0].Rows)
                        {
                            Guid crewGuid = ValidationHelper.GetGuid(member["CurrentCrewUserGUID"], Guid.Empty);
                            string crewRole = ValidationHelper.GetString(member["CurrentCrewRole"], String.Empty);

                            // make a new APM.CurrentCrew item
                            CustomTableItem crewMember = CustomTableItem.New("APM.CurrentCrew");

                            //Set the datetime of this row to the date passed in
                            crewMember.SetValue("CurrentCrewDateTime", date.ToShortDateString());

                            //Set the modify by name to AutoSave so we know this process set the data
                            crewMember.SetValue("ModifiedByName", "AutoSave");

                            //CurrentCrewUserGUID
                            crewMember.SetValue("CurrentCrewUserGUID", crewGuid.ToString());

                            //CurrentCrewName
                            crewMember.SetValue("CurrentCrewName", ValidationHelper.GetString(member["CurrentCrewName"], String.Empty));
                            //CurrentCrewRole
                            crewMember.SetValue("CurrentCrewRole", crewRole);                            

                            //Trip Number
                            crewMember.SetValue("TripNumber", ValidationHelper.GetString(currentTrip.Tables[0].Rows[0]["TripNumber"], String.Empty));
                            //Barge Number
                            crewMember.SetValue("BargeNumber", ValidationHelper.GetString(vesselInformation.Tables[0].Rows[0]["VesselNumber"], String.Empty));
                            //Tug Number
                            crewMember.SetValue("TugNumber", ValidationHelper.GetString(vesselInformation.Tables[0].Rows[1]["VesselNumber"], String.Empty));

                            //save the crew entry
                            crewMember.Insert();

                            // add this to the result so we can report the task results
                            result.Add(string.Format("Adding crew member {0} to role {1} on {2}.", member["CurrentCrewName"], member["CurrentCrewRole"], date));

                            // track the list of user ids that we're going to need for setting up security
                            userIds.Add(crewGuid, crewRole);
                        }
                    }
                    else
                    {
                        throw new Exception("No active trip was found. Please make sure this boat is properly setup and is on an active trip.");
                    }
                }
                else
                {
                    new Exception("No recent crew could be found. Please update the Current Crew screen manually.");
                }
            }

            // if everything is good then set the security roles
            foreach (KeyValuePair<Guid, string> u in userIds)
            {
                APMHelper.SetUserRole(u.Key, u.Value);
            }

            //log the crew update results
            CMS.EventLog.EventLogProvider.LogInformation("CurrentCrewAutoSave", "AUTOCREW", string.Format("The following crew was autosaved:\n {0}", result.Join("\n")));           
        }
        catch (Exception ex)
        {
            CMS.EventLog.EventLogProvider.LogException("CurrentCrewAutoSave", "AUTOCREW", ex);
        }
       
        return result;
    }
}