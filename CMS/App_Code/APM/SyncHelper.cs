﻿using CMS.CustomTables;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using CMS.Membership;
using CMS.SiteProvider;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for SyncHelper
/// </summary>
public static class SyncHelper
{

    public const string syncDateField = "BoatSyncDateTime";
    
    /// <summary>
    /// Checks for internet connection.
    /// </summary>
    /// <returns></returns>
    public static bool CheckForInternetConnection()
    {
        try
        {
            //get the address of the main port sync service
            SettingsKeyInfo k = SettingsKeyInfoProvider.GetSettingsKeyInfo("APM_PortSyncServiceUrl");
            
            //detect if we can reach the service, then we know 
            using (var client = new WebClient())
            using (var stream = client.OpenRead(k.KeyValue))
            {
                return true;
            }
        }
        catch(Exception e)
        {
            return false;
        }
    }

    /// <summary>
    /// Checks for internet connection.
    /// </summary>
    /// <returns></returns>
    public static bool CheckForVesselConnection(string vesselSyncServiceUrl)
    {
        try
        {
            //detect if we can reach the service, then we know 
            using (var client = new WebClient())
            using (var stream = client.OpenRead(vesselSyncServiceUrl))
            {
                return true;
            }
        }
        catch (Exception e)
        {
            return false;
        }
    }

    /// <summary>
    /// Gets the list of tables to synchronize.
    /// </summary>
    /// <returns></returns>
    public static List<string> GetListOfTablesToSync()
    {
        List<string> tl = new List<string>();

        ObjectQuery<DataClassInfo> qdci = CMS.CustomTables.CustomTableHelper.GetCustomTableClasses();
        foreach(DataClassInfo dci in qdci)
        {
            // filter out any custom tables that are just for drop down lists      
            // and don't sync the vessel information table    
            if (!dci.ClassDisplayName.StartsWith("LT "))            
            {
                tl.Add(dci.ClassName);
            }          
        }
        return tl;
    }

    /// <summary>
    /// Synchronizes the with port - central server.
    /// </summary>
    /// <returns></returns>
    public static Dictionary<string, int> SyncWithPort()
    {
        //count the number of rows that get sync'd
        int successCount = 0;
        int failCount = 0;

        //if this is the port server then don't sync - just in case the service gets turned on, 
        // we want a backup to make sure we  don't sync to ourself
        SettingsKeyInfo k = SettingsKeyInfoProvider.GetSettingsKeyInfo("APM_IsPortServer");
        SettingsKeyInfo syncServerService = SettingsKeyInfoProvider.GetSettingsKeyInfo("APM_PortSyncServiceUrl");
        var isDebug = SettingsKeyInfoProvider.GetBoolValue("APM_DebugSynchronization");

        if (!ValidationHelper.GetBoolean(k.KeyValue, false)) //if we are not the port server, then sync
        {            
            //if we have an internet connect start synching
            if (CheckForInternetConnection())
            {
                //open a connection to the sync service
                var client = new APMSyncService.SyncWebServiceSoapClient("SyncWebServiceSoap", ValidationHelper.GetString(syncServerService.KeyValue, String.Empty));

                try
                {                    
                    //foreach table we want to synced
                    List<string> ts = GetListOfTablesToSync();
                    foreach (string t in ts)
                    {
                        //select all rows where BoatSyncDateTime IS NULL
                        ObjectQuery<CustomTableItem> qcti = CMS.CustomTables.CustomTableItemProvider.GetItems(t, String.Format("{0} IS NULL", syncDateField));
                        if (qcti.Count > 0)
                        {
                            if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncWithPort BEGIN {0}", qcti.ClassName), String.Format("Begin Object Sync - Success: Object:{0}", qcti.ClassName));

                            foreach (CustomTableItem i in qcti)
                            {
                                //set the sync time so it gets included in our push to the port server
                                i.SetValue(syncDateField, DateTime.Now);

                                //create an object that will play nice with a webservice
                                Dictionary<string, string> dcols = new Dictionary<string, string>();

                                foreach (string col in i.ColumnNames)
                                {
                                    dcols.Add(col, i.GetStringValue(col, ""));
                                }

                                //convert the row to a serializable type so we can pass it through a WCF service
                                string cti = Newtonsoft.Json.JsonConvert.SerializeObject(dcols);

                                //make sure we have a connection to the internet still
                                if (!CheckForInternetConnection())
                                {
                                    throw new Exception("Internet connection lost.");
                                }

                                //sync the row
                                if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncWithPort {0}", qcti.ClassName), String.Format("Sending data to Port server: Object:{0}, Guid:{1}", qcti.ClassName, ValidationHelper.GetGuid(dcols["ItemGUID"], Guid.Empty)));

                                APMSyncService.WebServiceResult wsr = client.Upsert(i.ClassName, cti, "VESSEL");
                               
                                //update that we succeeded
                                if (wsr.success)
                                {
                                    if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncWithPort {0}", qcti.ClassName), String.Format("Data Sync - Success: Object:{0}, Guid:{1}", qcti.ClassName, ValidationHelper.GetGuid(dcols["ItemGUID"], Guid.Empty)));

                                    i.Update();
                                    successCount++;
                                }
                                else
                                {
                                    if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncWithPort {0}", qcti.ClassName), String.Format("Data Sync - Failure: Object:{0}, Guid:{1}", qcti.ClassName, ValidationHelper.GetGuid(dcols["ItemGUID"], Guid.Empty)));

                                    failCount++;
                                }
                            }

                            if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncWithPort END {0}", qcti.ClassName), String.Format("Completed Object Sync - Success: Object:{0}, Synced Rows: {1}, Failed Rows: {2}", qcti.ClassName, successCount, failCount));
                        }
                        // end foreach row
                    }

                    //end foreach table
                    client.Close();
                } 
                catch(Exception ex)
                {
                    //end foreach table
                    if (client.State == System.ServiceModel.CommunicationState.Opened)
                        client.Close();

                    if (ex.Message == "Internet connection lost.")
                    {
                        EventLogProvider.LogInformation("SyncHelper", "SyncWithPort", ex.Message);
                    }
                    else
                    {
                        EventLogProvider.LogException("SyncHelper", "SyncWithPort", ex);
                    }
                }
            }
        }

        //update the success/fail count
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        dCount.Add("success", successCount);
        dCount.Add("fail", failCount);
        return dCount;
    }

    /// <summary>
    /// Synchronizes the with port - central server.
    /// </summary>
    /// <returns></returns>
    public static Dictionary<string, int> SyncFromPort(string vesselUrl, string tugNumber, string bargeNumber, string startDate)
    {
        //count the number of rows that get sync'd
        int successCount = 0;
        int failCount = 0;

        //if this is NOT the port server then don't sync
        SettingsKeyInfo k = SettingsKeyInfoProvider.GetSettingsKeyInfo("APM_IsPortServer");
        
        if (ValidationHelper.GetBoolean(k.KeyValue, false)) //if we ARE the port server, then sync
        {
            string vesselSyncServiceUrl = String.Format(SettingsKeyInfoProvider.GetStringValue("APM_VesselSyncServiceUrl", String.Empty), vesselUrl);
            var isDebug = SettingsKeyInfoProvider.GetBoolValue("APM_DebugSynchronization");

            //if we have a connection to the vessel
            if (CheckForVesselConnection(vesselSyncServiceUrl) && !String.IsNullOrEmpty(vesselSyncServiceUrl))
            {
                //open a connection to the sync service
                var client = new APMSyncService.SyncWebServiceSoapClient("SyncWebServiceSoap", vesselSyncServiceUrl);

                try
                {
                    //foreach table we want to synced
                    List<string> ts = GetListOfTablesToSync();
                    foreach (string t in ts)
                    {
                        //select all rows where 
                        // ModifiedDateTime is greater than or equal to the start date
                        // and the TugNumber and the BargeNumber match the vessel we're trying to sync
                        ObjectQuery<CustomTableItem> qcti = CMS.CustomTables.CustomTableItemProvider.GetItems(t, String.Format("TugNumber = '{0}' AND BargeNumber = '{1}' AND ItemModifiedWhen >= '{2}'", tugNumber, bargeNumber, startDate));
                        if (qcti.Count > 0)
                        {
                            if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncFromPort {0} BEGIN {1}", vesselUrl, qcti.ClassName), String.Format("Begin Object Sync - Success: Object:{0}", qcti.ClassName));

                            foreach (CustomTableItem i in qcti)
                            {
                                //create an object that will play nice with a webservice
                                Dictionary<string, string> dcols = new Dictionary<string, string>();

                                foreach (string col in i.ColumnNames)
                                {
                                    dcols.Add(col, i.GetStringValue(col, ""));
                                }

                                //convert the row to a serializable type so we can pass it through a WCF service
                                string cti = Newtonsoft.Json.JsonConvert.SerializeObject(dcols);

                                //make sure we have a connection to the internet still
                                if (!CheckForVesselConnection(vesselSyncServiceUrl))
                                {
                                    throw new Exception(String.Format("Connection to vessel lost: {0}", vesselUrl));
                                }

                                //sync the row
                                if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncFromPort {0}", qcti.ClassName), String.Format("Sending from Port server to {0}: Object:{1}, Guid:{2}", vesselUrl, qcti.ClassName, ValidationHelper.GetGuid(dcols["ItemGUID"], Guid.Empty)));

                                APMSyncService.WebServiceResult wsr = client.Upsert(i.ClassName, cti, "PORT");

                                //update that we succeeded
                                if (wsr.success)
                                {
                                    if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncFromPort {0}", qcti.ClassName), String.Format("Data Sync - Success: Object:{0}, Guid:{1}", qcti.ClassName, ValidationHelper.GetGuid(dcols["ItemGUID"], Guid.Empty)));
                                    
                                    successCount++;
                                }
                                else
                                {
                                    if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncFromPort {0}", qcti.ClassName), String.Format("Data Sync - Failure: Object:{0}, Guid:{1}", qcti.ClassName, ValidationHelper.GetGuid(dcols["ItemGUID"], Guid.Empty)));

                                    failCount++;
                                }
                            }

                            if (isDebug) EventLogProvider.LogInformation("SyncHelper", String.Format("SyncFromPort END {0}", qcti.ClassName), String.Format("Completed Object Sync - Success: Object:{0}, Synced Rows: {1}, Failed Rows: {2}", qcti.ClassName, successCount, failCount));
                        }
                        // end foreach row
                    }

                    //end foreach table
                    client.Close();
                }
                catch (Exception ex)
                {
                    //end foreach table
                    if (client.State == System.ServiceModel.CommunicationState.Opened)
                    {
                        client.Close();
                    }

                    if (ex.Message.Contains("Connection to vessel lost"))
                    {
                        EventLogProvider.LogInformation("SyncHelper", "SyncFromPort", ex.Message);
                    }
                    else
                    {
                        EventLogProvider.LogException("SyncHelper", "SyncFromPort", ex, CMS.SiteProvider.SiteContext.CurrentSiteID, ex.StackTrace);
                    }
                }
            }
        }
        else
        {
            EventLogProvider.LogInformation("SyncHelper", "SyncFromPort", "SyncFromPort can only be run from the PORT server.");
        }
     
        //update the success/fail count
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        dCount.Add("success", successCount);
        dCount.Add("fail", failCount);
        return dCount;
    }

    /// <summary>
    /// </summary>
    /// <returns></returns>
    public static Dictionary<string, int> GetUsersFromPort()
    {
        //count the number of users that get sync'd
        int successCount = 0;
        int failCount = 0;
        StringBuilder sb = new StringBuilder();

        var isDebug = SettingsKeyInfoProvider.GetBoolValue("APM_DebugSynchronization");

        //if we are not the port server then pull users
        SettingsKeyInfo k = SettingsKeyInfoProvider.GetSettingsKeyInfo("APM_IsPortServer");
        if (!ValidationHelper.GetBoolean(k.KeyValue, false))
        {
            //consume REST call to users on the port server
            string url = "http://port.andrieapm.com/rest/cms.user?format=json";  //TODO: Use kentico settings
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            request.AddHeader("content-type", "application/json");

            // get the token for the authorized rest user            
            string passwd = ConfigurationManager.AppSettings["RestAPIAdmin"];
            client.Authenticator = new HttpBasicAuthenticator("RestAPIAdmin", passwd);

            if (isDebug) EventLogProvider.LogInformation("SyncHelper", "GetUsersFromProt BEGIN");

            // or automatically deserialize result
            // return content type is sniffed but can be explicitly set via RestClient.AddHandler();
            try
            {
                //IRestResponse<List<UserInfo>> response = client.Execute<List<UserInfo>>(request);
                IRestResponse response = client.Execute(request);
                var content = response.Content; // raw content as string

                // get the main content of users
                JToken token = JObject.Parse(content);
                JToken root = token.SelectToken("cms_users");
                JArray users = (JArray)root.First.SelectToken("CMS_User");
               
                List<UserInfo> userList = ((JArray)users).Select(x => new UserInfo
                {
                    UserEnabled = ValidationHelper.GetBoolean(x["UserEnabled"].Value<bool>(), true),
                    UserName = ValidationHelper.GetString(x["UserName"].Value<string>(), ""),
                    UserGUID = ValidationHelper.GetGuid(x["UserGUID"].Value<string>(), Guid.Empty),
                    LastName = ValidationHelper.GetString(x["LastName"].Value<string>(), ""),
                    UserIsGlobalAdministrator = ValidationHelper.GetBoolean(x["UserIsGlobalAdministrator"].Value<bool?>(), false),
                    FullName = ValidationHelper.GetString(x["FullName"].Value<string>(), ""),
                    UserIsDomain = ValidationHelper.GetBoolean(x["UserIsDomain"].Value<bool?>(), false),
                    Email = ValidationHelper.GetString(x["Email"].Value<string>(), ""),
                    UserIsHidden = ValidationHelper.GetBoolean(x["UserIsHidden"].Value<bool?>(), false)                    
                }).ToList();

                bool loggedReason = false;

                if (userList != null)
                {
                    foreach (UserInfo user in userList)
                    {
                        if (UserInfoProvider.GetUserInfo(user.UserName) == null)
                        {
                            try
                            {
                                // add the user to the user table
                                UserInfoProvider.SetUserInfo(user);

                                // yes, the password is the same for everyone.
                                // Jul 19 @ 2:41PM
                                // Jeff Musselman commented on this task
                                //    Passwords are "andrie" You can reset all to that.
                                UserInfoProvider.SetPassword(user.UserName, "andrie");
                                
                                // make sure to assign the user to the site
                                UserInfoProvider.AddUserToSite(user.UserName, SiteContext.CurrentSiteName);

                                // up the count
                                successCount++;
                            }
                            catch (Exception ex)
                            {                                
                                failCount++;
                                                               
                                if (!loggedReason)
                                {
                                    sb.AppendLine(String.Format("Last Error Reason: {0}", ex.Message));
                                    sb.AppendLine("");
                                    sb.AppendLine("Users Failed To Sync:");
                                    loggedReason = true;
                                }                                
                                sb.AppendLine(String.Format("{0} - {1}", user.UserName, user.FullName));
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                EventLogProvider.LogException("SyncHelper", "GetUsersFromPort", ex);                
            }
            
        }

        if (isDebug) EventLogProvider.LogInformation("SyncHelper", "GetUsersFromProt END");

        // log any errors in one log entry
        if (sb.Length > 0)
        {
            EventLogProvider.LogException("SyncHelper", "GetUsersFromPort", new Exception("Error syncing users"), SiteContext.CurrentSiteID, sb.ToString());
        }


        //update the success/fail count
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        dCount.Add("success", successCount);
        dCount.Add("fail", failCount);
        return dCount;
    }
}