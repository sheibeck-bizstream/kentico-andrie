﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using CMS;
using CMS.CustomTables;
using CMS.EventLog;
using CMS.Scheduler;
using CMS.DataEngine;
using CMS.SiteProvider;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System.Text.RegularExpressions;
using System.Data.Linq.Mapping;
using System.Data;


/// <summary>
/// Summary description for ResetAllSharePointRecords
/// </summary>
[assembly: RegisterCustomClass("APM.SPReset", typeof(APM.ResetKenticoSharePointSync))]
namespace APM
{
    public class ResetKenticoSharePointSync : ITask
    {
        public string Execute(TaskInfo ti)
        {
            Reset();
            return "Reset executed successufully";
        }
        public void Reset()
        {
            string url = SettingsKeyInfoProvider.GetStringValue(SiteContext.CurrentSiteName + ".APM_SPURL");
            using (ClientContext clientContext = new ClientContext(url))
            {

                clientContext.Credentials = SharePointHelper.GetSPCredentials();
                string customTables = string.Empty;

                foreach (DataClassInfo customTable in CustomTableHelper.GetCustomTableClasses())
                {
                    if (!customTable.ClassDisplayName.Contains("LT "))
                    {
                        string queryText = string.Format(@"
                            UPDATE {0}
                            SET SPSyncDateTime = NULL", customTable.ClassTableName);
                        ConnectionHelper.ExecuteNonQuery(queryText, null, QueryTypeEnum.SQLQuery);
                    }
                }
            }
        }
    }
}