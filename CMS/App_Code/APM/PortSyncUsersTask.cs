﻿using System;
using System.Collections.Generic;
using CMS;
using CMS.Scheduler;


/// <summary>
/// Summary description for UserSynchronizationTask
/// </summary>
[assembly: RegisterCustomClass("APM.PortSyncUsersTask", typeof(APM.PortSyncUsersTask))]
namespace APM
{
    public class PortSyncUsersTask : ITask
    {

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <param name="ti">Info object representing the scheduled task</param>
        public string Execute(TaskInfo ti)
        {
            Dictionary<string, int> dCount = SyncHelper.GetUsersFromPort();
            return String.Format("Syncronized {0} users from port server. Failed to syncronize {1} rows from port server.", dCount["success"], dCount["fail"]);
        }     
    }
}