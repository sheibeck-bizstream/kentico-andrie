﻿using CMS.DataEngine;
using CMS.EventLog;
using CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using CMS.CustomTables;

/// <summary>
/// Summary description for APMHelper
/// </summary>
public static class APMHelper
{
    /// <summary>
    /// Sets the user role.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="roleName">Name of the role.</param>
    /// <returns></returns>
    public static bool SetUserRole(Guid userId, string roleName)
    {
        try
        {
            int uid = CMS.Membership.UserInfoProvider.GetUserInfoByGUID(userId).UserID;

            //remove the user from any existing APM roles
            List<int> userRoleList = CMS.Membership.UserRoleInfoProvider.GetUserRoles().WhereEquals("UserID", uid).Select(ur => ur.RoleID).ToList();
            foreach (var r in userRoleList)
            {
                if (CMS.Membership.RoleInfoProvider.GetRoleInfo(r).RoleName.Contains("APM_"))
                    CMS.Membership.UserInfoProvider.RemoveUserFromRole(uid, r);
            }

            //then add them to the new role if a role was specified
            if (!String.IsNullOrEmpty(roleName))
            {
                int rid = CMS.Membership.RoleInfoProvider.GetRoleInfo(string.Format("APM_{0}", roleName.Replace(" ", "_")), CMS.SiteProvider.SiteContext.CurrentSiteID).RoleID;
                CMS.Membership.UserInfoProvider.AddUserToRole(uid, rid);
            }
        }
        catch (Exception ex)
        {
            EventLogProvider.LogException("APMHelper", "SetUserRole", ex);
            return false;
        }
        return true;
    }

    /// <summary>
    /// Gets all of the crew
    /// </summary>    
    /// <returns></returns>
    public static List<APM.CrewMember> GetAllCrew()
    {
        DataSet cc = CustomTableItemProvider.GetItems("APM.CurrentCrew");
        List<APM.CrewMember> crewList = new List<APM.CrewMember>();

        if (!DataHelper.DataSourceIsEmpty(cc))
        {
            foreach (DataRow crew in cc.Tables[0].Rows)
            {
                crewList.Add(new APM.CrewMember()
                {
                    CrewMemberUserGUID = ValidationHelper.GetGuid(crew["CurrentCrewUserGUID"], Guid.Empty)
                                    ,
                    CrewMemberName = ValidationHelper.GetString(crew["CurrentCrewName"], String.Empty)
                                    ,
                    CrewMemberRole = ValidationHelper.GetString(crew["CurrentCrewRole"], String.Empty)
                                    ,
                    CrewMemberDateOnBoard = ValidationHelper.GetString(crew["CurrentCrewDateOnBoard"], String.Empty)
                });
            }
        }

        return crewList.OrderBy(x => x.CrewMemberName).ToList();
    }

    /// <summary>
    /// Gets the current crew.
    /// </summary>
    /// <param name="date">This is a javascript formatted standard datetime.</param>
    /// <returns></returns>
    public static List<APM.CrewMember> GetCrewForDate(string date)
    {
        DateTime dt = ValidationHelper.GetDateTime(date, DateTime.Now);
        
        string whereStatement = "";
        whereStatement += string.Format(" " +
                "DATEPART(YEAR, CurrentCrewDateTime) = '{0}'" +
                " AND DATEPART(DAY, CurrentCrewDateTime) = '{1}'" +
                " AND DATEPART(MONTH, CurrentCrewDateTime) = '{2}'", dt.Year, dt.Day, dt.Month);
        DataSet cc = CustomTableItemProvider.GetItems("APM.CurrentCrew",whereStatement);
        List<APM.CrewMember> crewList = new List<APM.CrewMember>();

        if (!DataHelper.DataSourceIsEmpty(cc))
        {
            foreach (DataRow crew in cc.Tables[0].Rows)
            {               
                crewList.Add(new APM.CrewMember() 
                                { CrewMemberUserGUID = ValidationHelper.GetGuid(crew["CurrentCrewUserGUID"], Guid.Empty)
                                    , CrewMemberName = ValidationHelper.GetString(crew["CurrentCrewName"], String.Empty)
                                    , CrewMemberRole = ValidationHelper.GetString(crew["CurrentCrewRole"], String.Empty)
                                    , CrewMemberDateOnBoard = ValidationHelper.GetString(crew["CurrentCrewDateOnBoard"], String.Empty)
                                });
            }
        }

        return crewList.OrderBy(x => x.CrewMemberName).ToList();
    }

    // Given the role name, get the current user of that role
    public static string GetCrewMemberNameForRole(string rolename, int whichItem, string Date, string TripNumber)
    {
        string whereStatement = "CurrentCrewRole = '" + rolename + "' AND ISNULL(IsDeleted,0) = 0";
        
        //If this doesn't have a date associated with it, then we want to get the it by the current date, but if it only contains a trip number then we don't want to filter by the current date.
        whereStatement += " AND DATEPART(DAY, CurrentCrewDateTime) = DATEPART(DAY, CAST('" + ValidationHelper.GetDateTime(Date, DateTime.Now).ToString() + "' as datetime))";
        whereStatement += " AND DATEPART(MONTH, CurrentCrewDateTime) = DATEPART(MONTH, CAST('" + ValidationHelper.GetDateTime(Date, DateTime.Now).ToString() + "' as datetime))";
        whereStatement += " AND DATEPART(YEAR, CurrentCrewDateTime) = DATEPART(YEAR, CAST('" + ValidationHelper.GetDateTime(Date, DateTime.Now).ToString() + "' as datetime))";
        
        var item = CMS.CustomTables.CustomTableItemProvider.GetItems("APM.CurrentCrew", whereStatement).FirstOrDefault();
        if (whichItem > 1)
        {
            item = CMS.CustomTables.CustomTableItemProvider.GetItems("APM.CurrentCrew", whereStatement).Skip(whichItem - 1).Take(1).FirstOrDefault();
        }        

        if (item != null)
        {
            return item.GetStringValue("CurrentCrewName", String.Empty);
        }
        else
        {
            return "";
        }        
    }

    // Gets the current user name
    public static string GetCurrentUsername()
    {
        return CMS.Membership.MembershipContext.AuthenticatedUser.UserName;
    }
}