﻿using System;
using System.Collections.Generic;
using CMS;
using CMS.Scheduler;


/// <summary>
/// Summary description for PortSynchronizationTask
/// </summary>
[assembly: RegisterCustomClass("APM.PortSynchronization", typeof(APM.PortSynchronizationTask))]
namespace APM
{
    public class PortSynchronizationTask : ITask
    {       

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <param name="ti">Info object representing the scheduled task</param>
        public string Execute(TaskInfo ti)
        {
            Dictionary<string, int> dCount = SyncHelper.SyncWithPort();
            return String.Format("Syncronized {0} rows to port server. Failed to syncronize {1} rows to port server.", dCount["success"], dCount["fail"]);
        }     
    }
}