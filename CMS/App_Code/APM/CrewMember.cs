﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APM
{
    /// <summary>
    /// Summary description for UserPermissions
    /// </summary>
    public class CrewMember
    {
        public Guid CrewMemberUserGUID { get; set; }
        public string CrewMemberName { get; set; }
        public string CrewMemberRole { get; set; }
        public string CrewMemberDateOnBoard { get; set; }

        public CrewMember()
        {            
        }
    }
}