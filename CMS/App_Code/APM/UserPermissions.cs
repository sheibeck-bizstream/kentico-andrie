﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APM
{
    /// <summary>
    /// Summary description for UserPermissions
    /// </summary>
    public class UserPermissions
    {
        public string role { get; set; }
        public bool read { get; set; }
        public bool modify { get; set; }
        public bool create { get; set; }
        public bool delete { get; set; }

        public UserPermissions()
        {
            role = string.Empty;
            read = false;
            modify = false;
            create = false;
            delete = false;
        }
    }
}