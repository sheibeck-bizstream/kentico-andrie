﻿using CMS.MacroEngine;
using CMS.Helpers;
using System;
using CMS;
using System.Web;

[assembly: RegisterExtension(typeof(CustomMacroMethods), typeof(string))]
public class CustomMacroMethods : MacroMethodContainer
{    
    [MacroMethod(typeof(string), "Get a revision number based on the contents of a file for use in cache busting", 1)]
    [MacroMethodParam(1, "param2", typeof(string), "Second part of the string (optional).")]
    public static object GetRevisionNumber(EvaluationContext context, params object[] parameters)
    {
        // Branches according to the number of the method's parameters
        switch (parameters.Length)
        {
            case 1:
                // Overload with one parameter
                string filePath = ValidationHelper.GetString(parameters[0], "");
                DateTime fileCreated = DateTime.Parse("1/1/1900 12:00:00");
                if (!String.IsNullOrEmpty(filePath))
                {
                    fileCreated = CMS.FileSystemStorage.FileInfo.New(HttpContext.Current.Server.MapPath(filePath)).LastWriteTime;
                }
                return fileCreated.GetHashCode();

            default:
                // No other overloads are supported
                throw new NotSupportedException();
        }
    }
}