﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using CMS;
using CMS.CustomTables;
using CMS.EventLog;
using CMS.Scheduler;
using CMS.DataEngine;
using CMS.SiteProvider;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System.Text.RegularExpressions;
using System.Data.Linq.Mapping;


/// <summary>
/// Summary description for Crew Change Task. This will update roles for arriving/departing crew
/// </summary>
[assembly: RegisterCustomClass("APM.CrewChangeTask", typeof(APM.CrewChangeTask))]
namespace APM
{
    public class CrewChangeTask : ITask
    {
        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <param name="ti">Info object representing the scheduled task</param>
        public string Execute(TaskInfo ti)
        {
            try
            {
                //Dictionary<string,int> updatedCount = APMHelper.UpdateCrewRoles();
                //return String.Format("Updated roles for {0} departing and {1} arriving crew members.", updatedCount["departing"], updatedCount["arriving"]);
                return "DEPRECATED";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }       
    }
}