﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using CMS;
using CMS.CustomTables;
using CMS.EventLog;
using CMS.Scheduler;
using CMS.DataEngine;
using CMS.SiteProvider;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System.Text.RegularExpressions;
using System.Data.Linq.Mapping;
using System.Data;


/// <summary>
/// Summary description for SharePointSynchronizationTask
/// </summary>
[assembly: RegisterCustomClass("APM.SPSynchronization", typeof(APM.SharePointSynchronizationTask))]
namespace APM
{

    public class SharePointSynchronizationTask : ITask
    {

        public string Execute(TaskInfo ti)
        {
            //Reset is for testing only  in order to remove all of the SharePoint Synchronization dates from the Kentico database
            //ResetSync();

            return SyncItems(ti);
        }

        private void ResetSync()
        {
            string url = SettingsKeyInfoProvider.GetStringValue(SiteContext.CurrentSiteName + ".APM_SPURL");
            using (ClientContext clientContext = new ClientContext(url))
            {

                clientContext.Credentials = SharePointHelper.GetSPCredentials();
                string customTables = string.Empty;

                foreach (DataClassInfo customTable in CustomTableHelper.GetCustomTableClasses())
                {
                    if (!customTable.ClassDisplayName.Contains("LT "))
                    {
                        string queryText = string.Format(@"
                            UPDATE {0}
                            SET SPSyncDateTime = NULL", customTable.ClassTableName);
                        ConnectionHelper.ExecuteNonQuery(queryText, null, QueryTypeEnum.SQLQuery);
                    }
                }
            }
        }

        private string SyncItems(TaskInfo ti)
        {
            string url = SettingsKeyInfoProvider.GetStringValue(SiteContext.CurrentSiteName + ".APM_SPURL");
            using (ClientContext clientContext = new ClientContext(url))
            {

                clientContext.Credentials = SharePointHelper.GetSPCredentials();
                string customTables = string.Empty;
                //Iterate through all of the custom tables in order to sychronize them to SharePoint
                bool hadError = false;
                int totalInserts = 0;
                int totalFailedInserts = 0;
                int totalSuccessfulUpdates = 0;
                int totalFailedUpdates = 0;
                foreach (DataClassInfo customTable in CustomTableHelper.GetCustomTableClasses())
                {
                    string customTableName = SharePointHelper.ReadableFormat(customTable.ClassTableName);
                    List<string> successfullyUpsertedItems = new List<string>();
                    if (!customTable.ClassDisplayName.Contains("LT "))
                    {
                        //Make sure that the list exists since SharePoint doesn't have a list exists function
                        int successfulInserts = 0;
                        int failedInserts = 0;
                        int successfulUpdates = 0;
                        int failedUpdates = 0;
                        try
                        {
                            List<DataRow> KenticoUpdatedRecords = new List<DataRow>();
                            DataSet KenticoNewRecords = new DataSet();
                            SharePointHelper.UpdateAllItemsFromKentico(customTable, clientContext, ref KenticoUpdatedRecords, ref KenticoNewRecords, ref successfulUpdates, ref failedUpdates);

                            successfullyUpsertedItems = (from u in KenticoUpdatedRecords
                                                         select u["ItemGUID"].ToString()).ToList();
                            //Update all of our items
                            //If it isn't in debug mode, then process them all as a batch
                            bool isDebug = SettingsKeyInfoProvider.GetBoolValue("APM_DebugSynchronization");
                            if (!isDebug)
                            {
                                clientContext.ExecuteQuery();
                            }
                            foreach (DataRow dr in KenticoUpdatedRecords)
                            {
                                KenticoNewRecords.Tables[0].Rows.Remove(dr);
                            }
                            
                            if (KenticoNewRecords.Tables.Count > 0 && KenticoNewRecords.Tables[0].Rows.Count > 0)
                            {
                                    SharePointHelper.AddKenticoRecordsToSharepoint(KenticoNewRecords, clientContext, customTable.ClassTableName, ref successfullyUpsertedItems, ref successfulInserts, ref failedInserts);
                            }
                            if (failedUpdates > 0 || failedInserts > 0)
                            {
                                SharePointHelper.Log("SharePointSynchronizationTask.SyncItems Unsuccessful", "Some of the records in table " + customTableName + " did not import successfully, see the event log for details", true);
                                hadError = true;
                            }
                            else
                            {
                                if (successfulUpdates > 0)
                                {
                                    SharePointHelper.Log("Table " + customTable.ClassTableName + " sucessfully updated " + successfulUpdates + " items.", "SharePointSynchronizationTask.SyncItems");
                                }
                                if (successfulInserts > 0)
                                {
                                    SharePointHelper.Log("Table " + customTable.ClassTableName + " sucessfully inserted " + successfulUpdates + " items.", "SharePointSynchronizationTask.SyncItems");
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            hadError = true;
                            string logText = string.Empty;
                            if (successfulInserts > 0 && successfulUpdates > 0)
                            {
                                logText = "List " + customTableName + " successfully inserted " + successfulInserts + " rows, and updated " + successfulUpdates + " rows.";
                            }
                            else if (successfulInserts > 0 && successfulUpdates == 0)
                            {
                                logText = "List " + customTableName + " successfully inserted " + successfulInserts + " rows.";
                            }
                            else if (successfulInserts == 0 && successfulUpdates > 0)
                            {
                                logText = "List " + customTableName + " successfully updated " + successfulInserts + " rows.";
                            }
                            SharePointHelper.Log("SyncItems failed", "List " + customTableName + " failed updating " + failedUpdates + " records, and failed to insert " + failedInserts + " rows. Also did not complete the import because of error." + logText, true, e);
                        }
                        finally
                        {
                            if (successfulInserts > 0 || successfulUpdates > 0 || hadError)
                            {
                                //SharePointHelper.Log("End " + customTable.ClassTableName + " synchronization.", "Ended Synchronizing Object");
                            }
                        }

                        //Once we've updated the list item rows, then we want to make sure that we mark the rows that were updated as updated
                        DateTime currentDateTime = DateTime.Now;
                        QueryDataParameters qdp = new QueryDataParameters();
                        string upsertedItems = "'" + string.Join("','", successfullyUpsertedItems.ToArray()) + "'";


                        totalInserts += successfulInserts;
                        totalFailedInserts += failedInserts;
                        totalSuccessfulUpdates += successfulUpdates;
                        totalFailedUpdates += failedUpdates;
                    }


                }

                return string.Format("Port Sync {4} {0} insert, {1} insertfail, {2} update, {3} updatefail", totalInserts, totalFailedInserts, totalSuccessfulUpdates, totalFailedUpdates, hadError? "Failed!" : "Successful");

            }
        }
    }
}