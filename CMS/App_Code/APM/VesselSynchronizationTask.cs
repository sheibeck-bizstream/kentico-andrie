﻿using System;
using System.Collections.Generic;
using CMS;
using CMS.Scheduler;
using CMS.Helpers;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for SharePointSynchronizationTask
/// </summary>
[assembly: RegisterCustomClass("APM.VesselSynchronization", typeof(APM.VesselSynchronizationTask))]
namespace APM
{
    public class VesselSynchronizationTask : ITask
    {       

        /// <summary>
        /// Executes the task.
        /// </summary>
        /// <param name="ti">Info object representing the scheduled task</param>
        public string Execute(TaskInfo ti)
        {
            //taskdata will be the unique combination of
            // tugnumber + bargenumber
            // only sync data that is from the specified start date forward. If no 
            // start date is specified date will be since the beginning of the current year
            /*
                 Example task data:
                     {
                         "VesselUrl": "ka.andrieapm.com"
                         "TugNumber": "12345",
                         "BargeNumber": "67890",
                         "StartDate": "01/01/2015",             
                      }
            */
            try
            {
                JavaScriptSerializer json = new JavaScriptSerializer();
                var jsonObj = json.DeserializeObject(ti.TaskData);
                Dictionary<string, object> taskInfo = (Dictionary<string, object>)jsonObj;

           
                string vesselUrl = ValidationHelper.GetString(taskInfo["VesselUrl"], String.Empty);
                string tugNumber = ValidationHelper.GetString(taskInfo["TugNumber"], String.Empty);
                string bargeNumber = ValidationHelper.GetString(taskInfo["BargeNumber"], String.Empty);
                string startDate = ValidationHelper.GetDateTime( (taskInfo.ContainsKey("StartDate") ? taskInfo["StartDate"] : null), new DateTime(DateTime.Now.Year, 1, 1) ).ToShortDateString();

                if (!String.IsNullOrEmpty(tugNumber)
                    && !String.IsNullOrEmpty(bargeNumber)
                    && !String.IsNullOrEmpty(vesselUrl))
                {
                    Dictionary<string, int> dCount = SyncHelper.SyncFromPort(vesselUrl, tugNumber, bargeNumber, startDate);
                    return String.Format("Syncronized {0} rows to vessel {2}. Failed to syncronize {1} rows to vessel {2}.", ValidationHelper.GetInteger(dCount["success"], 0), ValidationHelper.GetInteger(dCount["fail"], 0), vesselUrl);
                }
                else
                {
                    return "Could not find vessel for Syncronization";
                }
            }
            catch(Exception ex)
            {
                CMS.EventLog.EventLogProvider.LogException("VesselSynchronizationTask", "SYNCERROR", ex, CMS.SiteProvider.SiteContext.CurrentSiteID, ex.StackTrace);
                return ex.Message;
            }
        }     
    }
}