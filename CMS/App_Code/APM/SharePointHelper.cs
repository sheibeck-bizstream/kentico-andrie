﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using CMS;
using CMS.CustomTables;
using CMS.EventLog;
using CMS.Scheduler;
using CMS.DataEngine;
using CMS.SiteProvider;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System.Text.RegularExpressions;
using System.Data.Linq.Mapping;
using System.Data;
using CMS.Helpers;

/// <summary>
/// Summary description for SharePointHelper
/// </summary>
public static class SharePointHelper
{

    //We need to set a row limit here or we will reach the list threshold. 
    //I know it's kind of dumb since the where statement should limit this, but it doesn't.
    //Go SharePoint
    private const string XML_QUERY_FORMAT = "<View><Query><Where><In><FieldRef Name='ItemGUID' /><Values>{0}</Values></In></Where></Query><RowLimit>100</RowLimit></View>";
    private const string FIELD_ID_FORMAT = "<Value Type='Guid'>{0}</Value>";
    
    public static SharePointOnlineCredentials GetSPCredentials()
    {
        //TODO: Get username, password, and URL from settings
        Console.WriteLine("Connecting to SharePoint...");
        SecureString passWord = new SecureString();
        foreach (char c in SettingsKeyInfoProvider.GetStringValue(SiteContext.CurrentSiteName + ".APM_SPPassword").ToCharArray()) passWord.AppendChar(c);
        return new SharePointOnlineCredentials(SettingsKeyInfoProvider.GetStringValue(SiteContext.CurrentSiteName + ".APM_SPUsername"), passWord);
    }

    public static void Log(string eventDescription, string detail, bool isError = false, Exception ex = null)
    {
        bool isDebug = SettingsKeyInfoProvider.GetBoolValue("APM_DebugSynchronization");
        if (!isError && isDebug)
        {
            EventLogProvider.LogInformation("SharePoint Custom Event", eventDescription, detail);
        }
        else if(isError)
        {
            EventLogProvider.LogException(eventDescription, detail, ex);
        }
        //Don't log anything if it isn't an error and we aren't debugging

    }

    //Makes Spaces in the list based on camel case formatting
    public static string ReadableFormat(string inString)
    {
        string returnString = string.Empty;
        //Get rid of the database prefix
        returnString = inString.Replace("dbo.APM_", "");
        returnString = inString.Replace("APM_", "");
        //Yay for stack overflow http://stackoverflow.com/questions/272633/add-spaces-before-capital-letters
        returnString = Regex.Replace(returnString, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
        return returnString;
    }

    public static void UpdateListItem(DataRow RecordToSync, ListItem itemToUpdate, DataColumnCollection KenticoColumns)
    {
        //Update any item that matches, and remove it from the updated Kentico items so that we can update the new items in the next foreach loop
        foreach (DataColumn column in KenticoColumns)
        {
            string SPColumnName = SharePointHelper.GetSharePointInternalCodedField(column.ColumnName);
            string KenticoColumnData = RecordToSync[column.ColumnName].ToString();
            if (!string.IsNullOrEmpty(KenticoColumnData))
            {
                return;
            }
            DateTime outValue;

            if (DateTime.TryParse(KenticoColumnData, out outValue))
            {
                //if the datetime is earlier than 1900 then we know it isn't valid
                if (DateTime.Compare(outValue, new DateTime(1900, 1, 1)) < 0)
                {
                    return;
                }
                //SharePoint doesn't do seconds...so you have to do this on update. At least it's helpful by automatically converting them on inserts, just not on updates.
                //There are a couple of workarounds one being risky http://sharepoint.stackexchange.com/questions/87124/formula-to-save-seconds-and-milliseconds-in-sharepoint-list-for-datetime-type-co
                //and the other one being a pain to implement http://stackoverflow.com/questions/20997711/how-to-save-datetime-with-milliseconds-in-sharepoint-2010
                //I figured they aren't too concerned about seconds in the values considering it's all manual entry anyway, so we'll just stick with seconds. The other thing we could do is just convert
                //everything into text fields and deal with it that way.
                //  2/23/2012 2:25 PM
                itemToUpdate.ParseAndSetFieldValue(SPColumnName, String.Format("{0:g}", outValue));
            }
            else
            {
                itemToUpdate.ParseAndSetFieldValue(SPColumnName, KenticoColumnData);
            }

            //itemToUpdate[SPColumnName] = KenticoColumnData;
        }
        itemToUpdate.Update();
    }

    //Updates all of the items, and then returns a list of the things that were updated along with a list of the new items by reference
    public static void UpdateAllItemsFromKentico(DataClassInfo customTable, ClientContext clientContext, ref List<DataRow> KenticoUpdatedRecords, ref DataSet KenticoNewRecords, ref int SucessfullyUpdated, ref int FailedUpdated)
    {
        int maxRowsPerRun = 59; // sharepoint is a dawg http://www.networksteve.com/enterprise/topic.php/CAML_query_IN_Clause_limiation/?TopicId=69973&Posts=1
        var whereStatement = new WhereCondition().Where("SPSyncDateTime", QueryUnaryOperator.IsNull);

        string whereGUIDIn = string.Empty;
        DataQuery changedDataQuery = new DataQuery(customTable.ClassName + ".generalselect").Where(whereStatement);
        DataSet allKenticoChangedRecords = changedDataQuery.TopN(maxRowsPerRun);
        Log("Updating All Items from Kentico table " + customTable.ClassName, allKenticoChangedRecords.Tables[0].Rows + " Rows changed from Kentico.");
        if (DataHelper.DataSourceIsEmpty(allKenticoChangedRecords))
            return;
        try
        {
            if (allKenticoChangedRecords.Tables[0].Rows.Count > 0)
            {
                //Log("Begin " + customTable.ClassTableName + " synchronization.", "Started Synchronizing");
            }
            foreach (DataRow dr in allKenticoChangedRecords.Tables[0].Rows)
            {
                whereGUIDIn = whereGUIDIn + string.Format(FIELD_ID_FORMAT, dr["ItemGUID"]);
            }
            KenticoNewRecords = changedDataQuery;
        }
        catch (Exception exe)
        {
            Log("Loading data error", "problem loading data into the changed records dataset", true, exe);
        }
        

        //Only run the updates if there is something we actually need to update
        if (!string.IsNullOrEmpty(whereGUIDIn))
        {
            bool removeRow = false;
            //Get all of the records from SharePoint that have changed recently in Kentico
            List syncList = clientContext.Web.Lists.GetByTitle(SharePointHelper.ReadableFormat(customTable.ClassTableName));
            CamlQuery query = new CamlQuery();
            ListItemCollection SPUpdateList  = null;
            string viewXML = string.Format(XML_QUERY_FORMAT, whereGUIDIn);
            query.ViewXml = viewXML;
            try
            {
                SPUpdateList = syncList.GetItems(query);
                clientContext.Load(SPUpdateList);
                clientContext.ExecuteQuery();
            }
            catch (Exception ex)
            {
                Log("List loading error", "Problem loading up the SPUpdate List", true, ex);
            }
            //Iterate through all of the changed Kentico items
            foreach (DataRow dr in allKenticoChangedRecords.Tables[0].Rows)
            {
                removeRow = false;
                //Iterate through all of the list items where the GUID has changed from the Kentico side of things for each of the changed Kentico items
                foreach (ListItem itemToUpdate in SPUpdateList)
                {
                    if (itemToUpdate["ItemGUID"].ToString() == dr["ItemGUID"].ToString())
                    {
                        removeRow = true;
                        try
                        {
                            UpdateListItem(dr, itemToUpdate, allKenticoChangedRecords.Tables[0].Columns);
                            //If it is in debug mode, then process them one by one instead of in a batch
                            bool isDebug = SettingsKeyInfoProvider.GetBoolValue("APM_DebugSynchronization");
                            if (isDebug)
                            {
                                clientContext.ExecuteQuery();
                            }
                            //DataHelper.SetDataRowValue(dr, "SPSyncDateTime", DateTime.Now);
                            ConnectionHelper.ExecuteNonQuery("UPDATE " + customTable.ClassTableName + " SET SPSyncDateTime = '" + DateTime.Now.ToString() + "'", null, QueryTypeEnum.SQLQuery);
                            SucessfullyUpdated++;
                        }
                        catch (Exception e)
                        {
                            FailedUpdated++;
                            Log("SharePointHelper.UpdateListItem", "Item " + itemToUpdate["ItemGUID"] + " failed to update from table " + customTable.ClassTableName + " into SharePoint.", true, e);
                        }
                    }
                }

                if (removeRow)
                {
                    KenticoUpdatedRecords.Add(dr);
                }
            }
        }
    }

    public static void AddKenticoRecordsToSharepoint(DataSet KenticoNewRecords, ClientContext clientContext, string listName, ref List<string> successfullyUpsertedItems, ref int successfulInserts, ref int failedInserts)
    {

        //Iterate through all of the records that we know that have been added
        List syncList = clientContext.Web.Lists.GetByTitle(SharePointHelper.ReadableFormat(listName));
        if (DataHelper.DataSourceIsEmpty(KenticoNewRecords))
            return;
        foreach (DataRow dr in KenticoNewRecords.Tables[0].Rows)
        {
            syncList = clientContext.Web.Lists.GetByTitle(SharePointHelper.ReadableFormat(listName));
            //Create a list item for each one of these records
            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
            ListItem itemToAdd = syncList.AddItem(itemCreateInfo);
            foreach (DataColumn column in KenticoNewRecords.Tables[0].Columns)
            {
                string SPColumnName = GetSharePointInternalCodedField(column.ColumnName);
                string KenticoColumnData = dr[column.ColumnName].ToString();
                Type type = column.DataType;
                try
                {
                    if (!string.IsNullOrEmpty(KenticoColumnData))
                    {
                        if (type.Equals(typeof(Int32)))
                        {
                            itemToAdd[SPColumnName] = Int32.Parse(KenticoColumnData);
                        }
                        else if (type.Equals(typeof(Decimal)))
                        {
                            itemToAdd[SPColumnName] = Decimal.Parse(KenticoColumnData);
                        }
                        else if (type.Equals(typeof(Int64)))
                        {
                            itemToAdd[SPColumnName] = Int64.Parse(KenticoColumnData);
                        }
                        else if (type.Equals(typeof(Double)))
                        {
                            itemToAdd[SPColumnName] = Double.Parse(KenticoColumnData);
                        }
                        else if (type.Equals(typeof(DateTime)))
                        {
                            if (DateTime.Compare(DateTime.Parse(KenticoColumnData), new DateTime(1900, 1, 1)) > 0)
                            {
                                itemToAdd[SPColumnName] = DateTime.Parse(KenticoColumnData);
                            }
                        }
                        else if (type.Equals(typeof(Guid)))
                        {
                            itemToAdd[SPColumnName] = Guid.Parse(KenticoColumnData);
                        }
                        else
                        {
                            itemToAdd[SPColumnName] = KenticoColumnData;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log("Problem matching up field types", "It errored out whil trying to match up the columns in the case statement when adding records", true, ex);
                }
            }
            try{
            itemToAdd.Update();

                //TODO: try to get the Execute query outside of the for loop for a batch statement
                clientContext.ExecuteQuery();
                successfullyUpsertedItems.Add(dr["ItemGUID"].ToString());
                //Add all of our items
                successfulInserts++;
            }
            catch (Exception e)
            {
                failedInserts++;
                Log("SharePointHelper.AddKenticoRecordsToSharepoint", "Item " + dr["ItemGUID"].ToString() + " failed to insert into the " + listName + " SharePoint list.", true, e);
            }
        }
    }

    public static ListItemCollection GetListItems(string listName, ClientContext context)
    {
        List<string> fields = new List<string>();
        Web web = context.Web;
        List SPList = web.Lists.GetByTitle(listName);
        CamlQuery query = CamlQuery.CreateAllItemsQuery(100000);
        ListItemCollection items = SPList.GetItems(query);
        context.Load(items);
        context.Load(SPList.Fields);
        context.ExecuteQuery();

        foreach (ListItem li in items)
        {
            Console.WriteLine("Getting List items for " + listName);
            foreach (Field f in SPList.Fields)
            {
                Log("GetListItems", f.Title + ":" + li[f.Title]);
            }
        }
        return items;
    }

    //SharePoint inserts the following string for each space in the field _x0080x_. It also truncates the fields down to 32 characters, and if more than one field has the same
    //name after the truncation, then it truncates it down to 31 characters and adds a 0 for second duplicate, 1 for the 3rd, 2 for the 4th and so on. 
    //The easiest thing to do for synchronization is to simply not allow spaces when we create tables
    public static string GetSharePointInternalCodedField(string inString)
    {
        string returnString = string.Empty;
        //SharePoint replaces the space character with these characters for some reason
        returnString = inString;
        //returnString = (ReadableFormat(inString).Replace(" ", "_x0020_"));
        if (returnString.Length > 32)
        {
            returnString = returnString.Substring(0, 32);
        }
        return returnString;
    }

    #region list creation methods
    //To use any of these methods, you need to create the dbml classes associated with the Kentico environment that you are working with.
    //The creation info will have a description related to andrie, so if you reuse this with another project, you may need to change that.
    //Currently the field types are very loose, so if you reuse this you may have to refine how we create the fields in SharePoint for how they are represented in Kentico

    public static void DeleteSPList(string listName, ClientContext context)
    {
        try
        {
            Console.WriteLine("Deleting List: " + listName);
            Web web = context.Web;
            List list = web.Lists.GetByTitle(listName);
            list.DeleteObject();
            context.ExecuteQuery();
        }
        catch (Exception e)
        {
            Log("DeleteSPList", "Failed to Delete List", true, e);
        }
    }

    private static void CreateSPList(string listName, ClientContext context, List<MetaDataMember> listColumns, ListTemplateType listType = ListTemplateType.GenericList)
    {
        try
        {
            Console.WriteLine("Creating List: " + listName);
            Web web = context.Web;
            ListCreationInformation creationInfo = new ListCreationInformation();
            creationInfo.Description = "This is an Andrie APM list from a Kentico instance on a ship server";
            creationInfo.Title = listName;
            creationInfo.TemplateType = (int)listType;
            List list = web.Lists.Add(creationInfo);
            string xmlFormat = "<Field DisplayName='{0}' Type='{1}' ShowInDisplayForm='{2}' ShowInEditForm='{2}' ShowInNewForm='{2}' Name='{3}'/>";
            foreach (MetaDataMember column in listColumns)
            {
                Field field;
                FieldType fieldType = FieldType.Text;
                //Note we don't support timespans at this point
                switch (column.Type.ToString())
                {
                    case "System.Nullable`1[System.Int32]":
                    case "System.Int32":
                    case "System.Nullable`1[System.Int64]":
                    case "System.Int64":
                    case "System.Nullable`1[System.Decimal]":
                    case "System.Decimal":
                    case "System.Nullable`1[System.Double]":
                    case "System.Double":
                        //If the data type is a decimal, and we are looking to the 2nd Decimal place, then we will assume this is currency
                        //90% of the time this will be true, and we can change it manually the other 10% of the time
                        if (column.DbType.ToString().Contains("Decimal") && column.DbType.ToString().Contains(",2)"))
                        {
                            fieldType = FieldType.Currency;
                        }
                        else
                        {
                            fieldType = FieldType.Number;
                        }
                        break;
                    case "System.Nullable`1[System.DateTime]":
                    case "System.DateTime":
                        fieldType = FieldType.DateTime;
                        break;
                    case "System.Nullable`1[System.Guid]":
                    case "System.Guid":
                        fieldType = FieldType.Guid;
                        break;
                    case "System.Nullable`1[System.String]":
                    case "System.String":
                        fieldType = FieldType.Text;
                        break;

                }
                //This creates the field and formats the name so that it look good in SharePoint
                if (column.Name.ToString().Contains("Item"))
                {
                    string xml = string.Format(xmlFormat, column.Name, fieldType.ToString(), "FALSE", column.Name);
                    field = list.Fields.AddFieldAsXml(xml, false, AddFieldOptions.DefaultValue);
                }
                else
                {
                    string xml = string.Format(xmlFormat, column.Name, fieldType.ToString(), "TRUE", column.Name);
                    field = list.Fields.AddFieldAsXml(xml, true, AddFieldOptions.DefaultValue);
                }

                if (column.DbType.ToString().Contains("NOT NULL") && field != null)
                {
                    field.Required = true;
                    field.Update();
                }
            }
            Field titleColumn = list.Fields.GetByTitle("Title");
            titleColumn.Required = false;
            titleColumn.UpdateAndPushChanges(true);
            list.Update();
            context.ExecuteQuery();
        }
        catch (Exception e)
        {
            Log("CreateSPList", "Error while Creating list: " + listName, true, e);
        }
    }

    #endregion
}