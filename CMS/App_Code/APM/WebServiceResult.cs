﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for WebServiceResult
/// </summary>
public class WebServiceResult
{
    public string data {get;set;}
    public List<string> datalist { get; set; }
    public bool success { get; set; }
    public string message { get; set; }

	public WebServiceResult()
	{		
	}
}