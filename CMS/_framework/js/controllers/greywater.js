﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _greyWater = $scope.greyWater = $scope.$new();
            
            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.dateFormat = Util.dateFormat(true);

            //tell the rest api what object we're looking for
            _greyWater.classname = 'apm.greywater'; //case sensitive
            _greyWater.customTableName = 'APM_GreyWater';

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_greyWater.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";
                }, function (newVal, oldVal) {
                    if (newVal)
                        _greyWater.List();
                });

                $scope.$watch('page.dateFormat', function (scope) {
                    _page.viewDate = new Date();
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        _page.complete = false;
                        _greyWater.List();
                    }
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables     
            //LookupTables.get('CargoType').then(function (result) {
            //    _page.cargoTypes = result;
            //});
            //LookupTables.get('TerminalDockName', 'TerminalName', 'TerminalName').then(function (result) {
            //    _page.terminalDocks = result;
            //});

            //_page.showLookupData = function (tableName, data, dataId) {
            //    return LookupTables.show(tableName, data, dataId);
            //}

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                "min-mode": "month",
                "datepicker-mode": "'month'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "MM/yyyy") === $filter('date')(new Date(), "MM/yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _greyWater.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
                var month = parseInt(fViewDate[0]);
                var year = parseInt(fViewDate[1]);
                where = where || "Month(GreyWaterDateTime)=" + month + " AND Year(GreyWaterDateTime)=" + year;

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;
                    _page.complete = true;
                });
            }

            //Update a Record == .update()
            _greyWater.Validate = function (data, validationType) {
                return Util.validate(data, validationType);
            }

            //Insert a new record OR update an existing record
            _greyWater.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _greyWater.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;
                    $this.data[result.insertedIndex].GreyWaterDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm");
                });
            };

            //Deletes
            _greyWater.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];