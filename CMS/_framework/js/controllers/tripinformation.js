module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'Modal', 'Security', 'Vessel', 'Trip',
	function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, Modal, Security, Vessel, Trip) {

		/*********************************************/
		/* scope vars */
		var _page = $scope.page = $scope.$new();
		var _tripInfo = $scope.tripInfo = $scope.$new();		
	   		
		//tell the rest api what object we're looking for
		_tripInfo.classname = 'apm.tripinformation'; //case sensitive
		_tripInfo.customTableName = 'APM_TripInformation';

	    /* end scope vars */
	    /*********************************************/			
		
		/*******************************************/
	    /* BEGIN Methods */

	    //show a status alerts            
		_page.addAlert = function (message, type) {
		    _page.alerts = Util.addAlert(message, type);
		};
		_page.closeAlert = function (index) {
		    _page.alerts = Util.closeAlert();
		};
	    //some messages auto fall off the list, watch for them and then update the alerts
		Util.registerObserverCallback(function () {
		    _page.alerts = Util.pageAlerts;
		});
	    // end status alerts


	    // Set security
	    // Tell the screen what permissions this user has
		Security.get(_tripInfo.classname).then(function (result) {
		    _page.security = result;

		    // Fetch Vessel
		    Vessel.get().then(function (result) {
		        _page.vessel = result;

		        //initialize the view
		        _tripInfo.List();
		    });
		});	   

        // start a new trip and make it active
		_tripInfo.StartTrip = function () {
		    var modalOptions = {
		        closeButtonText: 'Cancel',
		        actionButtonText: 'Start Trip',
		        headerText: 'Confirm Start Trip',
		        bodyText: 'Are you sure you want to start a new trip?'
		    };

		    //confirm the new trip
		    Modal.showModal({}, modalOptions).then(function (result) {
		        // make the current trip not the current trip
		        var curTrip = $.grep(_tripInfo.data, function (obj, idx) { return obj.IsCurrent; });

                //close down the current trip if we have one
		        if (curTrip.length > 0) {
		            var idx = _tripInfo.data.indexOf(curTrip[0]);

		            _tripInfo.data[idx].IsCurrent = false;
		            _tripInfo.data[idx].IsActive = false;
		            _tripInfo.data[idx].IsNext = false;		            
		            _tripInfo.Save(_tripInfo.data[idx], false, false);
		        }
             		        
		        //start the next trip
		        var nextTrip = $.grep(_tripInfo.data, function (obj, idx) { return obj.IsNext; });
		        if (nextTrip.length > 0) {
		            var idx = _tripInfo.data.indexOf(nextTrip[0]);

		            _tripInfo.data[idx].IsCurrent = true;
		            _tripInfo.data[idx].IsActive = true;
		            _tripInfo.data[idx].IsNext = false;
		            _tripInfo.data[idx].TripStart = Util.getDate(false);
		            _tripInfo.Save(_tripInfo.data[idx], true, false);		            
		        }
		        else {
		            //we don't have a next trip yet, make one and then activate it
		            _tripInfo.Insert(true, true, true);
		        }
		    });
		};

        //end an active trip, but keep it current
		_tripInfo.EndTrip = function () {
		    var modalOptions = {
		        closeButtonText: 'Cancel',
		        actionButtonText: 'End Trip',
		        headerText: 'Confirm End Trip',
		        bodyText: 'Are you sure you want to end your current trip?'
		    };

		    //confirm the delete
		    Modal.showModal({}, modalOptions).then(function (result) {
		        var activeTrip = $.grep(_tripInfo.data, function (obj, idx) { return obj.IsActive; });

		        if (activeTrip.length > 0) {
		            var idx = _tripInfo.data.indexOf(activeTrip[0]);

		            //actually end the trip
		            _tripInfo.data[idx].IsActive = false;
		            _tripInfo.data[idx].TripEnd = Util.getDate(false);
		            _tripInfo.Save(_tripInfo.data[idx]);
		        }
		    });
		};

		/* END Methods */
		/*******************************************/


		/*******************************************/
		/* BEGIN REST Methods */

	    //get the active or last trip if there is not active trip
		_tripInfo.List = function () {
		    var $this = this;

		    var orderby = "IsActive DESC, IsCurrent DESC, TripNumber DESC";
            var where = "IsCurrent=1 OR IsNext=1"
		    
            //get the active trip or the last trip that is not the "next" trip. 
            RestApi.list($this, where, orderby).then(function (result) {
                if (result.length > 0) {
                    $this.data = result;
                    _page.complete = true;
                }
                else {
                    //initialize our data and add a new record
                    $this.data = [];

                    //if we have no defined trips, don't make the first one current so we can edit the starting trip number if we need to
                    $this.Insert(true, false, false);
                }
		    });
		}

		//Update the current trip information
		_tripInfo.Save = function (data, makeNextTrip, refresh) {
		    var $this = this;

		    RestApi.save($this, data).then(function () {
		        if (makeNextTrip) {		            
		            //and add a next trip
		            $this.Insert(true, false, false);
		        }

		        if (refresh) {
		            //refresh the list when we save
		            $this.List();
		        }

		    });
		};

		//Insert a Record, but it doesn't become a true record until we click save
		_tripInfo.Insert = function (isNextTrip, isCurrentTrip, isActive) {
		    var $this = this;
		    _page.complete = false;

		    RestApi.insert(_page, $this).then(function (result) {
		        //update our scope variable with modified stuff, we're updating the whole object
		        // not just the data so we can capture the schema
		        $this = result.scopeObj;

		        var insertedIndex = result.insertedIndex;
		        $this.data[insertedIndex].IsNext = isNextTrip || false;
		        $this.data[insertedIndex].IsActive = isActive || false;
		        $this.data[insertedIndex].IsCurrent = isCurrentTrip || false;
		        
		        Trip.getNewTripNumber().then(function (result) {
		            // set any additional parameters we want when creating a new object                    
		            $this.data[insertedIndex].TripNumber = result;

		            //if we inserted a record, save it
		            $this.Save($this.data[insertedIndex], false, true);
		        })
		    });
		};
		/* End Rest Methods */
		/*******************************************/

	}];
