﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Security', 'Modal', 'Base',
    function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Security, Modal, Base) {        
        /*********************************************/
        /* scope vars */
        var _page = $scope.page = $scope.$new();

        var _nextCrewChange = $scope.nextCrewChange = $scope.$new();
        var _currentCrew = $scope.currentCrew = $scope.$new();        
        
        $scope.nextCrewChangeLoaded = false;
        $scope.currentCrewLoaded = false;        

        //tell the rest api what object we're looking for
        _nextCrewChange.classname = 'apm.nextcrewchange'; //case sensitive       
        _nextCrewChange.customTableName = "APM_NextCrewChange";
        _nextCrewChange.dateName = "";

        _currentCrew.classname = 'apm.currentcrew'; //case sensitive       
        _currentCrew.customTableName = "APM_CurrentCrew";
        _currentCrew.dateName = "";

        //datetime picker options and date format
        _page.datetimePickerOpts = Util.dateTimePickerOptions();
        _page.dateFormat = _page.datetimePickerOpts.format;

        /* end scope vars */
        /*********************************************/



        /*********************************************/
        /* initialize */

        //show a status alerts            
        _page.addAlert = function (message, type) {
            _page.alerts = Util.addAlert(message, type);
        };
        _page.closeAlert = function (index) {
            _page.alerts = Util.closeAlert();
        };
        //some messages auto fall off the list, watch for them and then update the alerts
        Util.registerObserverCallback(function () {
            _page.alerts = Util.pageAlerts;
        });
        // end status alerts

        // Make this call on most pages unless there is a special case
        // Setup security and get base information that every page needs            
        Base.get(_currentCrew.classname).then(function (result) {
            //iterate over any objects in the base and attach them to the page scope
            angular.forEach(result, function (value, key) {
                eval("_page." + key + ' = value');
            });
                
            // intialize the page after we have all the data we need loaded            
            $scope.$watch(
                function (scope) {
                    return typeof (_page.roles) === "object"
                            && typeof (_page.employees) === "object"
                            && typeof (_page.arrivedDeparted) === "object"
                },
                function (newVal, oldVal) {
                    if (newVal)
                    {
                        _nextCrewChange.List();                      
                    }
                }
            );

            $scope.$watch('nextCrewChangeLoaded', function (scope) {
                if ($scope.nextCrewChangeLoaded)
                    _page.complete = true;
            });

            
        });

        /* end initialize */
        /*********************************************/


        //********************************************
        // BEGIN Lookup Tables

        // Employee List
        LookupTables.getSystemTable("Employee", "cms.user", "FullName ASC", "ISNULL(UserIsHidden,0)!=1 AND UserEnabled='true'", "UserGUID", "FullName").then(function (result) {
            _page.employees = result;
        });
        LookupTables.getSystemTable("Role", "cms.role", "RoleDisplayName ASC", "RoleName Like 'APM_%'", "RoleDisplayName", "RoleDisplayName").then(function (result) {
            _page.roles = result;
        });

        LookupTables.get('ArrivedDeparted').then(function (result) {
            _page.arrivedDeparted = result;
        });
            
        _page.showLookupData = function (tableName, data, dataId) {                
            return LookupTables.show(tableName, data, dataId);
        }           

        // END Lookup Tables
        //********************************************
        
        /*********************************************/
        /* BEGIN DATE PICKER */

        _page.today = function () {
            _page.viewDate = new Date();
        };

        _page.openDatePicker = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            _page.isDatePickerOpened = true;
        };

        _page.dateOptions = {
            formatYear: 'yyyy',
            formatMonth: 'MM',
            "min-mode": "month",
            "datepicker-mode": "'month'"
        };
        /* END DATE PICKER */
        /*********************************************/

        /* misc methods */
        //are we viewing today?       
        _page.isToday = function () {
            return $filter('date')(_page.viewDate, "MM/yyyy") === $filter('date')(new Date(), "MM/yyyy");
        }

        /*********************************************/
        /* BEGIN REST Methods */

        /*Get Data
            * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST
        */

        //get the deck log for today
        _nextCrewChange.List = function (where) {
            var $this = this;
            RestApi.list($this, where, "NextCrewChangeDateTime DESC").then(function (result) {
                $this.data = result;
                $scope.nextCrewChangeLoaded = true;
            });
        }

        //Insert a new record OR update an existing record
        _nextCrewChange.Save = function (data, index) {
            var $this = this;
            RestApi.save($this, data);
        }
        //Insert a Record, but it doesn't become a true record until we click save
        _nextCrewChange.Insert = function () {
            var $this = this;
            RestApi.insert(_page, $this).then(function (result) {
                //update our scope variable with modified stuff, we're updating the whole object
                // not just the data so we can capture the schema
                $this = result.scopeObj;

                // set any additional parameters we want when creating a new object
                $this.data[result.insertedIndex].NextCrewChangeDateTime = new Date();
            });
        };
        _nextCrewChange.Delete = function (id) {
            var $this = this;
            _page.complete = false;
            RestApi.delete($this, id).then(function (result) {
                _page.complete = true;
                $this.data = result; //get the updated scope data object after deleting an object
            });
        };

        /* End Rest Methods */
        /*********************************************/

    }];
