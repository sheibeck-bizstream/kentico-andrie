module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'Modal', 'Security',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, Modal, Security) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _vesselInfo = $scope.vesselInfo = $scope.$new();


            /* end scope vars */
            /*********************************************/
            //tell the rest api what object we're looking for
            _vesselInfo.classname = 'apm.vesselinformation'; //case sensitive
            _vesselInfo.customTableName = 'APM_VesselInformation';

            // Set security
            // Tell the screen what permissions this user has
            Security.get(_vesselInfo.classname).then(function (result) {
                _page.security = result;

                //initialize the view
                _vesselInfo.List();
            });

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts
            

            /* BEGIN REST Methods */

            //get the deck log for today
            _vesselInfo.List = function (where) {               
                var $this = this;

                //get tug data
                RestApi.resource.query({ objecttype: 'customtableitem.', classname: $this.classname, orderby: "VesselBoatType DESC" }, function (data) {
                    //if we found entries for the vessel information           
                    if (data.length > 1) {
                        $this.data = data;
                    }
                    else { // if we didn't find an entry add one   
                        Util.logError("Vessel Information is incomplete");
                    }

                    _page.complete = true;
                }, function (e) {
                    Util.logError(e);
                });
            }

            //Insert a new record OR update an existing record
            _vesselInfo.Save = function (data) {
                var $this = this;
                RestApi.save($this, data);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _vesselInfo.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;
                });
            };  

        }];
