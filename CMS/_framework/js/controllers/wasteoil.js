﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {
            
            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();

            // Table
            var _dirtyFilter = $scope.dirtyFilter = $scope.$new();
            var _slopDump = $scope.slopDump = $scope.$new();
            var _slopTankSounding = $scope.slopTankSounding = $scope.$new();
            
            reset = function()
            {
                _page.complete = false;
                $scope.dirtyFilterLoaded = false;
                $scope.slopDumpLoaded = false;
                $scope.slopTankSoundingLoaded = false;
            }
            reset();

            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.dateFormat = Util.dateFormat(true);
           
            //tell the rest api what object we're looking for
            _dirtyFilter.classname = "apm.dirtyfilter"; //case sensitive
            _dirtyFilter.customTableName = "APM_DirtyFilter";
            _dirtyFilter.dateName = "DirtyFilterDate";

            _slopDump.classname = "apm.slopdump";
            _slopDump.customTableName = 'APM_SlopDump';
            _slopDump.dateName = "SlopDumpDateTime";

            _slopTankSounding.classname = "apm.sloptanksounding";
            _slopTankSounding.customTableName = "APM_SlopTankSounding";
            _slopTankSounding.dateName = "SlopTankSoundingDateTime";

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_dirtyFilter.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.dirtyFilterTypes) === "object"
                        && typeof (_page.terminalDocks) === "object"
                        && typeof (_page.slopDumpTanks) === "object"
                        && typeof (_page.slopSoundingTanks) === "object"
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";                    
                }, function (newVal, oldVal) {
                    if (newVal)
                    {
                        _dirtyFilter.List();
                        _slopDump.List();
                        _slopTankSounding.List();
                    }
                        
                });
              
                $scope.$watch('page.dateFormat', function (scope) {
                    _page.viewDate = new Date();
                });

                $scope.$watch('dirtyFilterLoaded', function (scope) {
                    if ($scope.dirtyFilterLoaded && $scope.slopDumpLoaded && $scope.slopTankSoundingLoaded)
                        _page.complete = true;
                });

                $scope.$watch('slopDumpLoaded', function (scope) {
                    if ($scope.dirtyFilterLoaded && $scope.slopDumpLoaded && $scope.slopTankSoundingLoaded)
                        _page.complete = true;
                });

                $scope.$watch('slopTankSoundingLoaded', function (scope) {
                    if ($scope.dirtyFilterLoaded && $scope.slopDumpLoaded && $scope.slopTankSoundingLoaded)
                        _page.complete = true;
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        reset();
                        _dirtyFilter.List();
                        _slopDump.List();
                        _slopTankSounding.List();
                    }
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables            

            LookupTables.get('DirtyFilterType').then(function (result) {
                _page.dirtyFilterTypes = result;
            });

            LookupTables.get('TerminalDockName', 'TerminalName', 'TerminalName').then(function (result) {
                _page.terminalDocks = result;
            });

            LookupTables.get('SlopDumpTank').then(function (result) {
                _page.slopDumpTanks = result;
            });

            LookupTables.get('SlopSoundingTank').then(function (result) {
                _page.slopSoundingTanks = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                "min-mode": "month",
                "datepicker-mode": "'month'"
            };

            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "MM/yyyy") === $filter('date')(new Date(), "MM/yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _dirtyFilter.List = function (where) {
                var $this = this;
                var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
                var month = parseInt(fViewDate[0]);
                var year = parseInt(fViewDate[1]);
                where = where || "Month(" + $this.dateName + ")=" + month + " AND Year(" + $this.dateName + ")=" + year;

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;
                    $scope.dirtyFilterLoaded = true;
                });            
            }

            _slopDump.List = function (where) {
                var $this = this;
                var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
                var month = parseInt(fViewDate[0]);
                var year = parseInt(fViewDate[1]);
                where = where || "Month(" + $this.dateName + ")=" + month + " AND Year(" + $this.dateName + ")=" + year;

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;
                    $scope.slopDumpLoaded = true;
                });
            }

            _slopTankSounding.List = function (where) {
                var $this = this;
                var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
                var month = parseInt(fViewDate[0]);
                var year = parseInt(fViewDate[1]);
                where = where || "Month(" + $this.dateName + ")=" + month + " AND Year(" + $this.dateName + ")=" + year;

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;
                    $scope.slopTankSoundingLoaded = true;
                });
            }


            //Insert a new record OR update an existing record
            _dirtyFilter.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            _slopDump.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };
          
            _slopTankSounding.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _dirtyFilter.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].DirtyFilterDate = $filter('date')(_page.viewDate, "MM/dd/yyyy");
                });
            };

            _slopDump.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].SlopDumpDateTime = _page.viewDate;
                });
            };

            _slopTankSounding.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].SlopTankSoundingDateTime = _page.viewDate;
                });
            };
           
            //Deletes
            _dirtyFilter.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _slopDump.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _slopTankSounding.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];