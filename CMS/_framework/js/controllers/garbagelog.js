﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
    function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {
        /*********************************************/
        /* scope vars */
        var _page = $scope.page = $scope.$new();
        var _garbageLog = $scope.garbageLog = $scope.$new();

        //tell the rest api what object we're looking for
        _garbageLog.classname = 'apm.garbagelog'; //case sensitive            
        _garbageLog.customTableName = 'APM_GarbageLog';
        /* end scope vars */
        /*********************************************/

        //show a status alerts            
        _page.addAlert = function (message, type) {
            _page.alerts = Util.addAlert(message, type);
        };
        _page.closeAlert = function (index) {
            _page.alerts = Util.closeAlert();
        };
        //some messages auto fall off the list, watch for them and then update the alerts
        Util.registerObserverCallback(function () {
            _page.alerts = Util.pageAlerts;
        });
        // end status alerts

        //datetime picker options and date format
        _page.datetimePickerOpts = Util.dateTimePickerOptions();
        _page.dateFormat = _page.datetimePickerOpts.format;


        /*********************************************/
        /* initialize */

        // Make this call on most pages unless there is a special case
        // Setup security and get base information that every page needs            
        Base.get(_garbageLog.classname).then(function (result) {
            //iterate over any objects in the base and attach them to the page scope
            angular.forEach(result, function (value, key) {
                eval("_page." + key + ' = value');
            });

            // intialize the page after we have all the data we need loaded
            $scope.$watch(function (scope) {
                return typeof (_page.terminalDockNames) === "object"
                    && typeof (_page.crewMembers) === "object"
                    && typeof (_page.viewDate) !== "undefined"
                    && typeof (_page.dateFormat) !== "undefined";                
            }, function (newVal, oldVal) {
                if (newVal)
                    _garbageLog.List();
            });

            $scope.$watch('page.dateFormat', function (scope) {
                _page.viewDate = new Date();
            });

            // intialize the page after we have all the data we need loaded
            $scope.$watch('page.viewDate', function () {
                if (_page.complete)
                {
                    _page.complete = false;
                    _garbageLog.List();
                }
            });
        });

        /* end initialize */
        /*********************************************/


        //********************************************
        // BEGIN Lookup Tables

        // Crew Member List
        LookupTables.GetCrewForDate("CrewMember", "CrewMemberName", "CrewMemberName").then(function (result) {
            _page.crewMembers = result;
        });

        // Docks
        LookupTables.get("TerminalDockName", "TerminalName", "TerminalName").then(function (result) {
            _page.terminalDockNames = result;
        });

        _page.showLookupData = function (tableName, data, dataId) {
            //alert(whereStatement);
            return LookupTables.show(tableName, data, dataId);
        }

        // END Lookup Tables
        //********************************************

        /*********************************************/
        /* BEGIN DATE PICKER */

        _page.today = function () {
            _page.viewDate = new Date();
        };

        _page.openDatePicker = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            _page.isDatePickerOpened = true;
        };

        _page.dateOptions = {
            formatYear: 'yyyy',
            formatMonth: 'MM',
            "min-mode": "month",
            "datepicker-mode": "'month'"
        };
        /* END DATE PICKER */
        /*********************************************/

        /* misc methods */
        //are we viewing today?       
        _page.isToday = function () {
            return $filter('date')(_page.viewDate, "MM/yyyy") === $filter('date')(new Date(), "MM/yyyy");
        }

        /*********************************************/
        /* BEGIN REST Methods */

        /*Get Data
            * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST
        */

        //get the role of the selected crew member
        _garbageLog.UpdateRole = function (data, index) {
            LookupTables.GetCrewForDate("CrewMemberRole", "CrewMemberName", "CrewMemberRole", _page.viewDate).then(function (result) {
                var item = $.grep(result, function (e) {
                    return e.id === data.GarbageLogName;
                });
                if (item.length > 0) {
                    _garbageLog.data[index].GarbageLogTitle = item[0].text;
                }
            });
        };

        //get the deck log for today
        _garbageLog.List = function (where) {
            var $this = this;

            var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
            var month = parseInt(fViewDate[0]);
            var year = parseInt(fViewDate[1]);
            where = where || "Month(GarbageLogDateTime)=" + month + " AND Year(GarbageLogDateTime)=" + year;

            RestApi.list($this, where).then(function (result) {
                $this.data = result;
                _page.complete = true;
            });
        }

        //Insert a new record OR update an existing record
        _garbageLog.Save = function (data, index) {
            var $this = this;
            RestApi.save($this, data);
        };
        //Insert a Record, but it doesn't become a true record until we click save
        _garbageLog.Insert = function () {
            var $this = this;
            RestApi.insert(_page, $this).then(function (result) {
                //update our scope variable with modified stuff, we're updating the whole object
                // not just the data so we can capture the schema
                $this = result.scopeObj;
                $this.data[result.insertedIndex].GarbageLogDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm");
            });
        };

        //Deletes
        _garbageLog.Delete = function (id) {
            var $this = this;
            _page.complete = false;
            RestApi.delete($this, id).then(function (result) {
                _page.complete = true;
                $this.data = result; //get the updated scope data object after deleting an object
            });
        };

        /* End Rest Methods */
        /*********************************************/
}];