﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'VesselForm', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, VesselForm, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _safetyMeeting = $scope.safetyMeeting = $scope.$new();

            $scope.page.viewDate = new Date();
            
            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.dateFormat = _page.datetimePickerOpts.format;


            _page.select2Options = {
                multiple: true,
                allowClear: true,
                minimumInputlength: 2
            };
           

            //tell the rest api what object we're looking for
            _safetyMeeting.classname = 'apm.safetymeeting'; //case sensitive
            _safetyMeeting.customTableName = 'APM_SafetyMeeting';

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_safetyMeeting.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.topicsDiscussed) === "object"
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";                    
                }, function (newVal, oldVal) {
                    if (newVal)
                        _safetyMeeting.List();
                });                            

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete) {
                        _page.complete = false;
                        _safetyMeeting.List();
                    }                        
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables
            LookupTables.get('SafetyMeetingTopicsDiscussed', 'TopicDiscussed', 'TopicDiscussed').then(function (result) {
                _page.topicsDiscussed = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',                
                "min-mode": "year",
                "datepicker-mode": "'year'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "yyyy") === $filter('date')(new Date(), "yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _safetyMeeting.List = function (where) {
                var $this = this;
                var fViewDate = $filter('date')(_page.viewDate, "yyyy");                
                where = where || "Year(SafetyMeetingDateTime)=" + fViewDate;
                var orderby = "SafetyMeetingDateTime";

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    _page.complete = true;
                });
            }            

            //Insert a new record OR update an existing record
            _safetyMeeting.Save = function (data, index) {
                var $this = this;
                //convert the saftey meetings from an array to a string for saving
                data.SafetyMeetingTopicsDiscussed = $filter('viewTags')(data.SafetyMeetingTopicsDiscussed);

                //save the data
                RestApi.save($this, data);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _safetyMeeting.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this, 1).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object
                    $this.data[result.insertedIndex].SafetyMeetingDateTime = new Date();
                    RestApi.save($this, $this.data[result.insertedIndex]);
                });
            };
           
            //Deletes
            _safetyMeeting.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            //TODO: Lookup the GUID of this form by name instead of hard-coding the GUID
            _safetyMeeting.GenerateForm = function (data) {
                //TODO: pass in the class name and lookup the guid on the server side
                VesselForm.export(
                    "f74290d5-2e94-4094-a338-a345c0b2c915",
                    null,
                    data.SafetyMeetingDateTime,
                    data.ItemGUID //report on a specific item
                );
            }

            /* End Rest Methods */
            /*********************************************/
        }];