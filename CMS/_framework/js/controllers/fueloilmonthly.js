﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();

            // Table references
            var _lastFOBunker = $scope.lastFOBunker = $scope.$new();
            var _fOGallonsTransferred = $scope.fOGallonsTransferred = $scope.$new();
            var _monthlyFuel = $scope.monthlyFuel = $scope.$new();
            var _fOSoundings = $scope.fOSoundings = $scope.$new();

            _page.select2Options = {
                allowClear: true
            };

            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.dateFormat = _page.datetimePickerOpts.format;

            reset = function () {
                _page.complete = false;
                $scope.lastFOBunkerLoaded = false;
                $scope.fOGallonsTransferredLoaded = false;
                $scope.fOSoundingsLoaded = false;
                $scope.monthlyFuelLoaded = false;
            };

            reset();

            //tell the rest api what object we're looking for
            // gallons bunkered
            _lastFOBunker.classname = 'apm.lastfobunker'; //case sensitive
            _lastFOBunker.customTableName = 'APM_LastFOBunker';
            _lastFOBunker.dateName = 'LastFOBunkerDateTime';

            // gallons transferred
            _fOGallonsTransferred.classname = 'apm.fogallonstransferred'; //case sensitive
            _fOGallonsTransferred.customTableName = 'APM_FOGallonsTransferred';
            _fOGallonsTransferred.dateName = 'FOGGallonsTransferredDateTime';

            // soundings
            _fOSoundings.classname = 'apm.fosoundings'; //case sensitive
            _fOSoundings.customTableName = 'APM_FOSoundings';
            _fOSoundings.dateName = 'FOSoundingsDateTime';

            // fuel totals
            _monthlyFuel.classname = 'apm.monthlyfuel'; //case sensitive
            _monthlyFuel.customTableName = 'APM_MonthlyFuel';
            _monthlyFuel.dateName = '';
                      

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_lastFOBunker.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.fuelOilVendors) === "object"
                        && typeof (_page.boatTypes) === "object"
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined"
                        && typeof (_page.timeLabel) == "object";
                }, function (newVal, oldVal) {                    
                    if (newVal)
                    {
                        _lastFOBunker.List();
                        _fOGallonsTransferred.List();
                        _fOSoundings.List();
                        _monthlyFuel.List();
                    }                        
                });

                $scope.$watch('page.dateFormat', function (scope) {
                    _page.viewDate = new Date();
                    _page.viewMonth = $filter('date')(_page.viewDate, "MM/yyyy").split('/')[0];
                    _page.viewYear = $filter('date')(_page.viewDate, "MM/yyyy").split('/')[1];
                });

                // Watch for everything to be loaded before setting the page to be completed. 
                $scope.$watch('lastFOBunkerLoaded', function (scope) {
                    if ($scope.lastFOBunkerLoaded && $scope.fOGallonsTransferredLoaded
                        && $scope.fOSoundingsLoaded && $scope.monthlyFuelLoaded) {
                        _page.complete = true;
                    }
                });
                $scope.$watch('fOGallonsTransferredLoaded', function (scope) {
                    if ($scope.lastFOBunkerLoaded && $scope.fOGallonsTransferredLoaded
                        && $scope.fOSoundingsLoaded && $scope.monthlyFuelLoaded) {
                        _page.complete = true;
                    }
                });
                $scope.$watch('fOSoundingsLoaded', function (scope) {
                    if ($scope.fOSoundingsLoaded && $scope.fOGallonsTransferredLoaded
                        && $scope.fOSoundingsLoaded && $scope.monthlyFuelLoaded) {
                        _page.complete = true;
                    }
                });
                $scope.$watch('monthlyFuelLoaded', function (scope) {
                    if ($scope.lastFOBunkerLoaded && $scope.fOGallonsTransferredLoaded
                        && $scope.fOSoundingsLoaded && $scope.monthlyFuelLoaded) {
                        _page.complete = true;
                    }
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    _page.viewMonth = $filter('date')(_page.viewDate, "MM/yyyy").split('/')[0];
                    _page.viewYear = $filter('date')(_page.viewDate, "MM/yyyy").split('/')[1];                    
                    if (_page.complete) {

                        reset();
                        $timeout(function () {
                            _lastFOBunker.List();
                            _fOGallonsTransferred.List();
                            _fOSoundings.List();
                            _monthlyFuel.List();
                        }, 100);
                    }
                });
            });


            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables            
            LookupTables.get('TimeLabel').then(function (result) {
                _page.timeLabel = result;
            });

            LookupTables.get('TripInformation', 'TripNumber', 'TripNumber', null, 'IsCurrent desc, RIGHT(TripNumber, 4) DESC, LEFT(TripNumber,3) DESC').then(function (result) {
                _page.tripNumbers = result;
            });

            LookupTables.get('FuelOilVendor').then(function (result) {
                _page.fuelOilVendors = result;
            });

            LookupTables.get('BoatType').then(function (result) {
                _page.boatTypes = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
                _page.viewMonth = $filter('date')(_page.viewDate, "MM/yyyy").split('/')[0];
                _page.viewYear = $filter('date')(_page.viewDate, "MM/yyyy").split('/')[1];
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                "min-mode": "month",
                "datepicker-mode": "'month'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "MM/yyyy") === $filter('date')(new Date(), "MM/yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _lastFOBunker.List = function (where) {
                var $this = this;
                where = where || "Month(" + $this.dateName + ")=" + _page.viewMonth + " AND Year(" + $this.dateName + ")=" + _page.viewYear;
                var orderby = "LastFOBunkerDateTime";

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    $scope.lastFOBunkerLoaded = true;
                });
            }

            _fOGallonsTransferred.List = function (where) {
                var $this = this;              
                where = where || "Month(" + $this.dateName + ")=" + _page.viewMonth + " AND Year(" + $this.dateName + ")=" + _page.viewYear;

                RestApi.list($this, where, $this.dateName).then(function (result) {
                    $this.data = result;
                    $scope.fOGallonsTransferredLoaded = true;
                });
            }
                        
            _fOSoundings.List = function (where, data, skipMonthlyUpdate) {
                var $this = this;                               
                where = where || "Month(" + $this.dateName + ")=" + _page.viewMonth + " AND Year(" + $this.dateName + ")=" + _page.viewYear;

                var orderby = "FOSoundingsVessel,FOSoundingsDateTime ASC, TripNumber ASC";

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;

                    // if we are loaded and if we call this again it should
                    // mean that we added a new entry and we need to sort it.
                    if ($scope.fOSoundingsLoaded && !skipMonthlyUpdate) {
                        _monthlyFuel.Save(data, data.FOSoundingsVessel.toLowerCase(), true);
                    }

                    //mark the soundings as loaded
                    $scope.fOSoundingsLoaded = true;                    
                });
            }

            _monthlyFuel.List = function (where) {
                var $this = this;
                where = where || "Month(MonthlyFuelDateTime)=" + _page.viewMonth + " AND Year(MonthlyFuelDateTime)=" + _page.viewYear;

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    if (result.length === 0) {
                        $this.Insert(_page.viewMonth, _page.viewYear);
                    }
                    else {
                        $scope.monthlyFuelLoaded = true;
                    }                    
                });                
            }

            // get last months ending fuel numbers
            _monthlyFuel.GetLastMonth = function(month, year)
            {                
                var $this = this;

                // javascript months in the Date() object are a 0 based
                // array, so account for that when we attempt to find last month
                month--;

                //get the previous month
                var date = $filter('date')(new Date(year, month-1), "MM/yyyy").split("/"); 
                var month = parseInt(date[0]);
                var year = parseInt(date[1]);
                var where = "Month(MonthlyFuelDateTime)=" + month + " AND Year(MonthlyFuelDateTime)=" + year;

                RestApi.list($this, where).then(function (result) {
                    if (result.length > 0) {                        
                        _monthlyFuel.data[0].MonthlyFuelBargeStartGallons = result[0].MonthlyFuelBargeEndGallons;                        
                        _monthlyFuel.data[0].MonthlyFuelTugStartGallons = result[0].MonthlyFuelTugEndGallons;                        
                    }

                    _monthlyFuel.Save(_monthlyFuel.data[0], 'all', true);
                    $scope.monthlyFuelLoaded = true;
                });
            }

            //Insert a new record OR update an existing record
            _lastFOBunker.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data).then(function (result) {
                    //update monthly number                    
                    _monthlyFuel.Save(data, data.LastFOBunkerVessel.toLowerCase(), true);
                });
            };

            _fOGallonsTransferred.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data).then(function (result) {
                    //update monthly number
                    // transfer affects both vessels so run all to update tug and barge
                    _monthlyFuel.Save(data, 'all', true);
                });
            };

            //Insert a new record OR update an existing record
            _fOSoundings.Save = function (data, index, skipMonthlyUpdate) {
                var $this = this;
                RestApi.save($this, data).then(function (result) {
                    // we need to keep things sorted, so re-run the List()
                    // and inside there we'll determine if we need to
                    // re-calculate the monthly stuff.
                    $this.List("", data, skipMonthlyUpdate);
                });
            };

            //data can be a transferred record or a monthly record. Calculate Usage will find and return the 
            // proper monthly record to pass to the monthly .save() api.
            _monthlyFuel.Save = function (data, type, bRefresh) {
                var $this = this;
                var objMonthly = _monthlyFuel.CalculateUsage(data, (type || "all").toLowerCase());
                RestApi.save($this, objMonthly).then(function (result) {
                    if (bRefresh) $this.List();
                });
            };

            //calculate the new montly LO and monthly burn numbers when we use LO
            _monthlyFuel.CalculateUsage = function (data, type) {
                type = (type || "all").toLowerCase();
                var objMonthly = _monthlyFuel.data[0];
              
                //tug burn
                if (type === "tug" || type === "all") {
                    var monthlyUsage = _monthlyFuel.CalculateVesselTotal("tug");

                    // calculate current amount of fuel remaining on this vessel
                    // after accounting for starting gallons,  transfers, bunkering and soundings
                    objMonthly.MonthlyFuelTugEndGallons = Util.parseFloat(parseFloat(objMonthly.MonthlyFuelTugStartGallons || 0) - Util.parseFloat(monthlyUsage || 0));

                    // calculate current month burn by adding back in the bunker/transfer so we only get consumed fuel
                    var bunkerTransferTotal = _monthlyFuel.CalculateVesselTotal("tug", true);
                    objMonthly.MonthlyFuelTugBurnGallons = Util.parseFloat(parseFloat(objMonthly.MonthlyFuelTugStartGallons || 0) - Util.parseFloat(objMonthly.MonthlyFuelTugEndGallons - bunkerTransferTotal || 0));
                }

                //barge burn
                if (type === "barge" || type === "all") {
                    var monthlyUsage = _monthlyFuel.CalculateVesselTotal("barge");

                    // calculate current amount of fuel remaining on this vessel
                    // after accounting for starting gallons,  transfers, bunkering and soundings
                    objMonthly.MonthlyFuelBargeEndGallons = Util.parseFloat(parseFloat(objMonthly.MonthlyFuelBargeStartGallons || 0) - Util.parseFloat(monthlyUsage || 0));

                    // calculate current month burn by adding back in the bunker/transfer so we only get consumed fuel
                    var bunkerTransferTotal = _monthlyFuel.CalculateVesselTotal("barge", true);
                    objMonthly.MonthlyFuelBargeBurnGallons = Util.parseFloat(parseFloat(objMonthly.MonthlyFuelBargeStartGallons || 0) - Util.parseFloat(objMonthly.MonthlyFuelBargeEndGallons - bunkerTransferTotal || 0));
                }

                //get the total burn for the month from both the barge and tug
                objMonthly.MonthlyFuelTotalBurnGallons = objMonthly.MonthlyFuelBargeBurnGallons + objMonthly.MonthlyFuelTugBurnGallons;

                //return the monthly data object
                return objMonthly;
            };

            //calculate the total used fuel for tug/barge
            // we are calculating how much fuel this particular vessel used which is
            // why we are subtracting from the total when we ADD to the vessel
            // and we add to the total when SUBTRACTING from the vessel
            _monthlyFuel.CalculateVesselTotal = function (type, isburn) {
                type = type.toLowerCase();
                var objMonthly = _monthlyFuel.data[0];
                var monthlyUsage = 0;

                 //sounding -- this already accounts for bunkering and transfers!
                // Subtract any fuel that was consumed on the vessel during operation                
                if (_fOSoundings.data) {
                    var remainingFuel = 0;
                    var consumedFuel = 0;
                    if (type === "tug") {
                        remainingFuel = objMonthly.MonthlyFuelTugStartGallons;
                    }
                    else {
                        remainingFuel = objMonthly.MonthlyFuelBargeStartGallons;
                    }

                    // track the consumed fuel and the remaining fuel for the current vessel type
                    for (var c = 0; c < _fOSoundings.data.length; c++) {
                        var sounding = _fOSoundings.data[c];
                        if (sounding.FOSoundingsVessel.toLowerCase() === type) {                            
                            sounding.FOSoundingsVesselConsumed = Util.parseFloat(remainingFuel - sounding.FOSoundingsVesselOnboard);
                            remainingFuel -= sounding.FOSoundingsVesselConsumed;
                            sounding.FOSoundingsVesselOnboard = remainingFuel;

                            if (!isburn) {
                                monthlyUsage += Util.parseFloat(sounding.FOSoundingsVesselConsumed);
                            
                                // save the sounding data to update remaining/consumed
                                // but don't re-run the monthly calculations or we'll get an
                                // infinite loop
                                _fOSoundings.Save(sounding, c, true);
                            }
                            else if (sounding.FOSoundingsVesselConsumed > 0)
                            {
                                monthlyUsage += Util.parseFloat(sounding.FOSoundingsVesselConsumed);
                            }
                        }                       
                    }
                }

                return monthlyUsage;
            }

            //Insert a Record, but it doesn't become a true record until we click save
            _lastFOBunker.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].LastFOBunkerDateTime = _page.viewDate;
                });
            };

            _fOGallonsTransferred.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].FOGGallonsTransferredDateTime = _page.viewDate;
                });
            };

            _fOSoundings.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object 
                    $this.data[result.insertedIndex].FOSoundingsDateTime = _page.viewDate;
                });
            };

            _monthlyFuel.Insert = function (month, year) {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object
                    $this.data[result.insertedIndex].MonthlyFuelDateTime = _page.viewDate;
                                        
                    //get last months fuel and fill in the numbers 
                    // from month end to month begin
                    _monthlyFuel.GetLastMonth(month, year);                    
                });
            };

            //Deletes
            _lastFOBunker.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;

                    $this.data = result; //get the updated scope data object after deleting an object

                    //update monthly numbers
                    var data = _monthlyFuel.data[0];
                    _monthlyFuel.Save(data, data.LastFOBunkerVessel.toLowerCase(), true);
                });
            };

            _fOGallonsTransferred.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;

                    $this.data = result; //get the updated scope data object after deleting an object

                    //update monthly numbers
                    var data = _monthlyFuel.data[0];

                    //transferring is from 1 vessel to another, so run all because we need tug and barge
                    _monthlyFuel.Save(data, 'all', true);
                });
            };

            _fOSoundings.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object

                    //update monthly numbers
                    var data = _monthlyFuel.data[0];
                    _monthlyFuel.Save(data, data.FOSoundingsVessel.toLowerCase(), true);
                });
            };

            _monthlyFuel.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object                    
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];