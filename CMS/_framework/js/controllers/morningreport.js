﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {
            
            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();

            // Table
            var _morningReport = $scope.morningReport = $scope.$new();

            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.datePickerOpts = Util.dateTimePickerOptions(true);
            _page.dateFormat = Util.dateFormat(true);

            //tell the rest api what object we're looking for
            _morningReport.classname = "apm.morningreport"; //case sensitive
            _morningReport.customTableName = "APM_MorningReport";
            _morningReport.dateField = "MorningReportDateTime";

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_morningReport.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined"
                        && typeof (_page.vesselConfigurations) === "object"
                        && typeof (_page.departureTypes) === "object"
                        && typeof (_page.windDirections) === "object"
                        && typeof (_page.windSpeeds) === "object"
                        && typeof (_page.seaStates) === "object"
                        && typeof (_page.terminalDocks) === "object";
                }, function (newVal, oldVal) {                     
                    if (newVal) {
                        _morningReport.List();
                    }
                });
              
                $scope.$watch('page.dateFormat', function (scope) {
                    _page.viewDate = $filter('date')(new Date(), _page.dateFormat);
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                        _morningReport.List();
                });

                $scope.$watch('morningReport.data[0].MorningReportNextLastDelayTime', function (scope) {
                    if (_page.complete)
                        _morningReport.CalculateDateTimes(_morningReport.data[0]);
                });
                $scope.$watch('morningReport.data[0].MorningReportNextLastTransitTime', function (scope) {
                    if (_page.complete)
                        _morningReport.CalculateDateTimes(_morningReport.data[0]);
                });
                $scope.$watch('morningReport.data[0].MorningReportNextLastDelayTime', function (scope) {
                    if (_page.complete)
                        _morningReport.CalculateDateTimes(_morningReport.data[0]);
                });
                $scope.$watch('morningReport.data[0].MorningReportNextDuePortLoadDischargeTime', function (scope) {
                    if (_page.complete)
                        _morningReport.CalculateDateTimes(_morningReport.data[0]);
                });
                $scope.$watch('morningReport.data[0].MorningReportNextDuePortDelayTime', function (scope) {
                    if (_page.complete)
                        _morningReport.CalculateDateTimes(_morningReport.data[0]);
                });
                $scope.$watch('morningReport.data[0].MorningReportNextDepartureTransitTime', function (scope) {
                    if (_page.complete)
                        _morningReport.CalculateDateTimes(_morningReport.data[0]);
                });
                $scope.$watch('morningReport.data[0].MorningReportNextDepartureDelayTime', function (scope) {
                    if (_page.complete)
                        _morningReport.CalculateDateTimes(_morningReport.data[0]);
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables            

            // Vessel configuration - Pushing/Towing       
            LookupTables.get('VesselConfiguration').then(function (result) {
                _page.vesselConfigurations = result;
            });

            // Departure Types - Next/Last
            LookupTables.get('DepartureType').then(function (result) {
                _page.departureTypes = result;
            });

            // Wind Direction           
            LookupTables.get('WindDirection', 'WindDirection', 'WindDirection', null, 'ItemOrder').then(function (result) {
                _page.windDirections = result;
            });            

            // Wind Speed
            LookupTables.get('WindSpeed', 'WindSpeed', 'WindSpeed', null, 'ItemOrder').then(function (result) {
                _page.windSpeeds = result;
            });            

            // Sea State
            LookupTables.get('SeaState', 'SeaState', 'SeaState', null, 'ItemOrder').then(function (result) {
                _page.seaStates = result;
            });

            // Terminal Docks
            LookupTables.get('TerminalDockName', 'TerminalName', 'TerminalName').then(function (result) {
                _page.terminalDocks = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                formatDay: 'dd',
                "min-mode": "day"                
            };

            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "MM/dd/yyyy") === $filter('date')(new Date(), "MM/dd/yyyy");
            }

            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the morning report
            _morningReport.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, _page.dateFormat);
                where = where || $this.dateField + " >= '" + fViewDate + " 12:00:00 AM' and " + $this.dateField + " <= '" + fViewDate + " 11:59:59 PM'";
                var topn = 1;
                var orderby = "ItemID desc"
                RestApi.list($this, where, orderby, topn).then(function (result) {
                    $this.data = result;

                    //if we don't actually have a morning report for today add one
                    if (!result.length > 0)
                    {
                        $this.Insert();
                    }

                    _page.complete = true;
                });            
            }

            //Insert a new record OR update an existing record
            _morningReport.Save = function (data, index) {
                var $this = this;

                var calculatedTimes = $this.CalculateDateTimes(data);
                RestApi.save($this, calculatedTimes);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _morningReport.Insert = function () {
                var $this = this;

                RestApi.insert(_page, $this).then(function (result) {
                    var currentDate = new Date(_page.viewDate);
                    var currentDateString = currentDate.format("MM/dd/yyyy");
                    var yesterdaysReport = { data: "" };
                    var fViewDate = $filter('date')(_page.viewDate, _page.dateFormat);
                    var ywhere = $this.dateField + " >= DATEADD( DAY, -1, '" + fViewDate + " 12:00:00 AM') and " + $this.dateField + " <= DATEADD( DAY, -1, '" + fViewDate + " 11:59:59 PM')";
                    RestApi.list($this, ywhere, null, 1).then(function (yResult) {
                        yesterdaysReport.data = yResult;
                        //If we do find yesterdays data, then we want to copy that data over onto todays object
                        //Since there will only ever be one morning report at a time, I'm just selecting the first one
                        if (yesterdaysReport.data && yesterdaysReport.data.length > 0 && typeof (yesterdaysReport.data[0].ItemGUID) !== 'undefined') {
                            $this.data[0] = angular.copy(yesterdaysReport.data[0]);
                            $this.data[0].ItemGUID = Util.getEmptyGuid;
                            $this.data[0].ItemID = 0;
                            $this.data[0].dateField = fViewDate;
                        }
                            //If we don't find yesterday's data, then we want to insert a new record that is blank.
                        else {
                            //update our scope variable with modified stuff, we're updating the whole object
                            // not just the data so we can capture the schema
                            $this = result.scopeObj;
                            // Run calculations for the first time
                            $this.CalculateDateTimes($this.data[0]);
                            // set any additional parameters we want when creating a new object
                            $this.data[result.insertedIndex].MorningReportNextLastTransitTime = 0;
                            $this.data[result.insertedIndex].MorningReportNextLastDelayTime = 0;
                            $this.data[result.insertedIndex].MorningReportNextDuePortLoadDischargeTime = 0;
                            $this.data[result.insertedIndex].MorningReportNextDuePortDelayTime = 0;
                            $this.data[result.insertedIndex].MorningReportNextDepartureTransitTime = 0;
                            $this.data[result.insertedIndex].MorningReportNextDepartureDelayTime = 0;
                        }

                        //no matter what day we grab, we want the date they are looking at to be there on insert                                               
                        $this.data[result.insertedIndex].MorningReportDateTime = fViewDate;

                        var today = Util.getDate(false);
                        $this.data[result.insertedIndex].MorningReportNextLastDateTime = today;
                        $this.data[result.insertedIndex].MorningReportNextDuePortDateTime = today;
                        $this.data[result.insertedIndex].MorningReportNextDepartureDateTime = today;
                    });
                });
            };
           
            //Deletes
            _morningReport.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };


            /* End Rest Methods */
            /*********************************************/


            /* Calculated methods*/
            /*********************************************/
            _morningReport.CalculateDateTimes = function (data) {
                if (data && data.MorningReportNextLastDateTime) {
                    data.MorningReportNextDuePortDateTime = _morningReport.getNextDateTime(data.MorningReportNextLastDateTime, data.MorningReportNextLastTransitTime || 0, data.MorningReportNextLastDelayTime || 0);
                    data.MorningReportNextDepartureDateTime = _morningReport.getNextDateTime(data.MorningReportNextDuePortDateTime, data.MorningReportNextDuePortLoadDischargeTime || 0, data.MorningReportNextDuePortDelayTime || 0);
                    data.MorningReportNextDestinationETA = _morningReport.getNextDateTime(data.MorningReportNextDepartureDateTime, data.MorningReportNextDepartureTransitTime || 0, data.MorningReportNextDepartureDelayTime || 0);

                    return data;
                }
            };

            // add the transite time + the delay time to a date object to calculate the next date
            _morningReport.getNextDateTime = function(sDate, transitTime, delayTime)
            {
                var minutesToAdd = _morningReport.getMinutesFromFloat(transitTime) + _morningReport.getMinutesFromFloat(delayTime);
                var hours = parseInt(minutesToAdd / 60);
                var minutes = parseInt(minutesToAdd % 60);
               
                var startDate = new Date(sDate);
                startDate.setHours(startDate.getHours() + hours, startDate.getMinutes() + minutes);

                return startDate;
            }

            //return the total minutes from a float
            _morningReport.getMinutesFromFloat = function(fVal)
            {
                var hours = parseInt(fVal); //hours
                var minutes = parseInt((fVal - hours) * 60); //minutes

                var totalMins = (hours * 60) + minutes;

                return totalMins;
            }
        }];