﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {
            
            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            
            var _loads = $scope.load = $scope.$new();
            var _discharges = $scope.discharges = $scope.$new();
            
            $scope.loadLoaded = false;

            _page.viewTripNumber = null;

            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions(true);
            _page.dateFormat = _page.datetimePickerOpts.format;
           
            //tell the rest api what object we're looking for
            _loads.classname = 'apm.cargo'; //case sensitive
            _loads.customTableName = 'APM_Cargo';

            _discharges.classname = 'apm.cargo'; //case sensitive
            _discharges.customTableName = 'APM_Cargo';

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_discharges.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.trips) === "object"
                        && typeof (_page.terminalDocks) === "object"
                        && typeof (_page.viewTripNumber) !== null;
                }, function (newVal, oldVal) {
                    if (newVal)
                        _loads.List();
                });
              
                // Watch for everything to be loaded before setting the page to be completed. 
                $scope.$watch('loadLoaded', function (scope) {
                    if ($scope.loadLoaded)
                        _page.complete = true;
                });

                // intialize the page after we change the trip number
                $scope.$watch('page.viewTripNumber', function (scope) {
                    if (_page.complete) {
                        $scope.loadLoaded = false;
                        _page.complete = false;
                        _loads.List();
                    }
                });

            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables      
           
            LookupTables.get('TerminalDockName', 'TerminalName', 'TerminalName').then(function (result) {
                _page.terminalDocks = result;
            });

            LookupTables.get('TripInformation', 'TripNumber', 'TripNumber', null, 'IsCurrent desc, RIGHT(TripNumber, 4) DESC, LEFT(TripNumber,3) DESC').then(function (result) {
                _page.trips = result;
                // Viewing first trip number. 
                _page.viewTripNumber = result[0].text;

                Util.initSelect2('#vtripnumber', _page.viewTripNumber, false);
            });         

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',                
                "min-mode": "month",
                "datepicker-mode": "'month'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "MM/yyyy") === $filter('date')(new Date(), "MM/yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            _loads.List = function (where) {
                var $this = this;
                where = where || "TripNumber='" + _page.viewTripNumber + "' AND CargoParentGUID is NULL";

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    if (result.length > 0)
                    {
                        $this.hasLoad = true;
                    }
                    else {
                        $this.hasLoad = false;
                    }

                    _discharges.List();                    
                });
            }

            // A list of all of the discharge for the trip. 
            _discharges.List = function (where) {
                var $this = this;
                where = "TripNumber='" + _page.viewTripNumber + "' AND CargoParentGUID is not NULL";

                // Only get the list of discharges that matches the selected trip number. 
                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    _page.complete = true;
                });
            }

            //Insert a new record OR update an existing record
            _discharges.Save = function (data, index) {                
                var $this = this;
                RestApi.save($this, data);
            };
          

            //Insert a Record, but it doesn't become a true record until we click save
            _discharges.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // Update the trip number. 
                    $this.data[result.insertedIndex].TripNumber = _page.viewTripNumber;
                    $this.data[result.insertedIndex].CargoParentGUID = _loads.data[0].ItemGUID;
                });
            };
           
            //Deletes
            _discharges.Delete = function (id) {
                var $this = this;
                _page.complete = false;

                //var _CargoWaterGauge = $scope= $scope.$new();
                var arrayOfObjects = [{ 'customTableName': 'APM_CargoWaterGauge', 'classname': 'APM.CargoWaterGauge', 'objecttype': "customtableitem." },
                                      { 'customTableName': 'APM_CargoDockDraft', 'classname': 'APM.CargoDockDraft', 'objecttype': "customtableitem." },
                                      { 'customTableName': 'APM_CargoDelay', 'classname': 'APM.CargoDelay', 'objecttype': "customtableitem." },
                                      { 'customTableName': 'APM_CargoGeneralInformation', 'classname': 'APM.CargoGeneralInformation', 'objecttype': "customtableitem." },
                                      { 'customTableName': 'APM_CargoDockTime', 'classname': 'APM.CargoDockTime', 'objecttype': "customtableitem." }];
                var whereJoinStatement = "CargoGUID='" + $this.data[id].ItemGUID + "'";
                               
                RestApi.delete($this, id, arrayOfObjects, whereJoinStatement).then(function (result) {                    
                    $this.data = result; //get the updated scope data object after deleting an object

                    //give some time for the child objectst to get deleted before we remove the loading screen
                    $timeout(function () {
                        _page.complete = true;
                    }, 2000);
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];