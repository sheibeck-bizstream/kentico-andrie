﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();

            // Table
            var _lubeOilUsage = $scope.lubeOilUsage = $scope.$new();
            var _lOBunker = $scope.lOBunker = $scope.$new();
            var _monthlyLubeOil = $scope.monthlyLubeOil = $scope.$new();

            reset = function () {
                _page.complete = false;
                $scope.lubeOilUsageLoaded = false;
                $scope.lOBunkerLoaded = false;
                $scope.monthlyLubeOilLoaded = false;
            };

            reset();

            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.dateFormat = _page.datetimePickerOpts.format;


            //tell the rest api what object we're looking for
            _lubeOilUsage.classname = "apm.lubeoilusage"; //case sensitive
            _lubeOilUsage.customTableName = "APM_LubeOilUsage";
            _lubeOilUsage.dateName = "LubeOilUsageDateTime";

            _lOBunker.classname = "apm.lobunker";
            _lOBunker.customTableName = 'APM_LOBunker';
            _lOBunker.dateName = "LOBunkerDateTime";

            _monthlyLubeOil.classname = "apm.monthlylubeoil";
            _monthlyLubeOil.customTableName = "APM_MonthlyLubeOil";
            _monthlyLubeOil.dateName = "";

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_lubeOilUsage.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.lubeOilTypes) === "object"
                        && typeof (_page.fuelOilVendors) === "object"
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";
                }, function (newVal, oldVal) {
                    if (newVal) {
                        _lubeOilUsage.List();
                        _lOBunker.List();
                        _monthlyLubeOil.List();
                    }
                });

                $scope.$watch('page.dateFormat', function (scope) {
                    _page.viewDate = new Date();
                });

                $scope.$watch('lubeOilUsageLoaded', function (scope) {
                    if ($scope.lubeOilUsageLoaded && $scope.lOBunkerLoaded && $scope.monthlyLubeOilLoaded)
                        _page.complete = true;
                });

                $scope.$watch('lOBunkerLoaded', function (scope) {
                    if ($scope.lubeOilUsageLoaded && $scope.lOBunkerLoaded && $scope.monthlyLubeOilLoaded)
                        _page.complete = true;
                });

                $scope.$watch('monthlyLubeOilLoaded', function (scope) {
                    if ($scope.lubeOilUsageLoaded && $scope.lOBunkerLoaded && $scope.monthlyLubeOilLoaded)
                        _page.complete = true;
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete) {
                        reset();

                        $timeout(function () {
                            _lubeOilUsage.List();
                            _lOBunker.List();
                            _monthlyLubeOil.List();
                        }, 100);
                    }
                });
            });


            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables            

            LookupTables.get('LubeOilType').then(function (result) {
                _page.lubeOilTypes = result;
            });

            LookupTables.get('FuelOilVendor').then(function (result) {
                _page.fuelOilVendors = result;
            });

            LookupTables.get('TripInformation', 'TripNumber', 'TripNumber', null, 'IsCurrent desc, RIGHT(TripNumber, 4) DESC, LEFT(TripNumber,3) DESC').then(function (result) {
                _page.tripNumbers = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                "min-mode": "month",
                "datepicker-mode": "'month'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "MM/yyyy") === $filter('date')(new Date(), "MM/yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _lubeOilUsage.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
                var month = parseInt(fViewDate[0]);
                var year = parseInt(fViewDate[1]);
                where = where || "Month(" + $this.dateName + ")=" + month + " AND Year(" + $this.dateName + ")=" + year;

                RestApi.list($this, where, _lubeOilUsage.dateName).then(function (result) {
                    $this.data = result;
                    $scope.lubeOilUsageLoaded = true;
                });
            }

            _lOBunker.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
                var month = parseInt(fViewDate[0]);
                var year = parseInt(fViewDate[1]);
                where = where || "Month(" + $this.dateName + ")=" + month + " AND Year(" + $this.dateName + ")=" + year;

                RestApi.list($this, where, _lOBunker.dateName).then(function (result) {
                    $this.data = result;
                    $scope.lOBunkerLoaded = true;
                });
            }

            _monthlyLubeOil.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, "MM/yyyy").split("/");
                var month = parseInt(fViewDate[0]);
                var year = parseInt(fViewDate[1]);
                where = where || "Month(MonthlyLubeOilDateTime)=" + month + " AND Year(MonthlyLubeOilDateTime)=" + year;
                var orderby = "MonthlyLubeOilStartType ASC";

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    if (result.length === 0) {
                        _monthlyLubeOil.PopulateFromLastMonth(month, year);
                    }
                    else {
                        $scope.monthlyLubeOilLoaded = true;
                    }
                });
            }

            _monthlyLubeOil.PopulateFromLastMonth = function (month, year) {
                var $this = this;

                // javascript months in the Date() object are a 0 based
                // array, so account for that when we attempt to find last month
                month--;

                //get the previous month
                var date = $filter('date')(new Date(year, month - 1), "MM/yyyy").split("/");
                var month = parseInt(date[0]);
                var year = parseInt(date[1]);
                var where = "Month(MonthlyLubeOilDateTime)=" + month + " AND Year(MonthlyLubeOilDateTime)=" + year;
                var orderby = "MonthlyLubeOilStartType ASC";

                RestApi.list($this, where, orderby).then(function (result) {
                    if (result.length > 0) {
                        // iterate over each lube oil and add it to this months.
                        for (var i = 0; i < result.length; i++) {
                            // insert a new item into the monthly lube oil
                            // we want to put them in the same order as the previous month
                            // so wrap them in a timeout so we have a litel bit of breathing room                            
                            setTimeout(_monthlyLubeOil.Insert(result[i]), 100);
                        }
                    }

                    $scope.monthlyLubeOilLoaded = true;
                });
            }

            //Insert a new record OR update an existing record
            _lubeOilUsage.Save = function (data, index) {
                var $this = this;

                RestApi.save($this, data).then(function (result) {
                    //whenever we use LO update our monthly records to record the fule use against our starting monthly gallons
                    //before we save the monthly auto calculate the monthly usage and burn;             
                    _monthlyLubeOil.Save(data, 0, "usage");
                });
            };

            _lOBunker.Save = function (data, index) {
                var $this = this;

                RestApi.save($this, data).then(function (result) {
                    //update our calculations
                    _monthlyLubeOil.Save(data, 0, "bunker");
                });
            };

            //data can be a usage record or a monthly record. Calculate Usage will find and return the 
            // proper monthly record to pass to the monthly .save() api.
            _monthlyLubeOil.Save = function (data, index, type) {
                var $this = this;

                var objMonthly = $this.CalculateUsage(data, type);
                RestApi.save($this, objMonthly).then(function (result) {
                    //clear the dirty flags on the monthly ending and monthly burn since they are autocalculated
                    $(".row-clear-dirty.editable-unsaved").removeClass("editable-unsaved");
                });
            };


            //calculate the new montly LO and monthly burn numbers when we use LO
            _monthlyLubeOil.CalculateUsage = function (data, type) {
                type = (type || "monthly").toLowerCase();

                //what type of oil are we calcualating usage for?
                var loType;
                switch (type) {
                    case "usage":
                        loType = data.LubeOilUsageLOType;
                        break;
                    case "bunker":
                        loType = data.LOBunkerLOType;
                        break;
                    default:
                        loType = data.MonthlyLubeOilStartType;
                        break;
                }
                var monthlyUsage = 0;

                //data can either be a usage/bunker or the monthly, we need to know this before we move on so
                //we can calculat properly
                var objMonthly;
                if (type === "usage" || type === "bunker") {
                    //if we have a lube oil usage/bunker object then go find the monthly object
                    objMonthly = $.grep(_monthlyLubeOil.data, function (obj) {
                        return obj.MonthlyLubeOilStartType === loType;
                    });

                    objMonthly = objMonthly[0];
                }
                else {
                    objMonthly = data;
                }

                //calculate 

                if (_lubeOilUsage.data) {
                    //total all the lube oil usages that have a lube oil type == loType
                    for (var u = 0; u < _lubeOilUsage.data.length; u++) {
                        var usage = _lubeOilUsage.data[u];
                        if (usage.LubeOilUsageLOType === loType) {
                            monthlyUsage += Util.parseFloat(usage.LubeOilUsageConsumed);
                        }
                    }

                }

                var bunkerTypeQty = 0;

                if (_lOBunker.data) {
                    //total all the lube oil bunkers that have a lube oil type == loType
                    for (var b = 0; b < _lOBunker.data.length; b++) {
                        var bunker = _lOBunker.data[b];
                        if (bunker.LOBunkerLOType === loType) {
                            bunkerTypeQty += Util.parseFloat(bunker.LOBunkerQuantity);
                        }
                    }
                }

                //update the monthly numbers               
                var startGallons = objMonthly ? objMonthly.MonthlyLubeOilStartGallons : 0;
                objMonthly.MonthlyLubeOilEndGallons = Util.parseFloat(startGallons - monthlyUsage + bunkerTypeQty);
                objMonthly.MonthlyLubeOilBurnGallons = monthlyUsage;

                return objMonthly;
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _lubeOilUsage.Insert = function () {
                var $this = this;

                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].LubeOilUsageDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm");
                });
            };

            _lOBunker.Insert = function () {
                var $this = this;

                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].LOBunkerDateTime = $filter('date')(new Date(), "MM/dd/yyyy");
                });
            };

            _monthlyLubeOil.Insert = function (objLastMonth) {
                var $this = this;

                RestApi.insert(_page, $this, null, objLastMonth).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema

                    $this = result.scopeObj;
                    $this.data[result.insertedIndex].MonthlyLubeOilDateTime = _page.viewDate;

                    if (!Util.isEmpty(objLastMonth)) {
                        // set the type name
                        $this.data[result.insertedIndex].MonthlyLubeOilStartType = objLastMonth.MonthlyLubeOilStartType;

                        // set the start gallons equal to last months end gallons
                        $this.data[result.insertedIndex].MonthlyLubeOilStartGallons = objLastMonth.MonthlyLubeOilEndGallons;
                        
                        //auto save the row so the calcuations happen
                        setTimeout(function () { $this.Save($this.data[result.insertedIndex]) }, 100);
                    }
                });
            };

            //Deletes
            _lubeOilUsage.Delete = function (id) {
                var $this = this;

                var loType = $this.data[id].LubeOilUsageLOType;
                _page.complete = false;

                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object

                    //if we have a lube oil usage/bunker object then go find the monthly object
                    var objMonthly = $.grep(_monthlyLubeOil.data, function (obj) {
                        return obj.MonthlyLubeOilStartType === loType;
                    });

                    if (objMonthly) {
                        //update monthly numbers
                        _monthlyLubeOil.Save(objMonthly[0], 0, "monthly");
                    }
                });
            };

            _lOBunker.Delete = function (id) {
                var $this = this;

                var loType = $this.data[id].LOBunkerLOType;
                _page.complete = false;

                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object

                    //if we have a lube oil usage/bunker object then go find the monthly object
                    var objMonthly = $.grep(_monthlyLubeOil.data, function (obj) {
                        return obj.MonthlyLubeOilStartType === loType;
                    });

                    if (objMonthly) {
                        //update monthly numbers
                        _monthlyLubeOil.Save(objMonthly[0], 0, "monthly");
                    }
                });
            };

            _monthlyLubeOil.Delete = function (id) {
                var $this = this;

                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];