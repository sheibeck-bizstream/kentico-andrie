module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'VesselForm', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, VesselForm, Modal, Base) {
            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _deckLog = $scope.deckLog = $scope.$new();
            var _deckLogEntry = $scope.deckLogEntry = $scope.$new();

            _deckLogEntry.classname = "apm.decklogentry";
            _deckLogEntry.customTableName = "APM_DeckLogEntry";
            _deckLog.classname = "apm.decklog";
            _deckLog.customTableName = "APM_DeckLog";

            _page.dateFormat = Util.dateFormat(true);
            _page.viewDate = new Date();

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                formatDay: 'dd',
                startingDay: 1
            };

            _page.select2Options = {
                allowClear: true
            };

            /* end scope vars */
            /*********************************************/
            
            /*********************************************/
            /* initialize */

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {                
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_deckLogEntry.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {                    
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(
                    function(scope) {
                        return typeof (_page.windDirections) === "object"
                                        && typeof (_page.windSpeeds) === "object"
                                        && typeof (_page.seaStates) === "object"
                                        && typeof (_page.viewDate) !== "undefined"
                                        && typeof (_page.dateFormat) !== "undefined";
                    },
                    function (newVal, oldVal) {
                        if (newVal)
                            _deckLog.List();
                    }
                );
							
				$scope.$watch('page.dateFormat', function(scope) {
					_page.viewDate = $filter('date')(new Date(), _page.dateFormat);
				});

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        _page.complete = false;
                        _deckLog.List();
                    }
                });
            });
            
            /* end initialize */
            /*********************************************/

           
            //********************************************
            // BEGIN Lookup Tables

            // wind direction            
            LookupTables.get('WindDirection', 'WindDirection', 'WindDirection', null, 'ItemOrder').then(function (result) {
                _page.windDirections = result;
            });
            LookupTables.get('WindSpeed', 'WindSpeed', 'WindSpeed', null, 'ItemOrder').then(function (result) {
                _page.windSpeeds = result;                
            });
            LookupTables.get('SeaState', 'SeaState', 'SeaState', null, 'ItemOrder').then(function (result) {
                _page.seaStates = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */            

            _page.currentDateTime = function () {                
                return $filter('date')(new Date(), Util.dateFormat(false));
            }

            _page.today = function () {
                _page.viewDate = $filter('date')(new Date(), _page.dateFormat);
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, _page.dateFormat) === $filter('date')(new Date(), _page.dateFormat);
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST
             *
             * Here's a sample where statement to get Deck Log items between two dates: "DeckLogDateTime Between DeckLogDateTime Between '06-03-2014 12:00:00 AM' and '06-03-2014 11:59:59 PM'"
             * BETWEEN isn't working for non Admin user in rest. using > and < comparison
            */

            //get the deck log for today
            _deckLog.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, _page.dateFormat);
                var where = where || "DeckLogDateTime >= '" + fViewDate + " 12:00:00 AM' and DeckLogDateTime <= '" + fViewDate + " 11:59:59 PM'";

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    if (result.length > 0) {
                        _deckLogEntry.List();
                    }
                    else { // if we didn't find a deck log make a new one
                        $this.Insert();
                    }

                    _page.complete = true;
                });
            }

            _deckLogEntry.PrintDeckLog = function () {

                //TODO: pass in the class name and lookup the guid on the server side
                VesselForm.export(
                    "93b2a152-4d7f-43af-bfce-3a54ee9938a0",
                    null,
                    _page.viewDate,
                    null
                );
            }

            // Getting the previous MARSEC Level
            _deckLog.PreviousMARSECLevel = function (date, index) {
                var $this = this;

                var previousDay = new Date();
                previousDay.setDate((new Date(date)).getDate() + (-1));

                var formatedDate = (previousDay.getMonth() + 1) + "-" + previousDay.getDate() + "-" + previousDay.getFullYear();

                // I need to get previous formated into an actual date.
                var where = "DeckLogDateTime = '" + formatedDate + "'";
                RestApi.list($this, where).then(function (result) {
                    if ($this.data && $this.data.length > 0) {
                        if (result.length === 0) {
                            $this.data[index].DeckLogMARSECLevel = "01";
                        }
                        else {
                            $this.data[index].DeckLogMARSECLevel = result[0].DeckLogMARSECLevel;
                        }
                        //save this object right away, don't wait for a user to explicity save
                        $this.Save($this.data[index]);
                    }
                });
            }

            _deckLogEntry.List = function (where) {
                var $this = this;
                if (_deckLog.data && _deckLog.data.length > 0) {
                    where = where || "DeckLogItemGUID = '" + _deckLog.data[0].ItemGUID + "'";
                    var orderby = "DeckLogEntryTime ASC";

                    RestApi.list($this, where, orderby).then(function (result) {
                        $this.data = result;
                    });
                }
            }

            //Insert a new record OR update an existing record
            _deckLog.Save = function (data, index) {                
                var $this = this;
                RestApi.save($this, data);
            };

            //Insert a new record OR update an existing record
            _deckLogEntry.Save = function (data, index) {
                var $this = this;

                // highlight marked rows
                var commentRow = $($(".decklogentry-mark")[index]).prev();
                data.DeckLogEntryMark ? commentRow.addClass("highlight") : commentRow.removeClass("highlight");

                //the date we save should be the date we're on with the picker
                data.DeckLogEntryTime = $filter('date')(_page.viewDate, _page.dateFormat) + ' ' + data.DeckLogEntryTime;

                RestApi.save($this, data).then(function () {
                    //after we save pass back just the time so we display properly
                    data.DeckLogEntryTime = moment(data.DeckLogEntryTime).format("HH:mm");
                })
            };

          
            //insert a new log
            _deckLog.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object  
                    $this.data[result.insertedIndex].DeckLogDateTime = _page.viewDate;

                    // Waiting for this function to finish
                    $this.PreviousMARSECLevel(_page.viewDate, result.insertedIndex);

                    // refresh the decklog entry list so we don't get an invalid data object
                    _deckLogEntry.List();
                });
            }
            //Insert a Log Entry Record, but it doesn't become a true record until we click save
            _deckLogEntry.Insert = function () {
                var $this = this;

                if (_deckLog.data && _deckLog.data.length > 0) {
                    RestApi.insert(_page, $this, _deckLog.data[0].ItemGUID).then(function (result) {
                        //update our scope variable with modified stuff, we're updating the whole object
                        // not just the data so we can capture the schema
                        $this = result.scopeObj;

                        // set any additional parameters we want when creating a new object                    
                        $this.data[result.insertedIndex].DeckLogItemGUID = _deckLog.data[0].ItemGUID;

                        // the the current time regardless of the date
                        $this.data[result.insertedIndex].DeckLogEntryTime = moment(new Date()).format("HH:mm");
                    });
                }
            };
           

            //Deletes
            
            _deckLogEntry.Delete = function (id) {
                var $this = this;
                _page.complete = false;                
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/

        }];
