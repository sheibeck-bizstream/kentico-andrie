﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'VesselForm', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, VesselForm, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _safety = $scope.safety = $scope.$new();            
            
            $scope.page.viewDate = new Date();
            
            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.dateFormat = _page.datetimePickerOpts.format;

            _page.selectOptions = {
                allowClear: true,
                maximumSelectionSize: 3
            };

            //tell the rest api what object we're looking for
            _safety.classname = 'apm.safetyreport'; //case sensitive
            _safety.customTableName = 'APM_Safety';
            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_safety.classname).then(function (result) {                
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.safetyTypes) === "object"
                        && typeof (_page.crewMembers) === "object"                        
                        && typeof (_page.forms === "object")
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";                    
                }, function (newVal, oldVal) {
                    if (newVal)
                        _safety.List();
                });                            

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        _page.complete = false;
                        _safety.List();
                    }
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables
            LookupTables.get('SafetyType').then(function (result) {
                _page.safetyTypes = result;
            });

            LookupTables.get('Form', "ItemGUID", "FormName").then(function (result) {
                _page.forms = result;
            });            

            LookupTables.GetCrewForDate("CrewMember", "CrewMemberName", "CrewMemberName").then(function (result) {
                _page.crewMembers = result;
            });          

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',                
                "min-mode": "year",
                "datepicker-mode": "'year'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "yyyy") === $filter('date')(new Date(), "yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the role of the selected crew member
            _safety.UpdateRole = function (data, index) {                
                LookupTables.GetCrewForDate("CrewMemberRole", "CrewMemberName", "CrewMemberRole").then(function (result) {
                    var item = $.grep(result, function (e) {
                        return e.id == data.SafetyName;
                    });
                    if (item[0] != null)
                    {
                        _safety.data[index].SafetyRole = item[0].text;
                    } 
                });
            };

            //get the deck log for today
            _safety.List = function (where) {
                var $this = this;
                var fViewDate = $filter('date')(_page.viewDate, "yyyy");                
                where = where || "Year(SafetyDateTime)=" + fViewDate;
                var orderby = "SafetyDateTime DESC";

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    _page.complete = true;
                });              
            }
          
            //Insert a new record OR update an existing record
            _safety.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            //AH BugzID:50859 - We need to save this right away so that reports will generate for the record no matter when they click the "Generate Form" button
            _safety.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;
                    $this.data[result.insertedIndex].SafetyDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm");
                    RestApi.save($this, $this.data[result.insertedIndex]);
                });
            };

            //Deletes
            _safety.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _safety.GenerateForm = function (data) {
                //find the form guid for the type of form we want to kick out
                var formName = "";
                switch (data.SafetyType)
                {
                    case "Accident":
                        formName = "Vessel Equipment Accident Report";
                        break;

                    default:
                        formName = "Report of Injury or Illness";
                        break;
                }

                var form = $.grep(_page.forms, function (obj) { return obj.text === formName; });
                
                if (form !== null) {
                    VesselForm.export(
                        form[0].id,
                        null,
                        null,
                        data.ItemGUID //report on a specific item
                    );
                }
            }

            /* End Rest Methods */
            /*********************************************/
        }];