﻿module.exports = ['$scope', '$http', '$log', '$filter', '$routeParams', '$location', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, $routeParams, $location, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {
            
            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _cargoLoad = $scope.cargoLoad = $scope.$new();
            var _arrivalLoadDockDrafts = $scope.arrivalLoadDockDrafts = $scope.$new();
            var _departureLoadDockDrafts = $scope.departureLoadDockDrafts = $scope.$new();
            var _loadDockTimes = $scope.loadDockTimes = $scope.$new();
            var _waterGauges = $scope.waterGauges = $scope.$new();
            var _delays = $scope.delays = $scope.$new();
            var _cargoGeneralInformations = $scope.cargoGeneralInformations = $scope.$new();
            _cargoGeneralInformations.calculationScheduled = null;
            
            var reset = function () {
                $scope.cargoLoadLoaded = false;
                $scope.arrivalLoadDockDraftLoaded = false;
                $scope.departureLoadDockDraftLoaded = false;
                $scope.loadDockTimeLoaded = false;
                $scope.waterGaugeLoaded = false;
                $scope.delayLoaded = false;
                $scope.cargoGeneralInformationLoaded = false;
                _page.complete = false;
            }
            reset();

            // If there is a route parameter, set that as the trip number

            _page.viewTripNumber = null;

            _page.select2Options = {
                allowClear: true
            };
            
            //tell the rest api what object we're looking for
            _cargoLoad.classname = 'apm.cargo'; //case sensitive
            _cargoLoad.customTableName = 'APM_Cargo';

            _arrivalLoadDockDrafts.classname = 'apm.cargodockdraft'; //case sensitive
            _arrivalLoadDockDrafts.customTableName = 'APM_CargoDockDraft';

            _departureLoadDockDrafts.classname = 'apm.cargodockdraft'; //case sensitive
            _departureLoadDockDrafts.customTableName = 'APM_CargoDockDraft';

            _loadDockTimes.classname = 'apm.cargodocktime'; //case sensitive
            _loadDockTimes.customTableName = 'APM_CargoDockTime';

            _waterGauges.classname = 'apm.cargowatergauge'; //case sensitive
            _waterGauges.customTableName = 'APM_CargoWaterGauge	';

            _delays.classname = 'apm.cargodelay'; //case sensitive
            _delays.customTableName = 'APM_CargoDelay';

            _cargoGeneralInformations.classname = 'apm.cargogeneralinformation'; //case sensitive
            _cargoGeneralInformations.customTableName = 'APM_CargoGeneralInformation';

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            _page.isLoaded = function () {
                return ($scope.cargoLoadLoaded
                        && $scope.arrivalLoadDockDraftLoaded
                        && $scope.departureLoadDockDraftLoaded
                        && $scope.loadDockTimeLoaded
                        && $scope.waterGaugeLoaded
                        && $scope.delayLoaded
                        && $scope.cargoGeneralInformationLoaded);
            };

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs
            Base.get(_cargoLoad.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof(_page.trips) === "object"
                        && typeof (_page.viewTripNumber) !== null
                        && typeof (_page.cargoTypes) === "object"
                        && typeof (_page.terminalDocks) === "object"
                        && typeof (_page.delayCauses) === "object"
                      //&& typeof (_page.vesselDrafts) === "object"
                        && typeof (_page.timeLabels) === "object"
                        && typeof (_page.customers) === "object";
                }, function (newVal, oldVal) {
                    if (newVal)
                        _cargoLoad.List();
                });

                $scope.$watch('cargoLoadLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('arrivalLoadDockDraftLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('departureLoadDockDraftLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('loadDockTimeLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('waterGaugeLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('delayLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('cargoGeneralInformationLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });               

                $scope.$watch('loadDockTimes.data', function (scope) {
                    if (_cargoLoad.data && _cargoLoad.data.length > 0) {
                        _loadDockTimes.calculateRates();
                    }
                }, true);                               

                $scope.$watch('page.viewTripNumber', function (scope) {
                    if (_page.viewTripNumber && _page.complete) {
                        $location.path('/view/' + _page.viewTripNumber);
                    }
                });

                //update general information calculations
                // whenever cargo tank soundings change 
                // or cargo general information changes
                $scope.$watch('cargoGeneralInformations.data', function (scope) {
                    if (_page.complete) {
                        _cargoLoad.calculateBbls();
                        _loadDockTimes.calculateRates();
                        _cargoGeneralInformations.ScheduleCalculation();
                    }
                }, true);
                
                $scope.$watch('cargoLoad.data', function (scope) {
                    if (_page.complete) {
                        _cargoLoad.calculateBbls();
                        _loadDockTimes.calculateRates();
                        _cargoGeneralInformations.ScheduleCalculation();
                    }
                }, true);
            });

            //calculate load rates
            _loadDockTimes.calculateRates = function () {                
                if (_cargoLoad.data && _cargoLoad.data.length > 0) {
                    var data = _cargoLoad.data[0];

                    data.CargoTotalLoadTime = 0;
                    data.CargoBblsLoadedPerHour = 0;

                    //get the total load time 
                    var totalLoadTime = 0;
                    if (_loadDockTimes.data && _loadDockTimes.data.length > 0) {

                        //find the start cargo and finish cargo events
                        var startCargo = $.grep(_loadDockTimes.data, function (e) {
                            return e.CargoDockTimeLocation === "Start Cargo";
                        });
                        var finishCargo = $.grep(_loadDockTimes.data, function (e) {
                            return e.CargoDockTimeLocation === "Finish Cargo";
                        });
                        
                        if (startCargo.length > 0 && finishCargo.length > 0) {

                            //get the total time between the start cargo and finish cargo
                            var sCargo = startCargo[0].CargoDockTimeDateTime;
                            var fCargo = finishCargo[0].CargoDockTimeDateTime;

                            //make sure the dates/times are not null
                            if (sCargo && fCargo) {
                                //get the total hours.minutes difference
                                totalLoadTime = Util.getDateDiffHoursAsFloat(sCargo, fCargo);

                                //now collect any Stop/Start pairs between these two
                                var startIndex = _loadDockTimes.data.indexOf(startCargo[0]);
                                var finishIndex = _loadDockTimes.data.indexOf(finishCargo[0]);

                                var aryStop = new Array();
                                var aryStart = new Array();

                                for (var i = startIndex; i < finishIndex; i++) {
                                    var dtd = _loadDockTimes.data[i];

                                    if (dtd.CargoDockTimeLocation === "Stop")
                                        aryStop.push(dtd.CargoDockTimeDateTime);

                                    if (dtd.CargoDockTimeLocation === "Restart")
                                        aryStart.push(dtd.CargoDockTimeDateTime);
                                }

                                var totalStopTime = 0;
                                //get the total time between all the start and stop pairs and subtract this from the total time
                                for (var i = 0; i < aryStop.length; i++)
                                {
                                    //make sure we have a matching Start item or we don't have a proper pair
                                    if (aryStart[i])
                                    {
                                        totalStopTime += Util.getDateDiffHoursAsFloat(aryStop[i], aryStart[i]);
                                    }
                                }

                                totalLoadTime -= totalStopTime;
                            }
                        }
                    }

                    if (totalLoadTime > 0) {
                        data.CargoTotalLoadTime = Util.parseFloat(totalLoadTime);
                        data.CargoBblsLoadedPerHour = Util.parseFloat(data.CargoBblsLoaded / totalLoadTime, 2);
                    }                   
                }               
            }

            //get total number of barrels from tanks
            _cargoLoad.calculateBbls = function () {

                if (_cargoLoad.data && _cargoLoad.data.length > 0) {
                    var data = _cargoLoad.data[0];
                    var totalBblsLoaded = 0;
                    var averageTemp = 0;
                    var tankCount = 0;

                    var arrTanks = ["P", "S"];
                    for (var t = 0; t < arrTanks.length; t++) {
                        // 6 tanks on each side
                        for (var n = 1; n <= 6; n++) {

                            //iterate over port and starboard tanks
                            var tankType = eval("data.CargoType" + n + arrTanks[t]);

                            if (!Util.isEmpty(tankType)) {
                                tankCount++;
                                totalBblsLoaded += parseFloat(eval("data.CargoBbls" + n + arrTanks[t]) || 0);
                                averageTemp += parseFloat(eval("data.CargoTemp" + n + arrTanks[t]) || 0);
                            }
                        }
                    }

                    //get obqs
                    // sum them by type in the cargo general information section                    
                    var obqs = 0;
                    for (var g = 0; g < _cargoGeneralInformations.data.length; g++) {
                        obqs += parseFloat(_cargoGeneralInformations.data[g].CargoGeneralInformationOBQs || 0);
                    }
                    
                    data.CargoBblsLoaded = Util.parseFloat(totalBblsLoaded - obqs , 2);
                    data.CargoAverageTemperature = Util.parseFloat(averageTemp / tankCount, 2);
                }               
            }

            // make sure everything is ready before we start running calculations. WE may need to add rows
            // and those rows all need to be ready to go before we calculate.
            _cargoGeneralInformations.ScheduleCalculation = function () {
                if (_cargoGeneralInformations.calculationScheduled === null) {
                    // make sure all the cargo general information items are fully loaded/inserted
                    // or will get some timing issues that may cause extra rows to get added
                    _cargoGeneralInformations.calculationScheduled = setInterval(function () {
                        clearInterval(_cargoGeneralInformations.calculationScheduled);
                        _cargoGeneralInformations.calculationScheduled = null;
                        $timeout(function () {
                            _cargoGeneralInformations.calculate();
                        }, 25);                        
                    }, 1000);
                }
            }

            //make calculations for each type of cargo in the tanks
            _cargoGeneralInformations.calculate = function () {
                if (_cargoGeneralInformations.data && _cargoGeneralInformations.data.length > 0) {                   
                    for (var g = 0; g < _cargoGeneralInformations.data.length; g++) {
                        //get the row we want to calculate on from the general information table                        
                        var tempLoaded = 0;                        
                        var cargoTankTypeCount = 0;
                        var cargoTotalBarrelsTypeCount = 0;

                        //go find any tank that has the same type
                        var arrTanks = ["P", "S"];
                        for (var t = 0; t < arrTanks.length; t++) {
                            // 6 tanks on each side
                            for (var n = 1; n <= 6; n++) {
                                //iterate over port and starboard tanks
                                var tankType = eval("_cargoLoad.data[0].CargoType" + n + arrTanks[t]);

                                if (!Util.isEmpty(tankType)) {
                                    var tankTemp = eval("_cargoLoad.data[0].CargoTemp" + n + arrTanks[t]);
                                    var tankBbls = eval("_cargoLoad.data[0].CargoBbls" + n + arrTanks[t]);
                                    if (tankType === _cargoGeneralInformations.data[g].CargoGeneralInformationType) {
                                        //if we found a tank that has this type of stuff in it then do some calculations
                                        cargoTankTypeCount++;
                                        cargoTotalBarrelsTypeCount += parseFloat(tankBbls);
                                        tempLoaded += parseFloat(tankTemp);
                                    }
                                }
                            }
                        }

                        //set the final values
                        var obqs = parseFloat(_cargoGeneralInformations.data[0].CargoGeneralInformationOBQs || 0);
                        var llbsPerBbl = parseFloat(_cargoGeneralInformations.data[g].CargoGeneralInformationLbsGal || 0) * 42;                        

                        _cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl = Util.parseFloat(llbsPerBbl, 3);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationTemp = cargoTankTypeCount === 0 ? 0 : Util.parseFloat(tempLoaded / cargoTankTypeCount, 2);
                        //NOTE: in the future, we'll need to figure out OBQ over multiple cargo types
                        _cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls = Util.parseFloat(cargoTotalBarrelsTypeCount, 2);

                        _cargoGeneralInformations.data[g].CargoGeneralInformationNetBbls = Util.parseFloat((_cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls * _cargoGeneralInformations.data[g].CargoGeneralInformationFactor) - obqs, 2);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationGrossTons = Util.parseFloat((_cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl * _cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls) / 2000, 2);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationNetTons = Util.parseFloat((_cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl * _cargoGeneralInformations.data[g].CargoGeneralInformationNetBbls) / 2000, 2);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationMetricTons = Util.parseFloat(_cargoGeneralInformations.data[g].CargoGeneralInformationNetTons * 0.907185, 3);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationUSLbs = Util.parseFloat(_cargoGeneralInformations.data[g].CargoGeneralInformationNetBbls * _cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl, 2);


                        //don't do any of this if the current user doesn't have privileges
                        if (!_page.security.modify)
                            return;

                        //save the row
                        // we've got a timing issue that's duplicating some cargo general information rows. The type should NEVER
                        // be null, so we can use that to double check that we have a valid row here.
                        if (!Util.isEmpty(_cargoGeneralInformations.data[g])
                                && !Util.isEmpty(_cargoGeneralInformations.data[g].CargoGeneralInformationType)
                                && _cargoGeneralInformations.calculationScheduled === null) {
                            _cargoGeneralInformations.Save(_cargoGeneralInformations.data[g], g, false);
                        }
                    }
                }
            }

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables      

            LookupTables.get('TimeLabel').then(function (result) {
                _page.timeLabels = result;
            });

            LookupTables.get('CargoType').then(function (result) {
                _page.cargoTypes = result;
            });

            LookupTables.get('Customer', 'CustomerName', 'CustomerName').then(function (result) {
                _page.customers = result;
            });
         
            LookupTables.get('TerminalDockName', 'TerminalName', 'TerminalName').then(function (result) {
                _page.terminalDocks = result;
            });

            // BZS (SMH) 7/20/2015: Feature - Prepopulate Dock Drafts
            // Moving the laoding of this into the _arrivalLoadDockDrafts.List() function
            // so we can autopopulate the list of items for the users
            /*LookupTables.get('VesselDraft').then(function (result) {
                _page.vesselDrafts = result;
            });*/

            LookupTables.get('DelayCause').then(function (result) {
                _page.delayCauses = result;
            });
            
            LookupTables.get('TripInformation', 'TripNumber', 'TripNumber', null, 'IsCurrent desc, RIGHT(TripNumber, 4) DESC, LEFT(TripNumber,3) DESC').then(function (result) {
                _page.trips = result;

                // The value is equal to what's in the querystring at the time. 
                _page.viewTripNumber = $routeParams.id ? $routeParams.id : result[0].text;
                
                Util.initSelect2('#vtripnumber', _page.viewTripNumber, false);
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                "min-mode": "month",
                "datepicker-mode": "'month'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _cargoLoad.List = function (refresh) {
                var $this = this;
                refresh = refresh || false;                
                
                // Will make sure that TripNumber is the same as the cargo and the cargoParentGUID is null.
                where = "TripNumber='" + _page.viewTripNumber + "' AND CargoParentGUID is NULL";

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    $scope.cargoLoadLoaded = true;

                    if (result.length > 0 || refresh) // If there are cargo data, look for all of the data in the sub pages. 
                    {
                        try {
                            var initalWhereCondition = "CargoGUID='" + $this.data[0].ItemGUID + "'";
                            _arrivalLoadDockDrafts.List(initalWhereCondition);
                            _departureLoadDockDrafts.List(initalWhereCondition);
                            _loadDockTimes.List(initalWhereCondition);
                            _waterGauges.List(initalWhereCondition);
                            _delays.List(initalWhereCondition);
                            _cargoGeneralInformations.List(initalWhereCondition);
                        }
                        catch(ex)
                        {
                            // we are having some timing issues that were leading to duplicate inserts
                            // if the result length is zero for some reason then refresh the page
                            window.location.reload(true);
                        }
                    }
                    else {
                        //don't do any of this if the current user doesn't have privileges
                        if (!_page.security.modify)
                            return;

                        // If there are no cargo data, insert and save empty record data                                           
                        $this.Insert();
                    }                    
                });            
            }

            _arrivalLoadDockDrafts.List = function (where) {
                var $this = this;
                var orderby = "CargoDockDraftLocation ASC";
                where = where + " AND CargoDockDraftType='Arrival'";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;

                    //We always have to fill out PF, PA, SF, SA, so prepopulate them if they don't exist
                    LookupTables.get('VesselDraft').then(function (result) {
                        _page.vesselDrafts = result;

                        LookupTables.get('VesselDraft').then(function (result) {
                            _page.vesselDrafts = result;

                            angular.forEach(_page.vesselDrafts, function (draft, key) {
                                var exists = $.grep(_arrivalLoadDockDrafts.data, function (obj) {
                                    return obj.CargoDockDraftLocation === draft.text;
                                });

                                if (exists.length === 0) {
                                    // if the vessel draft doesn't exist in the list then add it
                                    _arrivalLoadDockDrafts.Insert(draft.text);
                                }
                            });

                            $scope.arrivalLoadDockDraftLoaded = true;
                        });

                    });
                });
            }

            _departureLoadDockDrafts.List = function (where) {
                var $this = this;
                var orderby = "CargoDockDraftLocation ASC";
                where = where + " AND CargoDockDraftType='Departure'";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;

                    //We always have to fill out PF, PA, SF, SA, so prepopulate them if they don't exist
                    LookupTables.get('VesselDraft').then(function (result) {
                        _page.vesselDrafts = result;

                        LookupTables.get('VesselDraft').then(function (result) {
                            _page.vesselDrafts = result;

                            angular.forEach(_page.vesselDrafts, function (draft, key) {
                                var exists = $.grep(_departureLoadDockDrafts.data, function (obj) {
                                    return obj.CargoDockDraftLocation === draft.text;
                                });

                                if (exists.length === 0) {
                                    // if the vessel draft doesn't exist in the list then add it
                                    _departureLoadDockDrafts.Insert(draft.text);
                                }
                            });

                            $scope.departureLoadDockDraftLoaded = true;
                        });

                    });
                });
            }

            _loadDockTimes.List = function (where) {
                var $this = this;
                //the order is important because we calculate times
                var orderby = "CargoDockTimeDateTime ASC";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    $scope.loadDockTimeLoaded = true;
                });
            }

            _waterGauges.List = function (where) {
                var $this = this;
                var orderby = "ItemID ASC";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    $scope.waterGaugeLoaded = true;
                });
            }

            _delays.List = function (where) {
                var $this = this;
                var orderby = "ItemID ASC";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    $scope.delayLoaded = true;
                });
            }

            _cargoGeneralInformations.List = function (where) {
                var $this = this;
                RestApi.list($this, where).then(function (result) {
                    $this.data = result;
                    $scope.cargoGeneralInformationLoaded = true;
                });
            }
            
            //Insert a new record OR update an existing record
            _cargoLoad.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data).then(function (result) {
                    //run barrel calculations
                    $this.calculateBbls();

                    //run the general information calculations
                    _cargoGeneralInformations.ScheduleCalculation();

                    //run the load rate calculations
                    _loadDockTimes.calculateRates();
                });                
            };
          
            _arrivalLoadDockDrafts.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            _departureLoadDockDrafts.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data, _departureLoadDockDrafts.classname + "-1");
            };

            _loadDockTimes.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data).then(function (result) {
                    //reorder the list by times ASC
                    $this.data = $filter('orderBy')($this.data, 'CargoDockTimeDateTime', false);
                });
            };

            _waterGauges.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            _delays.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            // save gen information for load
            _cargoGeneralInformations.Save = function (data, index, bCalculate) {
                var $this = this;
                bCalculate === null ? true : bCalculate;

                //recalculate formulas if needed
                if (bCalculate)
                {
                    $this.ScheduleCalculation();
                    //update our data variable now that we've recalculated
                    data = $this.data[index];
                }

                RestApi.save($this, data);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _cargoLoad.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // Update the trip number. 
                    // If there is no trip number in the URL use the most recent trip number / selected trip number. Otherwise, use the trip contained in the URL. 
                    $this.data[result.insertedIndex].TripNumber = _page.viewTripNumber;

                    //save this object right away, don't wait for a user to explicity save
                    $this.Save($this.data[result.insertedIndex]);

                    //now refresh the cargo list
                    $this.List(true);
                });
            };

            _arrivalLoadDockDrafts.Insert = function (dockDraftLocation) {
                var $this = this;
                var autoOpenIndex = (dockDraftLocation || '') !== '' ? -1 : null;
                RestApi.insert(_page, $this, _cargoLoad.data[0].ItemGUID, autoOpenIndex).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object
                    $this.data[result.insertedIndex].CargoGUID = _cargoLoad.data[0].ItemGUID;
                    $this.data[result.insertedIndex].CargoDockDraftType = 'Arrival';
                    
                    if (!Util.isEmpty(dockDraftLocation))
                    {
                        $this.data[result.insertedIndex].CargoDockDraftLocation = dockDraftLocation;
                    }
                });
            };

            _departureLoadDockDrafts.Insert = function (dockDraftLocation) {
                var $this = this;
                var autoOpenIndex = (dockDraftLocation || '') !== '' ? -1 : null;
                //added a "-1" to the classname so we can distinguish between a LoadDockDraft and DischargeDockDraft 
                //when the UI wants to hilight a row or open a row
                RestApi.insert(_page, $this, _cargoLoad.data[0].ItemGUID, autoOpenIndex, null, $this.classname + '-1').then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].CargoGUID = _cargoLoad.data[0].ItemGUID;
                    $this.data[result.insertedIndex].CargoDockDraftType = 'Departure';
                    
                    if (!Util.isEmpty(dockDraftLocation)) {
                        $this.data[result.insertedIndex].CargoDockDraftLocation = dockDraftLocation;
                    }
                });
            };

            _loadDockTimes.Insert = function () {
                var $this = this;
                RestApi.insert(_page, _loadDockTimes, _loadDockTimes.classname, _cargoLoad.data[0].ItemGUID).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    _loadDockTimes = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    _loadDockTimes.data[result.insertedIndex].CargoGUID = _cargoLoad.data[0].ItemGUID;
                    _loadDockTimes.data[result.insertedIndex].TripNumber = _page.viewTripNumber;
                    _loadDockTimes.data[result.insertedIndex].CargoDockTimeDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm");
                });
            };

            _waterGauges.Insert = function () {
                var $this = this;
                RestApi.insert(_page, _waterGauges, _waterGauges.classname, _cargoLoad.data[0].ItemGUID).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    _waterGauges = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    _waterGauges.data[result.insertedIndex].CargoGUID = _cargoLoad.data[0].ItemGUID;
                    _waterGauges.data[result.insertedIndex].TripNumber = _page.viewTripNumber;
                });
            };

            _delays.Insert = function () {
                var $this = this;
                RestApi.insert(_page, _delays, _delays.classname, _cargoLoad.data[0].ItemGUID).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    _delays = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    _delays.data[result.insertedIndex].CargoGUID = _cargoLoad.data[0].ItemGUID;
                    _delays.data[result.insertedIndex].TripNumber = _page.viewTripNumber;
                });

            };

            _cargoGeneralInformations.Insert = function () {
                var $this = this;
                RestApi.insert(_page, _cargoGeneralInformations, _cargoGeneralInformations.classname, _cargoLoad.data[0].ItemGUID).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    _cargoGeneralInformations = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    _cargoGeneralInformations.data[result.insertedIndex].CargoGUID = _cargoLoad.data[0].ItemGUID;
                    _cargoGeneralInformations.data[result.insertedIndex].TripNumber = _page.viewTripNumber;                    
                });
            };

            //Deletes
            _cargoLoad.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _arrivalLoadDockDrafts.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _departureLoadDockDrafts.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _loadDockTimes.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                    $this.calculateRates();
                });
            };

            _waterGauges.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _delays.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _cargoGeneralInformations.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];