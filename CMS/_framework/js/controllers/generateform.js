﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Security', 'Modal', 'VesselForm',
    function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Security, Modal, VesselForm) {
        /*********************************************/
        /* scope vars */
        var _page = $scope.page = $scope.$new();
        var _vesselForm = $scope.vesselForm = $scope.$new();
                
        _page.viewDate = new Date();

        _vesselForm.data = [{ formGuid: "", tripNumber: "" }];
        
        //setup security
        _page.security = $scope.$new();
        _page.security.modify = true; //anyone can run a report

        //datetime picker options and date format
        _page.datetimePickerOpts = Util.dateTimePickerOptions(true);
        _page.dateFormat = _page.datetimePickerOpts.format;


        _page.select2Options = {
            allowClear: true
        };       


        /* end scope vars */
        /*********************************************/
        

        /*********************************************/
        /* initialize */

        //show a status alerts            
        _page.addAlert = function (message, type) {
            _page.alerts = Util.addAlert(message, type);
        };
        _page.closeAlert = function (index) {
            _page.alerts = Util.closeAlert();
        };
        //some messages auto fall off the list, watch for them and then update the alerts
        Util.registerObserverCallback(function () {
            _page.alerts = Util.pageAlerts;
        });
        // end status alerts

        // intialize the page after we have all the data we need loaded            
        $scope.$watch(
            function (scope) {
                return typeof (_page.forms) === "object"
                    && typeof (_page.trips) === "object"
            },
            function (newVal, oldVal) {
                _page.complete = newVal;                    
            }
        );

        /* end initialize */
        /*********************************************/

        /*********************************************/
        /* BEGIN DATE PICKER */

        _page.today = function () {
            _page.viewDate = $filter('date')(new Date(), _page.dateFormat);
        };

        _page.openDatePicker = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            _page.isDatePickerOpened = true;
        };

        _page.dateOptions = {
            formatYear: 'yyyy',
            formatMonth: 'MM',
            formatDay: 'dd',
            startingDay: 1
        };
        /* END DATE PICKER */
        /*********************************************/


        //********************************************
        // BEGIN Lookup Tables

        // Employee List
        LookupTables.get("Form", "ItemGUID", "FormName", "FormIsSingleItem != 1", "FormName").then(function (result) {
            _page.forms = result;
            Util.initSelect2('#vform', _vesselForm.data.formGuid, false);            
        });       

        LookupTables.get("TripInformation", "TripNumber", "TripNumber", null, "IsCurrent desc, RIGHT(TripNumber, 4) DESC, LEFT(TripNumber,3) DESC").then(function (result) {
            _page.trips = result;
            //default the dropdown list to the first item in our trip list
            _vesselForm.data.tripNumber = result[0].text;
            
            Util.initSelect2('#vtripnumber', _vesselForm.data.tripNumber, false);
        });       

        _page.showLookupData = function (tableName, data, dataId) {
            return LookupTables.show(tableName, data, dataId);
        }
        
        // END Lookup Tables
        //********************************************
          

        //********************************************
        // BEGIN Methods

        _page.GenerateForm = function ()
        {
            var vf = angular.element("#vesselFormName");
            if (!_vesselForm.data.formGuid) {
                _page.addAlert("You must choose a vessel form");
                vf.addClass("has-error").addClass("has-feedback");
            }
            else {
                vf.removeClass("has-error").removeClass("has-feedback");
                VesselForm.export(
                    _vesselForm.data.formGuid, //guid of the form we want to save
                    _vesselForm.data.tripNumber, //get data by trip number
                    _page.viewDate, //get data by date
                    "" //we don't specify a particular item from the generate form page as a parameter
                );
            }
        }

        /* END Methods */
        /*********************************************/

    }];
