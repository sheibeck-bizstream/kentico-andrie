﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {


            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _fccLog = $scope.fccLog = $scope.$new();            
                       
            //datetime picker options and date format                        
            _page.dateFormat = Util.dateFormat(true);
            _page.viewDate = new Date();
            
            //tell the rest api what object we're looking for
            _fccLog.classname = 'apm.fccoperatorlog'; //case sensitive
            _fccLog.customTableName = 'APM_FCCOperatorLog';

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_fccLog.classname).then(function (result) {                
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });
               
                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.crewMembers) === "object"                        
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";
                }, function (newVal, oldVal) {
                    if (newVal)
                        _fccLog.List();
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        _page.complete = false;
                        _fccLog.List();
                    }
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables
            LookupTables.GetCrewForDate("CrewMember", "CrewMemberName", "CrewMemberName", _page.viewDate).then(function (result) {
                _page.crewMembers = result;
            });
         
            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }
          
            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                formatDay: 'dd',
                startingDay: 1
            };

            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "MM/dd/yyyy") === $filter('date')(new Date(), "MM/dd/yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST
             *
             * Here's a sample where statement to get Deck Log items between two dates: "DeckLogDateTime Between DeckLogDateTime Between '06-03-2014 12:00:00 AM' and '06-03-2014 11:59:59 PM'"
            */

            //get the deck log for today
            _fccLog.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, _page.dateFormat);
                var where = where || "FCCOperatorLogDateTime >= '" + fViewDate + " 12:00:00 AM' and FCCOperatorLogDateTime <= '" + fViewDate + " 11:59:59 PM'";

                var orderby = "FCCOperatorLogDateTime";

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    _page.complete = true;
                });
                LookupTables.GetCrewForDate("CrewMember", "CrewMemberName", "CrewMemberName", _page.viewDate, true).then(function (result) {
                    _page.crewMembers = result;
                });
            }            

            //Insert a new record OR update an existing record
            _fccLog.Save = function (data, index) {
                var $this = this;

                //the date we save should be the date we're on with the picker
                data.FCCOperatorLogDateTime = $filter('date')(_page.viewDate, _page.dateFormat) + ' ' + data.FCCOperatorLogDateTime;
                
                RestApi.save($this, data).then(function () {
                    //after we save pass back just the time so we display properly
                    data.FCCOperatorLogDateTime = moment(data.FCCOperatorLogDateTime).format("HH:mm");
                })
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _fccLog.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    LookupTables.getCurrentUser().then(function(currentUser){
                        //update our scope variable with modified stuff, we're updating the whole object
                        // not just the data so we can capture the schema
                        $this = result.scopeObj;

                        // set any additional parameters we want when creating a new object                                
                        $this.data[result.insertedIndex].FCCOperatorLogOperator = currentUser;
                        // the the current time regardless of the date
                        $this.data[result.insertedIndex].FCCOperatorLogDateTime = moment(new Date()).format("HH:mm");
                    });
                });
            };

            //Deletes
            _fccLog.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];
