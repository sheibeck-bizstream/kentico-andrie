﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Security', 'Modal', 'Base',
    function ($scope, $http, $log, $filter, $timeout, RestApi, RecordSchemaApi, Util, LookupTables, Security, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _currentcrew = $scope.currentcrew = $scope.$new();
            var _lastcrew = $scope.lastcrew = $scope.$new();

            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();            
            _page.dateFormat = Util.dateFormat(true);
           
            //tell the rest api what object we're looking for
            _currentcrew.classname = 'apm.currentcrew'; //case sensitive
            _currentcrew.customTableName = 'APM_CurrentCrew';
            _currentcrew.dateField = 'CurrentCrewDateTime';

            _page.select2Options = {
                allowClear: true
            };

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_currentcrew.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.viewDate) !== "undefined"                        
                        && typeof (_page.dateFormat) !== "undefined";
                }, function (newVal, oldVal) {
                    if (newVal)
                        _currentcrew.List();
                });
              
                $scope.$watch('page.dateFormat', function (scope) {
                    _page.viewDate = new Date();
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        _page.complete = false;
                        // This is interferring with the inital insert. 
                        _currentcrew.List();
                    }
                });                
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables            


            // Employee List
            LookupTables.getSystemTable("Employee", "cms.user", "FullName ASC", "ISNULL(UserIsHidden,0)!=1 AND UserEnabled='true'", "UserGUID", "FullName").then(function (result) {
                _page.employees = result;
            });
            LookupTables.getSystemTable("Role", "cms.role", "RoleDisplayName ASC", "RoleName Like 'APM_%'", "RoleDisplayName", "RoleDisplayName").then(function (result) {
                _page.roles = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }         

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            //set limits on the datepicker
            var minDate = new Date();
            var maxDate = new Date();
            maxDate.setDate(minDate.getDate() + 2);
            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                formatDay: 'dd',
                "min-mode": "day"              
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, _page.dateFormat) === $filter('date')(new Date(), _page.dateFormat);
            }

            //record the name of the crew member when we change the drop down list
            // so we have the name AND the guid
            _currentcrew.updateEmployeeName = function (index) {
                try {
                    var item = $.grep(_page.employees, function (e) {
                        return e.id == _currentcrew.data[index].CurrentCrewUserGUID;
                    });
                    _currentcrew.data[index].CurrentCrewName = item[0].text;
                }
                catch (ex) { }
            }

            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _currentcrew.List = function () {
                var $this = this;
                var fViewDate = $filter('date')(_page.viewDate, _page.dateFormat).split("/");
                var month = parseInt(fViewDate[0]);
                var day = parseInt(fViewDate[1]);
                var year = parseInt(fViewDate[2]);
                where = "CurrentCrewDateTime IS NOT NULL AND Month(CurrentCrewDateTime)=" + month + " AND Day(CurrentCrewDateTime)=" + day + " AND Year(CurrentCrewDateTime)=" + year;

                var orderby = " CASE WHEN CurrentCrewRole = 'Captain' THEN 1" +
                                    " WHEN CurrentCrewRole = 'Mate' THEN 2" + 
                                    " WHEN CurrentCrewRole = 'Chief Engineer' THEN 3" + 
                                    " WHEN CurrentCrewRole = 'Assistant Engineer' THEN 4" + 
                                    " WHEN CurrentCrewRole = 'Able Body Seamen' OR CurrentCrewRole = 'Able Seamen' THEN 5" + 
                                    " WHEN CurrentCrewRole = 'Ordinary Seamen' THEN 6" + 
                                    " WHEN CurrentCrewRole = 'Extra' THEN 7" + 
                                    " ELSE 8 END, CurrentCrewName ASC";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;

                    //Try to find the most recent crew list and put those people on today's list
                    if ($this.data.length === 0 ) {
                        where = "CAST(Month(CurrentCrewDateTime) AS VARCHAR(2)) + CAST(Day(CurrentCrewDateTime) AS VARCHAR(2)) + CAST(Year(CurrentCrewDateTime) AS VARCHAR(4)) = (" +
                                     " SELECT TOP 1 CAST(MONTH(CurrentCrewDateTime) AS VARCHAR(2)) + CAST(DAY(CurrentCrewDateTime) AS VARCHAR(2)) + CAST(YEAR(CurrentCrewDateTime) AS VARCHAR(4))" +
                                     " FROM " + _currentcrew.customTableName +
                                     " WHERE CurrentCrewDateTime = (SELECT MAX(CurrentCrewDateTime) FROM " + _currentcrew.customTableName + " WHERE  CurrentCrewDateTime <  '" + $filter('date')(_page.viewDate, _page.dateFormat) + "'" +
                                     " AND ISNULL(IsDeleted,0)!=1)) AND ISNULL(CurrentCrewIsDepartingToday,0)!=1";
                        
                        RestApi.list($this, where, orderby).then(function (result) {
                            if (result != null) {
                                var crewList = result;
                                angular.forEach(crewList, function (crew, key) {
                                    RestApi.insert(_page, $this, null, -1).then(function (result) {
                                        $this = result.scopeObj;
                                        $this.data[result.insertedIndex].CurrentCrewUserGUID = crew.CurrentCrewUserGUID;
                                        $this.data[result.insertedIndex].CurrentCrewName = crew.CurrentCrewName;
                                        $this.data[result.insertedIndex].CurrentCrewRole = crew.CurrentCrewRole;
                                        $this.data[result.insertedIndex].CurrentCrewDateTime = $filter('date')(_page.viewDate, _page.dateFormat);
                                    });
                                });
                            }
                        });
                    }
                    _page.complete = true;
                });
            }
          
            //Insert a new record OR update an existing record

            _currentcrew.Save = function (crew, index) {
                var $this = this;

                _currentcrew.calculateHoursRested();
                _currentcrew.updateEmployeeName(index);

                //save the record
                RestApi.save($this, crew).then(function (result) {
                    //update the kentico roles for the current crew so people have access

                    var $this = this;
                    var vViewDate = $filter('date')(_page.viewDate, _page.dateFormat).split("/");
                    var vmonth = parseInt(vViewDate[0]);
                    var vday = parseInt(vViewDate[1]);
                    var vyear = parseInt(vViewDate[2]);
                    var cViewDate = $filter('date')(new Date(), _page.dateFormat).split("/");
                    var cmonth = parseInt(cViewDate[0]);
                    var cday = parseInt(cViewDate[1]);
                    var cyear = parseInt(cViewDate[2]);
                    //if it's today...
                    if (vmonth == cmonth && vday == cday && vyear == cyear) {
                        Security.set(crew.CurrentCrewUserGUID, crew.CurrentCrewRole);
                    }
                });
            }

            // for checkboxes, if the current crew item is already saved then
            // save the row when we click the checkbox. Otherwise just leave it alone
            _currentcrew.IsSaved = function(crew, index) {
                if (crew.ItemID !== 0)
                {
                    _currentcrew.Save(crew, index);
                }
            }

            //Insert a Record, but it doesn't become a true record until we click save
            _currentcrew.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;
                    $this.data[result.insertedIndex].CurrentCrewDateTime = _page.viewDate;
                });
            };

           
            //Delete
            _currentcrew.Delete = function (id) {                
                var $this = this;
                var delCrew = $this.data[id];

                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;

                    $this.data = result; //get the updated scope data object after deleting an object

                    // if the row had been committed to the DB already then we need to cleanup
                    // user roles after removing this item
                    if (delCrew.ItemID !== 0)
                    {
                        //update the kentico roles for the removed crew memeber
                        var vViewDate = $filter('date')(_page.viewDate, _page.dateFormat).split("/");
                        var vmonth = parseInt(vViewDate[0]);
                        var vday = parseInt(vViewDate[1]);
                        var vyear = parseInt(vViewDate[2]);
                        var cViewDate = $filter('date')(new Date(), _page.dateFormat).split("/");
                        var cmonth = parseInt(cViewDate[0]);
                        var cday = parseInt(cViewDate[1]);
                        var cyear = parseInt(cViewDate[2]);

                        //if it's today...
                        if (vmonth == cmonth && vday == cday && vyear == cyear) {
                            Security.set(delCrew.CurrentCrewUserGUID, "");
                        }
                    }
                });
            };

            //calculate the number of rested hours
            _currentcrew.calculateHoursRested = function () {
                if (_currentcrew.data && _currentcrew.data.length > 0) {
                    for (var i = 0; i < _currentcrew.data.length; i++) {                        
                        _currentcrew.data[i].CurrentCrewHoursRested = 24 - _currentcrew.data[i].CurrentCrewHoursWorked;
                    }
                }
            };

            
            
            /* End Rest Methods */
            /*********************************************/
        }];