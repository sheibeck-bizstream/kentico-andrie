﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'VesselForm', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, VesselForm, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _nearMiss = $scope.nearMiss = $scope.$new();                        
            _page.viewDate = new Date();
            
            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions();
            _page.dateFormat = _page.datetimePickerOpts.format;

            _page.select2Options = {
                multiple: true,
                allowClear: true,
                minimumInputlength: 2
            };

            //tell the rest api what object we're looking for
            _nearMiss.classname = 'apm.nearmiss'; //case sensitive
            _nearMiss.customTableName = 'APM_NearMiss';

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_nearMiss.classname).then(function (result) {                
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.unsafeActs) === "object"
                        && typeof (_page.levels) === "object"
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";                    
                }, function (newVal, oldVal) {
                    if (newVal)
                        _nearMiss.List();
                });                            

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        _page.complete = false;
                        _nearMiss.List();
                    }
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables            
            LookupTables.get('UnsafeAct').then(function (result) {
                _page.unsafeActs = result;
            });

            LookupTables.get('Level').then(function (result) {
                _page.levels = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',                
                "min-mode": "year",
                "datepicker-mode": "'year'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, "yyyy") === $filter('date')(new Date(), "yyyy");
            }


            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the deck log for today
            _nearMiss.List = function (where) {
                var $this = this;
                var fViewDate = $filter('date')(_page.viewDate, "yyyy");                
                where = where || "Year(NearMissDateTime)=" + fViewDate;
                var orderby = 'NearMissDateTime';

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    _page.complete = true;
                });
            }            

            //Update a Record == .update()
            _nearMiss.Validate = function (data, validationType) {
                return Util.validate(data, validationType);
            }
          
            //Insert a new record OR update an existing record
            _nearMiss.Save = function (data, index) {
                var $this = this;

                //convert the saftey meetings from an array to a string for saving
                data.NearMissUnsafeAct = $filter('viewTags')(data.NearMissUnsafeAct);

                RestApi.save($this, data);
            };

            //Insert a Record, but it doesn't become a true record until we click save
            //AH BugzID:50859 - We need to save this right away so that reports will generate for the record no matter when they click the "Generate Form" button
            _nearMiss.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;
                    $this.data[result.insertedIndex].NearMissDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm");
                    RestApi.save($this, $this.data[result.insertedIndex]);
                });
            };
           
            //Deletes
            //AH - 12-17-2014 BugzID:50859 - Enabled the delete for all records in the near miss table.
            //SMH - 01-29-2014 - Refactored deletes to set a bit instead of a true delete
            _nearMiss.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _nearMiss.GenerateForm = function (data) {                
                //TODO: pass in the class name and lookup the guid on the server side
                VesselForm.export(
                    "c812ae51-1b00-45a2-9531-5cab31796a83",
                    null,
                    null,
                    data.ItemGUID //report on a specific item
                );
            }

            /* End Rest Methods */
            /*********************************************/
        }];