﻿module.exports = ['$scope', '$http', '$log', '$filter', '$timeout', '$routeParams', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, $timeout, $routeParams, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();

            var _cargoDischarge = $scope.cargoDischarge = $scope.$new();            
            var _arrivalDischargeDockDrafts = $scope.arrivalDischargeDockDrafts = $scope.$new();
            var _departureDischargeDockDrafts = $scope.departureDischargeDockDrafts = $scope.$new();
            var _dischargeDockTimes = $scope.dischargeDockTimes = $scope.$new();
            var _waterGauges = $scope.waterGauges = $scope.$new();
            var _delays = $scope.delays = $scope.$new();
            var _cargoGeneralInformations = $scope.cargoGeneralInformations = $scope.$new();
            _cargoGeneralInformations.calculationScheduled = null;

            //load data
            var _cargoLoad = $scope.cargoLoad = $scope.$new();
            var _cargoLoadGeneralInformations = $scope._cargoLoadGeneralInformations = $scope.$new();

            $scope.cargoDischargeLoaded = false;
            $scope.arrivalDischargeDockDraftLoaded = false;
            $scope.departureDischargeDockDraftLoaded = false;
            $scope.dischargeDockTimeLoaded = false;
            $scope.waterGaugeLoaded = false;
            $scope.delayLoaded = false;
            $scope.cargoGeneralInformationLoaded = false;

            $scope.cargoLoadLoaded = false;
            $scope.cargoLoadGeneralInformationLoaded = false;

            var _detailsId = $routeParams.id;
            if (!_detailsId) {
                window.location = "/cargo#/view/";
            }

            //datetime picker options and date format
            _page.datetimePickerOpts = Util.dateTimePickerOptions(true);
            _page.select2Options = {
                allowClear: true
            };
            _page.dateFormat = _page.datetimePickerOpts.format;


            //tell the rest api what object we're looking for
            _cargoDischarge.classname = 'apm.cargo'; //case sensitive
            _cargoDischarge.customTableName = 'APM_Cargo';

            _cargoLoad.classname = 'apm.cargo'; //case sensitive
            _cargoLoad.customTableName = 'APM_Cargo';

            _cargoLoadGeneralInformations.classname = 'apm.cargogeneralinformation'; //case sensitive
            _cargoLoadGeneralInformations.customTableName = 'APM_CargoGeneralInformation';

            _arrivalDischargeDockDrafts.classname = 'apm.cargodockdraft'; //case sensitive
            _arrivalDischargeDockDrafts.customTableName = 'APM_CargoDockDraft';

            _departureDischargeDockDrafts.classname = 'apm.cargodockdraft'; //case sensitive
            _departureDischargeDockDrafts.customTableName = 'APM_CargoDockDraft';

            _dischargeDockTimes.classname = 'apm.cargodocktime'; //case sensitive
            _dischargeDockTimes.customTableName = 'APM_CargoDockTime';

            _waterGauges.classname = 'apm.cargowatergauge'; //case sensitive
            _waterGauges.customTableName = 'APM_CargoWaterGauge	';

            _delays.classname = 'apm.cargodelay'; //case sensitive
            _delays.customTableName = 'APM_CargoDelay';

            _cargoGeneralInformations.classname = 'apm.cargogeneralinformation'; //case sensitive
            _cargoGeneralInformations.customTableName = 'APM_CargoGeneralInformation';
            

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            _page.isLoaded = function () {
                return ($scope.cargoDischargeLoaded
                        && $scope.arrivalDischargeDockDraftLoaded
                        && $scope.departureDischargeDockDraftLoaded
                        && $scope.dischargeDockTimeLoaded
                        && $scope.waterGaugeLoaded
                        && $scope.delayLoaded
                        && $scope.cargoGeneralInformationLoaded
                        && $scope.cargoLoadLoaded
                        && $scope.cargoLoadGeneralInformationLoaded);
            };

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_cargoDischarge.classname).then(function (result) {
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.cargoTypes) === "object"
                        && typeof (_page.timeLabels) === "object"
                        && typeof (_page.terminalDocks) === "object"
                        && typeof (_page.delayCauses) === "object"
                      //&& typeof (_page.vesselDrafts) === "object"
                        && typeof (_page.customers) === "object";
                }, function (newVal, oldVal) {
                    if (newVal)
                        _cargoDischarge.List();
                });

                // Check to make sure everything is loaded
                $scope.$watch('cargoDischargeLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('arrivalDischargeDockDraftLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('departureDischargeDockDraftLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('dischargeDockTimeLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('waterGaugeLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('delayLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('cargoGeneralInformationLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('cargoLoadLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });

                $scope.$watch('cargoLoadGeneralInformationLoaded', function (scope) {
                    if (_page.isLoaded()) {
                        _page.complete = true;
                    }
                });                

                $scope.$watch('dischargeDockTimes.data', function (scope) {
                    if (_cargoDischarge.data && _cargoDischarge.data.length > 0) {
                        _dischargeDockTimes.calculateRates();
                    }
                }, true);

                //update general information calculations
                // whenever cargo tank soundings change 
                // or cargo general information changes
                $scope.$watch('cargoGeneralInformations.data', function () {
                    if (_page.complete) {
                        _cargoGeneralInformations.ScheduleCalculation();
                        _cargoDischarge.calculateBbls();
                        _dischargeDockTimes.calculateRates();
                    }
                }, true);

                $scope.$watch('cargoDischarge.data', function () {
                    if (_page.complete) {                        
                        _cargoGeneralInformations.ScheduleCalculation();
                        _cargoDischarge.calculateBbls();
                        _dischargeDockTimes.calculateRates();
                    }
                }, true);

            });

            //calculate load rates
            _dischargeDockTimes.calculateRates = function () {
                if (_cargoDischarge.data && _cargoDischarge.data.length > 0) {
                    var data = _cargoDischarge.data[0];

                    data.CargoTotalDischargeTime = 0;
                    data.CargoBblsDischargeRate = 0;

                    //get the total load time 
                    var totalUnloadTime = 0;
                    if (_dischargeDockTimes.data && _dischargeDockTimes.data.length > 0) {

                        //find the start cargo and finish cargo events
                        var startCargo = $.grep(_dischargeDockTimes.data, function (e) {
                            return e.CargoDockTimeLocation === "Start Cargo";
                        });
                        var finishCargo = $.grep(_dischargeDockTimes.data, function (e) {
                            return e.CargoDockTimeLocation === "Finish Cargo";
                        });

                        if(startCargo.length > 0 && finishCargo.length > 0) {

                            //get the total time between the start cargo and finish cargo
                            var sCargo = startCargo[0].CargoDockTimeDateTime;
                            var fCargo = finishCargo[0].CargoDockTimeDateTime;

                            //make sure the dates/times are not null
                            if (sCargo && fCargo) {
                                //get the total hours.minutes difference
                                totalUnloadTime = Util.getDateDiffHoursAsFloat(sCargo, fCargo);

                                //now collect any Stop/Start pairs between these two
                                var startIndex = _dischargeDockTimes.data.indexOf(startCargo[0]);
                                var finishIndex = _dischargeDockTimes.data.indexOf(finishCargo[0]);

                                var aryStop = new Array();
                                var aryStart = new Array();

                                for (var i = startIndex; i < finishIndex; i++) {
                                    var dtd = _dischargeDockTimes.data[i];

                                    if (dtd.CargoDockTimeLocation === "Stop")
                                        aryStop.push(dtd.CargoDockTimeDateTime);

                                    if (dtd.CargoDockTimeLocation === "Restart")
                                        aryStart.push(dtd.CargoDockTimeDateTime);
                                }

                                var totalStopTime = 0;
                                //get the total time between all the start and stop pairs and subtract this from the total time
                                for (var i = 0; i < aryStop.length; i++) {
                                    //make sure we have a matching Start item or we don't have a proper pair
                                    if (aryStart[i]) {
                                        totalStopTime += Util.getDateDiffHoursAsFloat(aryStop[i], aryStart[i]);
                                    }
                                }

                                totalUnloadTime -= totalStopTime;
                            }
                        }
                    }

                    if (totalUnloadTime > 0) {
                        data.CargoTotalDischargeTime = totalUnloadTime;
                        data.CargoBblsDischargeRate = Util.parseFloat(data.CargoTotalBblsDischarged / totalUnloadTime, 2);
                    }
                }
            }

            //get total number of barrels from tanks
            _cargoDischarge.calculateBbls = function () {
                if (_cargoDischarge.data && _cargoDischarge.data.length > 0) {
                    var data = _cargoDischarge.data[0];
                    var totalBbls = 0;
                    var averageTemp = 0;
                    var tankCount = 0;

                    var arrTanks = ["P", "S"];
                    for (var t = 0; t < arrTanks.length; t++) {
                        // 6 tanks on each side
                        for (var n = 1; n <= 6; n++) {

                            //iterate over port and starboard tanks
                            var tankType = eval("data.CargoType" + n + arrTanks[t]);

                            if (!Util.isEmpty(tankType)) {
                                tankCount++;
                                totalBbls += parseFloat(eval("data.CargoBbls" + n + arrTanks[t]) || 0);
                                averageTemp += parseFloat(eval("data.CargoTemp" + n + arrTanks[t]) || 0);
                            }
                        }
                    }
                }
            }

            // make sure everything is ready before we start running calculations. WE may need to add rows
            // and those rows all need to be ready to go before we calculate.
            _cargoGeneralInformations.ScheduleCalculation = function()
            {
                if (_cargoGeneralInformations.calculationScheduled === null) {
                    // make sure all the cargo general information items are fully loaded/inserted
                    // or will get some timing issues that may cause extra rows to get added
                    _cargoGeneralInformations.calculationScheduled = setInterval(function () {
                        if (_cargoGeneralInformations.data.length === _cargoLoadGeneralInformations.data.length) {
                            clearInterval(_cargoGeneralInformations.calculationScheduled);
                            _cargoGeneralInformations.calculationScheduled = null;
                            $timeout(function () {
                                _cargoGeneralInformations.calculate();
                            }, 25);
                        }
                    }, 1000);
                }
            }          

            //make calculations for each type of cargo in the tanks
            // Type, Lbs/Gal, Lbs/Bbl, Factor, API and Temp Loaded all come from the load screen.
            // Calculate Gross Bbls, Gross Tons, Net Tons, US Lbs, Metric Tons and Net Bbls
            _cargoGeneralInformations.calculate = function () {               
                if (_cargoGeneralInformations.data && _cargoGeneralInformations.data.length > 0) {

                    var dischargedBbls = 0;

                    for (var g = 0; g < _cargoGeneralInformations.data.length; g++) {
                        //get the row we want to calculate on from the general information table                       
                        var disData = _cargoDischarge.data[0];
                        var cargoTankTypeCount = 0;                        
                        var cargoTotalBarrelsTypeCount = 0;
                        
                        //go find any tank that has the same type
                        var arrTanks = ["P", "S"];
                        for (var t = 0; t < arrTanks.length; t++) {
                            // 6 tanks on each side
                            for (var n = 1; n <= 6; n++) {
                                //iterate over port and starboard tanks                                
                                var tankType = eval("disData.CargoType" + n + arrTanks[t]);                               

                                //other stuff is based on the discharge tanks
                                if (!Util.isEmpty(tankType)) {
                                    var tankBbls = eval("disData.CargoBbls" + n + arrTanks[t]);
                                    if (tankType === _cargoGeneralInformations.data[g].CargoGeneralInformationType) {
                                        //if we found a tank that has this type of stuff in it then do some calculations
                                        cargoTankTypeCount++;
                                        cargoTotalBarrelsTypeCount += parseFloat(tankBbls);
                                    }
                                }
                            }
                        }

                        // OBQs are per type of load
                        _cargoGeneralInformations.data[g].CargoGeneralInformationOBQs = cargoTotalBarrelsTypeCount;
                     
                        //find a matching product in the load general information
                        var loadGi = $.grep(_cargoLoadGeneralInformations.data, function (e) {
                            return e.CargoGeneralInformationType === _cargoGeneralInformations.data[g].CargoGeneralInformationType;
                        });
                        if (loadGi.length > 0) {
                            var loadGeneralInfo = loadGi[0];

                            //only attempt to get the gross onboard barrels if we don't have a value
                            if (!_cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls) {
                                _cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls = loadGeneralInfo.CargoGeneralInformationGrossBbls;
                            }

                            if (!_cargoGeneralInformations.data[g].CargoGeneralInformationFactor) {
                                _cargoGeneralInformations.data[g].CargoGeneralInformationFactor = loadGeneralInfo.CargoGeneralInformationFactor;
                            }

                            if (!_cargoGeneralInformations.data[g].CargoGeneralInformationTemp) {
                                _cargoGeneralInformations.data[g].CargoGeneralInformationTemp = loadGeneralInfo.CargoGeneralInformationTemp;
                            }                            

                            //update some other stuff that we always want from the load                            
                            _cargoGeneralInformations.data[g].CargoGeneralInformationLbsGal = loadGeneralInfo.CargoGeneralInformationLbsGal;
                            _cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl = loadGeneralInfo.CargoGeneralInformationLbsBbl;
                            _cargoGeneralInformations.data[g].CargoGeneralInformationAPI = loadGeneralInfo.CargoGeneralInformationAPI;
                        }         
                        
                        //This is actually Gross Bbl Onboard
                        _cargoGeneralInformations.data[g].CargoGeneralInformationNetBbls = Util.parseFloat((_cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls * _cargoGeneralInformations.data[g].CargoGeneralInformationFactor) - _cargoGeneralInformations.data[g].CargoGeneralInformationOBQs, 2);

                        _cargoGeneralInformations.data[g].CargoGeneralInformationGrossTons = Util.parseFloat((_cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl * _cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls) / 2000, 2);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationNetTons = Util.parseFloat((_cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl * _cargoGeneralInformations.data[g].CargoGeneralInformationNetBbls) / 2000, 2);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationMetricTons = Util.parseFloat(_cargoGeneralInformations.data[g].CargoGeneralInformationNetTons * 0.907185, 3);
                        _cargoGeneralInformations.data[g].CargoGeneralInformationUSLbs = Util.parseFloat(_cargoGeneralInformations.data[g].CargoGeneralInformationNetBbls * _cargoGeneralInformations.data[g].CargoGeneralInformationLbsBbl, 2);


                        //keep track of total discharged barrels
                        // cargoTotalBarrelsTypeCount will === OBQs when everything is of the same type.
                        // so we are essentially subtracting what's left of the this type from how much
                        // from the gross on board and that gives us how much we discharged of this type
                        dischargedBbls += _cargoGeneralInformations.data[g].CargoGeneralInformationGrossBbls - cargoTotalBarrelsTypeCount;

                        // now we can calculate ROBs                        
                        // This is net bbls (on the load screen) - net bbls (on the discharge screen) not sure where it is getting the number it is
                        _cargoGeneralInformations.data[g].CargoGeneralInformationROBs = Util.parseFloat((loadGi[0].CargoGeneralInformationNetBbls || 0) - _cargoGeneralInformations.data[g].CargoGeneralInformationNetBbls);
                       
                        //update the scope variable
                        //_cargoGeneralInformations.data[g] = data;

                        //don't do any of this if the current user doesn't have privileges
                        if (!_page.security.modify)
                            return;

                        //save the row
                        if (!Util.isEmpty(_cargoGeneralInformations.data[g])) {
                            _cargoGeneralInformations.Save(_cargoGeneralInformations.data[g], g, false);                            
                        }
                        
                    }

                    //update total discharged barrels
                    _cargoDischarge.data[0].CargoTotalBblsDischarged = Util.parseFloat(dischargedBbls, 0);
                    _cargoDischarge.Save(_cargoDischarge.data[0], 0, false);
                }
            }

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables      

            LookupTables.get('TimeLabel').then(function (result) {
                _page.timeLabels = result;
            });

            LookupTables.get('CargoType').then(function (result) {
                _page.cargoTypes = result;
            });

            // BZS (SMH) 7/20/2015: Feature - Prepopulate Dock Drafts
            // Moving the laoding of this into the _arrivalDischargeDockDrafts.List() function
            // so we can autopopulate the list of items for the users
            /*LookupTables.get('VesselDraft').then(function (result) {
                _page.vesselDrafts = result;
            });*/

            LookupTables.get('Customer', 'CustomerName', 'CustomerName').then(function (result) {
                _page.customers = result;
            });

            LookupTables.get('TerminalDockName', 'TerminalName', 'TerminalName').then(function (result) {
                _page.terminalDocks = result;
            });

            LookupTables.get('DelayCause').then(function (result) {
                _page.delayCauses = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }

            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                "min-mode": "month",
                "datepicker-mode": "'month'"
            };
            /* END DATE PICKER */
            /*********************************************/

            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the discharge
            _cargoDischarge.List = function (where) {
                var $this = this;

                // Will make sure that TripNumber is the same as the cargo and the cargoParentGUID is not null. 
                where = where || "ItemID='" + _detailsId + "'";


                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    if (result.length > 0) {
                        // get the cargo load
                        _cargoLoad.List();

                        $scope.cargoDischargeLoaded = true;

                        var initalWhereCondition = "CargoGUID='" + $this.data[0].ItemGUID + "'";
                        _arrivalDischargeDockDrafts.List(initalWhereCondition);
                        _departureDischargeDockDrafts.List(initalWhereCondition);
                        _dischargeDockTimes.List(initalWhereCondition);
                        _waterGauges.List(initalWhereCondition);
                        _delays.List(initalWhereCondition);                        
                    }
                    else
                    {
                        //don't do any of this if the current user doesn't have privileges
                        if (!_page.security.modify)
                            return;

                        $this.Insert();
                    }
                });
            }

            //get the load so we can do calculations
            _cargoLoad.List = function (where) {
                var $this = this;
                where = "ItemGUID = '" + _cargoDischarge.data[0].CargoParentGUID + "'";

                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    //auto populate the cargo tank types from the load screen
                    _cargoDischarge.populateTankType();

                    //force an update in case something changed on the load screen
                    _cargoDischarge.Save(_cargoDischarge.data[0], 0, true);

                    $scope.cargoLoadLoaded = true;

                    var initalWhereCondition = "CargoGUID='" + $this.data[0].ItemGUID + "'"; //get general info items from the LOAD record
                    _cargoLoadGeneralInformations.List(initalWhereCondition);
                });
            }

            // if we don't have any values in the cargo tank types then prepopulate
            // the tanks with the type from the load screen
            _cargoDischarge.populateTankType = function()
            {
                //iterate over the tanks and if there is not cargotype set it to the cargo type of the load if it exists
                var data = this.data[0];
                var arrTanks = ["P", "S"];
                for (var t = 0; t < arrTanks.length; t++) {
                    // 6 tanks on each side
                    for (var n = 1; n <= 6; n++) {

                        //iterate over port and starboard tanks
                        var tankType = eval("data.CargoType" + n + arrTanks[t]);

                        if (Util.isEmpty(tankType)) {                            
                            eval('_cargoDischarge.data[0].CargoType' + n + arrTanks[t] + ' = ' + '_cargoLoad.data[0].CargoType' + n + arrTanks[t]);
                        }
                    }
                }
            }

            //get the load general information so we can do calculations
            _cargoLoadGeneralInformations.List = function (where) {
                var $this = this;
                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    $scope.cargoLoadGeneralInformationLoaded = true;

                    //we need to make sure our Load general informations list is available
                    //before we get the discharge general cargo information
                    var initalWhereCondition = "CargoGUID='" + _cargoDischarge.data[0].ItemGUID + "'"; //get general info items from the DISCHARGE record
                    _cargoGeneralInformations.List(initalWhereCondition);
                });
            }

            _arrivalDischargeDockDrafts.List = function (where) {
                var $this = this;
                var orderby = "CargoDockDraftLocation ASC";
                where = where + " AND CargoDockDraftType='Arrival'";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;

                    //We always have to fill out PF, PA, SF, SA, so prepopulate them if they don't exist
                    LookupTables.get('VesselDraft').then(function (result) {
                        _page.vesselDrafts = result;

                        LookupTables.get('VesselDraft').then(function (result) {
                            _page.vesselDrafts = result;

                            angular.forEach(_page.vesselDrafts, function (draft, key) {
                                var exists = $.grep(_arrivalDischargeDockDrafts.data, function (obj) {
                                    return obj.CargoDockDraftLocation === draft.text;
                                });

                                if (exists.length === 0) {
                                    // if the vessel draft doesn't exist in the list then add it
                                    _arrivalDischargeDockDrafts.Insert(draft.text);
                                }
                            });

                            $scope.arrivalDischargeDockDraftLoaded = true;
                        });

                    });
                });
            }

            _departureDischargeDockDrafts.List = function (where) {
                var $this = this;
                var orderby = "CargoDockDraftLocation ASC";
                where = where + " AND CargoDockDraftType='Departure'";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;

                    //We always have to fill out PF, PA, SF, SA, so prepopulate them if they don't exist
                    LookupTables.get('VesselDraft').then(function (result) {
                        _page.vesselDrafts = result;

                        LookupTables.get('VesselDraft').then(function (result) {
                            _page.vesselDrafts = result;

                            angular.forEach(_page.vesselDrafts, function (draft, key) {
                                var exists = $.grep(_departureDischargeDockDrafts.data, function (obj) {
                                    return obj.CargoDockDraftLocation === draft.text;
                                });

                                if (exists.length === 0) {
                                    // if the vessel draft doesn't exist in the list then add it
                                    _departureDischargeDockDrafts.Insert(draft.text);
                                }
                            });

                            $scope.departureDischargeDockDraftLoaded = true;
                        });

                    });
                });
            }

            _dischargeDockTimes.List = function (where) {
                var $this = this;
                var orderby = "CargoDockTimeDateTime ASC";
                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = $filter('orderBy')(result, 'CargoDockTimeDateTime', false);
                    $scope.dischargeDockTimeLoaded = true;
                });
            }
            _waterGauges.List = function (where) {
                var $this = this;
                RestApi.list($this, where).then(function (result) {
                    $this.data = result;
                    $scope.waterGaugeLoaded = true;
                });
            }
            _delays.List = function (where) {
                var $this = this;
                RestApi.list($this, where).then(function (result) {
                    $this.data = result;
                    $scope.delayLoaded = true;
                });
            }

            //this item needs to pull data from the load screen
            _cargoGeneralInformations.List = function (where) {
                var $this = this;
                RestApi.list($this, where).then(function (result) {
                    $this.data = result;

                    //don't do any of this if the current user doesn't have privileges
                    if (!_page.security.modify)
                    {
                        $scope.cargoGeneralInformationLoaded = true;
                        return;
                    }                        

                    //go through the load cargo general information and add a record
                    //for each one in the load if we don't have one already
                    var runCalculations = false;
                    angular.forEach(_cargoLoadGeneralInformations.data, function (loadGI, key) {
                        //do a double check that we have a valid product type, if we don't,
                        // then don't add anything to the cargo general information section
                        if (loadGI.CargoGeneralInformationType !== null) {
                            //find a matching record in the discharge cargo general information
                            var disGI = $.grep($this.data, function (d, key) {
                                return d.CargoGeneralInformationType === loadGI.CargoGeneralInformationType;
                            });

                            //Add a new discharge cargo general info if we didn't find a match
                            if (disGI.length === 0) {
                                $this.Insert(loadGI);
                                runCalculations = true;
                            }
                        }
                    });

                    //once we're done adding stuff run the calculations if needed
                    if (runCalculations) {
                        $this.ScheduleCalculation();
                    }

                    $scope.cargoGeneralInformationLoaded = true;
                });
            }            

            //Insert a new record OR update an existing record
            _cargoDischarge.Save = function (data, index, bCalculate) {
                var $this = this;
                //make sure we have updated data from the load
                if (_cargoLoad.data && _cargoLoad.data.length > 0) {
                    $this.data[0].CargoCustomer = _cargoLoad.data[0].CargoCustomer;
                    $this.data[0].CargoLoadTerminal = _cargoLoad.data[0].CargoLoadTerminal;
                    $this.data[0].CargoCustomerBOLNumber = _cargoLoad.data[0].CargoCustomerBOLNumber;
                    $this.data[0].CargoCustomerTripNumber = _cargoLoad.data[0].CargoCustomerTripNumber;
                }

                RestApi.save($this, data).then(function (result) {
                    if (bCalculate !== false) {
                        //run the general information calculations
                        _cargoGeneralInformations.ScheduleCalculation();                    

                        //run the load rate calculations
                        _dischargeDockTimes.calculateRates();
                    }
                });
            };

            _arrivalDischargeDockDrafts.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            _departureDischargeDockDrafts.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data, $this.classname + "-1");
            };

            _dischargeDockTimes.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data).then(function (result) {
                    //reorder the list by times ASC
                    $this.data = $filter('orderBy')($this.data, 'CargoDockTimeDateTime', false);
                });
            };

            _waterGauges.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            _delays.Save = function (data, index) {
                var $this = this;
                RestApi.save($this, data);
            };

            _cargoGeneralInformations.Save = function (data, index, bCalculate) {
                var $this = this;
                bCalculate = bCalculate === null ? true : bCalculate;

                //recalculate formulas if needed
                if (bCalculate) {
                    $this.ScheduleCalculation();
                }
                else {
                    RestApi.save($this, data);
                }
            };

            //Insert a Record, but it doesn't become a true record until we click save
            _cargoDischarge.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    _cargoDischarge = result.scopeObj;

                    //save this object right away, don't wait for a user to explicity save
                    $this.Save($this.data[result.insertedIndex]);
                });
            };

            _arrivalDischargeDockDrafts.Insert = function (dockDraftLocation) {
                var $this = this;
                var autoOpenIndex = (dockDraftLocation || '') !== '' ? -1 : null;
                RestApi.insert(_page, $this, _cargoDischarge.data[0].ItemGUID, autoOpenIndex).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object
                    $this.data[result.insertedIndex].CargoGUID = _cargoDischarge.data[0].ItemGUID;
                    $this.data[result.insertedIndex].CargoDockDraftType = 'Arrival';

                    if (!Util.isEmpty(dockDraftLocation)) {
                        $this.data[result.insertedIndex].CargoDockDraftLocation = dockDraftLocation;
                    }
                });
            };

            _departureDischargeDockDrafts.Insert = function (dockDraftLocation) {
                var $this = this;
                var autoOpenIndex = (dockDraftLocation || '') !== '' ? -1 : null;
                //added a "-1" to the classname so we can distinguish between a LoadDockDraft and DischargeDockDraft 
                //when the UI wants to hilight a row or open a row
                RestApi.insert(_page, $this, _cargoDischarge.data[0].ItemGUID, autoOpenIndex, null, $this.classname + '-1').then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].CargoGUID = _cargoDischarge.data[0].ItemGUID;
                    $this.data[result.insertedIndex].CargoDockDraftType = 'Departure';

                    if (!Util.isEmpty(dockDraftLocation)) {
                        $this.data[result.insertedIndex].CargoDockDraftLocation = dockDraftLocation;
                    }
                });
            };

            _dischargeDockTimes.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this, _cargoDischarge.data[0].ItemGUID).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object
                    $this.data[result.insertedIndex].CargoGUID = _cargoDischarge.data[0].ItemGUID;                    
                    $this.data[result.insertedIndex].CargoDockTimeDateTime = $filter('date')(new Date(), "MM/dd/yyyy HH:mm");
                });
            };

            _waterGauges.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this, _cargoDischarge.data[0].ItemGUID).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].CargoGUID = _cargoDischarge.data[0].ItemGUID;                    
                });
            };

            _delays.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].CargoGUID = _cargoDischarge.data[0].ItemGUID;                    
                });
            };

            _cargoGeneralInformations.Insert = function (loadGeneralInfo) {
                var $this = this;
                RestApi.insert(_page, $this, _cargoDischarge.data[0].ItemGUID, -1).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    $this = result.scopeObj;

                    // set any additional parameters we want when creating a new object                    
                    $this.data[result.insertedIndex].CargoGUID = _cargoDischarge.data[0].ItemGUID;                    
                    $this.data[result.insertedIndex].CargoGeneralInformationType = loadGeneralInfo.CargoGeneralInformationType;
                    $this.data[result.insertedIndex].CargoGeneralInformationLbsGal = loadGeneralInfo.CargoGeneralInformationLbsGal;
                    $this.data[result.insertedIndex].CargoGeneralInformationFactor = loadGeneralInfo.CargoGeneralInformationFactor;
                    $this.data[result.insertedIndex].CargoGeneralInformationTemp = loadGeneralInfo.CargoGeneralInformationTemp;
                    $this.data[result.insertedIndex].CargoGeneralInformationLbsBbl = loadGeneralInfo.CargoGeneralInformationLbsBbl;
                    $this.data[result.insertedIndex].CargoGeneralInformationAPI = loadGeneralInfo.CargoGeneralInformationAPI;
                });
            };

            //Deletes
            _cargoDischarge.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _arrivalDischargeDockDrafts.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };
            _departureDischargeDockDrafts.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _dischargeDockTimes.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                    $this.calculateRates();
                });
            };

            _waterGauges.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _delays.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            _cargoGeneralInformations.Delete = function (id) {
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];