﻿module.exports = ['$scope', '$http', '$log', '$filter', 'RestApi', 'RecordSchemaApi', 'Util', 'LookupTables', 'Modal', 'Base',
        function ($scope, $http, $log, $filter, RestApi, RecordSchemaApi, Util, LookupTables, Modal, Base) {

            /*********************************************/
            /* scope vars */
            var _page = $scope.page = $scope.$new();
            var _visualMonitoring = $scope.visualMonitoring = $scope.$new();

            _page.select2Options = {
                allowClear: true
            };
           
            //tell the rest api what object we're looking for
            _visualMonitoring.classname = 'apm.visualmonitoring'; //case sensitive
            _visualMonitoring.customTableName = 'APM_VisualMonitoring';

            //datetime picker options and date format           
            _page.dateFormat = Util.dateFormat(true);
            _page.viewDate = new Date();

            /* end scope vars */
            /*********************************************/

            //show a status alerts            
            _page.addAlert = function (message, type) {
                _page.alerts = Util.addAlert(message, type);
            };
            _page.closeAlert = function (index) {
                _page.alerts = Util.closeAlert();
            };
            //some messages auto fall off the list, watch for them and then update the alerts
            Util.registerObserverCallback(function () {
                _page.alerts = Util.pageAlerts;
            });
            // end status alerts

            /*********************************************/
            /* initialize */

            // Make this call on most pages unless there is a special case
            // Setup security and get base information that every page needs            
            Base.get(_visualMonitoring.classname).then(function (result) {                
                //iterate over any objects in the base and attach them to the page scope
                angular.forEach(result, function (value, key) {
                    eval("_page." + key + ' = value');
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch(function (scope) {
                    return typeof (_page.cargoTypes) === "object"
                        && typeof (_page.terminalDocks) === "object"
                        && typeof (_page.crewMembers) === "object"
                        && typeof (_page.viewDate) !== "undefined"
                        && typeof (_page.dateFormat) !== "undefined";                    
                }, function (newVal, oldVal) {
                    if (newVal)
                        _visualMonitoring.List();
                });              

                $scope.$watch('page.dateFormat', function (scope) {
                    _page.viewDate = new Date();
                });

                // intialize the page after we have all the data we need loaded
                $scope.$watch('page.viewDate', function (scope) {
                    if (_page.complete)
                    {
                        _page.complete = false;
                        _visualMonitoring.List();
                    }   
                });
            });

            /* end initialize */
            /*********************************************/


            //********************************************
            // BEGIN Lookup Tables            
            // Employee List
            LookupTables.GetCrewForDate("CrewMember", "CrewMemberName", "CrewMemberName", _page.viewDate).then(function (result) {
                _page.crewMembers = result;
            });            

            LookupTables.get('CargoType').then(function (result) {
                _page.cargoTypes = result;
            });
            LookupTables.get('TerminalDockName', 'TerminalName', 'TerminalName').then(function (result) {
                _page.terminalDocks = result;
            });

            _page.showLookupData = function (tableName, data, dataId) {
                return LookupTables.show(tableName, data, dataId);
            }


            // END Lookup Tables
            //********************************************

            /*********************************************/
            /* BEGIN DATE PICKER */

            _page.today = function () {
                _page.viewDate = new Date();
            };

            _page.openDatePicker = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();

                _page.isDatePickerOpened = true;
            };

            _page.dateOptions = {
                formatYear: 'yyyy',
                formatMonth: 'MM',
                formatDay: 'dd',
                startingDay: 1
            };
            /* END DATE PICKER */
            /*********************************************/

            /* misc methods */
            //are we viewing today?       
            _page.isToday = function () {
                return $filter('date')(_page.viewDate, _page.dateFormat) === $filter('date')(new Date(), _page.dateFormat);
            }

            /*********************************************/
            /* BEGIN REST Methods */

            /*Get Data
             * Use Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST             
            */

            //get the role of the selected crew member
            _visualMonitoring.UpdateRole = function (data, index) {
                LookupTables.GetCrewForDate("CrewMemberRole", "CrewMemberName", "CrewMemberRole", _page.viewDate, true).then(function (result) {
                    var item = $.grep(result, function (e) {
                        return e.id == data.VisualMonitoringName;
                    });
                    if (item.length > 0) {
                        _visualMonitoring.data[index].VisualMonitoringTitle = item[0].text;
                    }
                });
            };

            //get the deck log for today
            _visualMonitoring.List = function (where) {
                var $this = this;

                var fViewDate = $filter('date')(_page.viewDate, _page.dateFormat);
                var where = where || "VisualMonitoringDateTime >= '" + fViewDate + " 12:00:00 AM' and VisualMonitoringDateTime <= '" + fViewDate + " 11:59:59 PM'";

                var orderby = "VisualMonitoringDateTime";

                RestApi.list($this, where, orderby).then(function (result) {
                    $this.data = result;
                    _page.complete = true;
                });
            }            

            //Insert a new record OR update an existing record
            _visualMonitoring.Save = function (data, index) {               
                var $this = this;

                //the date we save should be the date we're on with the picker
                data.VisualMonitoringDateTime = $filter('date')(_page.viewDate, _page.dateFormat) + ' ' + data.VisualMonitoringDateTime;

                RestApi.save($this, data).then(function () {
                    //after we save pass back just the time so we display properly
                    data.VisualMonitoringDateTime = moment(data.VisualMonitoringDateTime).format("HH:mm");
                })

            };

            //Insert a Record, but it doesn't become a true record until we click save
            _visualMonitoring.Insert = function () {
                var $this = this;
                RestApi.insert(_page, $this).then(function (result) {
                    //update our scope variable with modified stuff, we're updating the whole object
                    // not just the data so we can capture the schema
                    LookupTables.getCurrentUser().then(function(currentUser){
                        $this = result.scopeObj;                        
                        $this.data[result.insertedIndex].VisualMonitoringName = currentUser;
                        // the the current time regardless of the date
                        $this.data[result.insertedIndex].VisualMonitoringDateTime = moment(new Date()).format("HH:mm");;
                    });

                });
            };
           
            //Deletes
            _visualMonitoring.Delete = function (id) {               
                var $this = this;
                _page.complete = false;
                RestApi.delete($this, id).then(function (result) {
                    _page.complete = true;
                    $this.data = result; //get the updated scope data object after deleting an object
                });
            };

            /* End Rest Methods */
            /*********************************************/
        }];