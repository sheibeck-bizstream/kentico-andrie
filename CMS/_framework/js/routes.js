module.exports = ['$routeProvider', function ($routeProvider) {
	//dynamically determine our template and controller based on the url we're at
	//this will allow us to always have http://...#/view in our angular route
	
	var path = window.location.pathname.split("/");

	//get the last item in the array and make a nice name
	// everything in here is name and case sensitive. Be consistant with your
	// controllers, URLs, and template names	    
	var url = path[path.length - 1];
	var appName = url.toLocaleLowerCase().replace(/-/g, '');
	
	$routeProvider.when('/view', {
	    templateUrl: appName + '.html', //fetches from our cached templates
	    controller: appName + 'Ctrl'
	})
    .when('/view/:id', {
        templateUrl: appName + '.html', //fetches from our cached templates
        controller: appName + 'Ctrl'
    });	    
    //take the Kentico URL and reroute us to the angular #/view if we aren't there
	$routeProvider.otherwise({
	    redirectTo: '/view'    
	});
		
}];
