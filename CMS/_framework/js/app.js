'use strict';
var jquery = require('jquery'),
	angular = require('angular'),
	moment = require('moment'),	
	angularRoute = require('angularRoute'),
	angularResource = require('angularResource'),
	angularCookies = require('angularCookies'),
	angularSanitize = require('angularSanitize'),	
	bootstrap = require('bootstrap'),
    uibootstrapdatetimepicker = require('ui.bootstrap.datetimepicker'),
    uiBootstrap = require('ui.bootstrap'),
	bootstrapEditable = require('bootstrap.editable'),
    select2 = require('select2'),

	filters = require('./filters'),
    services = require('./services'),    
	directives = require('./directives'),
	controllers = require('./controllers');
	
// Declare app level module which depends on filters, and services
angular.module('apm', [
	'apm.filters',
	'apm.services',
	'apm.directives',
    'apm.templates',
    'apm.controllers',
	'ngRoute',
	'ngResource',
	'ngCookies',
	'ngSanitize',
	'ui.bootstrap'    
]).config(['$logProvider', function ($logProvider) {
    //TODO: Check Kentico Debugging to turn this on and off
    $logProvider.debugEnabled(true);
}])
.config(require('./routes.js'))
.run(['$rootScope', '$location', 'Modal', function ($rootScope, $location, Modal) {
    //use angular jq lite - this also fixes a bug when trying to view CMSDesk -> Pages
    var $ = angular.element;

    //tap into angular $location so we can prompt the user nicely that changes haven't been saved
    angular.forEach(angular.element("ul.CMSListMenuUL li a"), function (value, key) {
        var a = angular.element(value);
        var path = a.attr("href");
        var hash = "#/view";
        a.attr("href", path + hash);
        a.click(function (e) {
            e.preventDefault();            
            $rootScope.$broadcast('checkUnsavedData', path, hash);
            return false;
        })
    });

    $rootScope.$on("checkUnsavedData", function (event, path, hash) {
        //Navigate to newUrl if the form isn't dirty
        if ($(".editable-unsaved").length === 0) {
            $location.path(path);            
            $location.hash(hash);
            window.location = $location.url();
        }
        else {
            var modalOptions = {
                closeButtonText: 'Cancel',
                actionButtonText: 'Ignore Changes',
                headerText: 'Unsaved Changes',
                bodyText: 'You have unsaved changes. Leave the page?'
            };

            Modal.showModal({}, modalOptions).then(function (result) {
                if (result === 'ok') {                    
                    $location.path(path);                    
                    $location.hash(hash);
                    window.location = $location.url();
                }
            });
        }
    });

    // If mode is night, show in night mode
    if (localStorage.getItem("mode") == "night") {
        $('body').addClass('night');
        $('.mode-toggle').children('i').addClass('fa-sun-o').removeClass('fa-moon-o');
        $('.mode-toggle').children('.mode-name').text('Day Mode');
        $('.logo-container').children('img').attr('src', '../APM/media/Images/logo-apm-night.png?width=91&amp;height=25&amp;ext=.png');
    } else {
        $('body').addClass('day');
    }

    //menu navigation
    $('#menuElem > li').each(function () {
        if ($(this).children('.CMSListMenuUL').length) {
            $(this).children('a').after('<i class="fa fa-angle-down"></i>');
        } else {
            // click link when entire li clicked
            $(this).click(function () {
                window.location = $(this).children('a').attr('href');
            })
        }
    });
    var menuIcon = $('#menuElem').children('li').children('i');
    $('#menuElem').children('li').click(function (e) {
        $(this).children('.CMSListMenuUL').slideToggle();
        $(this).children('i').toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    })
    menuIcon.click(function (e) {
        e.stopPropagation();
        $(this).siblings('.CMSListMenuUL').slideToggle();
        $(this).toggleClass('fa-angle-down').toggleClass('fa-angle-up');
    })
    $('#menuElem').children('li').children('ul').children('li').click(function (e) {
        var goTo = $(this).children('a').attr('href');
        window.location = goTo;
        e.stopPropagation();
    })
    $('#menuElem').find('li').hover(function () {
        $(this).toggleClass('hovered');
    });
    $('#menuElem').children('li').last().children('a').after('<i class="fa fa-table"></i>');

    if ($('.CMSListMenuHighlightedLI').children('i').hasClass('fa-angle-down') || $('.CMSListMenuHighlightedLI').children('i').hasClass('fa-angle-up')) {
        $('.CMSListMenuHighlightedLI').children('i').removeClass('fa-angle-down').addClass('fa-angle-up');
    }
    $('.CMSListMenuHighlightedLI').children('.CMSListMenuUL').show();

    //user icon & logout
    $('.user-name').prepend('<i class="fa fa-user"></i>');
    $('.signoutLink').addClass('btn');

    //mini login
    $('.login-toggle').append('<i class="fa fa-caret-down"></i>').prepend('<i class="fa fa-lock"></i>');
    $('.login-toggle').click(function () {
        $(this).children('i:last-child').toggleClass('fa-caret-down');
        $(this).children('i:last-child').toggleClass('fa-caret-up');
        $(this).next('.login-mini-form').slideToggle('500');
    });

    //login page
    var loginPage = $('.LogonDialog').find('tbody');
    loginPage.find('td').addClass('login-td');
    loginPage.children('tr:first-child').find('input').attr('placeholder', 'Username').before('<i class="fa fa-user"></i>');
    loginPage.children('tr:nth-child(2)').find('input').attr('placeholder', 'Password').before('<i class="fa fa-lock"></i>');
    loginPage.children('tr').find('input[type=submit]').attr('value', 'login').after('<i class="fa fa-long-arrow-right"></i>');
    $('.DialogPosition').find('a').addClass('forgot-password').contents().replaceWith('Forgotten Password?');

    //dashboard navigation
    $('.dashboard-grid ul').children('li').click(function () {
        var goTo = $(this).children('a').attr('href');
        window.location = goTo;
    });
    var ps = $('#dashboard .dashboard-grid').children('li');
    var offset = 4;
    for (var i = 0; i < ps.size() ; i += offset) {
        ps.slice(i, i + offset).wrapAll('<ul></ul>');
    }

    //content navigation
    var activeUrl = $('.CMSListMenuHighlightedLI').find('.CMSListMenuHighlightedLI').children('a').attr('href');
    $('.dashboard-grid').find('li').each(function () {
        var gridUrl = $(this).children('a').attr('href');
        if (activeUrl == gridUrl) {
            $(this).addClass('hovered');
        }
    });

    //night/day mode toggle
    $('.mode-toggle').click(function () {
        if ($('body').hasClass('night')) {
            localStorage.setItem('mode', 'day');
            $(this).children('.mode-name').text('Night Mode');
            $(this).parents('body').removeClass('night').addClass('day');
            $(this).children('i').addClass('fa-moon-o').removeClass('fa-sun-o');
            $('.logo-container').children('img').attr('src', '../APM/media/Images/logo-apm.png?width=91&amp;height=25&amp;ext=.png');
        } else {
            localStorage.setItem('mode', 'night');
            $(this).children('.mode-name').text('Day Mode');
            $(this).parents('body').removeClass('day').addClass('night');
            $(this).children('i').removeClass('fa-moon-o').addClass('fa-sun-o');
            $('.logo-container').children('img').attr('src', '../APM/media/Images/logo-apm-night.png?width=91&amp;height=25&amp;ext=.png');
        }       
    }); 

}])


$(function ($) {
    //HACK: <div id="ng=app"> is defined on the kentico master page so we can make
    //  angular-xeditable work with kenticos <form>.
    //  put a timeout on it because we occassionally will get an angular error if we don't    
    window.setTimeout(function() { angular.bootstrap(document, ['apm']) }, 250);

    if (!window.console) console = { log: function () { } }; //IE fix for console
    console.log("loaded angular and dependencies.");
});