    'use strict';
    var xml2json = require('xml2json');
    
    /* Services */

    // Demonstrate how to register services

    /*
      When making rest calls we'll use the rest service built in to Kentico https://docs.kentico.com/display/K8/Kentico+REST+service
      Supported Kentico URL parameters: https://docs.kentico.com/display/K8/URL+parameters+supported+by+REST   
      Kentico REST data manipulation: https://docs.kentico.com/display/K8/Data+manipulation+methods+for+REST

      NOTE: because of the way angularjs works it won't accept a none array json object,
            we intercept the response using transformResponse in order to get it work right. 
            We should only have to do this for query and get.
    */
    angular.module('apm.services', [])
    .value('version', '0.1') // In this case it is a simple value service.    
	.config(['$httpProvider', function ($httpProvider) {
	    //HACK: (SMH) fix JSON dates so they play nice with POSTs to the REST SERVICE
	    //  http://stackoverflow.com/questions/10209867/replace-a-json-date-in-a-string-to-a-more-readable-date
	    function convertDateStringsToDates(input) {
	        var regexJson = /^\/Date\((d|-|.*)\)[\/|\\]$/;
	        // Ignore things that aren't objects.
	        if (typeof input !== "object") return input;

	        for (var key in input) {
	            if (!input.hasOwnProperty(key)) continue;

	            var value = input[key];
	            var match;
	            // Check for string properties which look like dates.
	            if (typeof value === "string" && (match = value.match(regexJson))) {
	                if (!isNaN(match[1])) {
	                    //format military time by default
	                    input[key] = new Date(Number(match[1]));//.format("MM/dd/yyyy HH:mm");
	                }
	            } else if (typeof value === "object") {
	                // Recurse into object
	                convertDateStringsToDates(value);
	            }
	        }
	    }
	    //intercept request an response object to perform any custom data manipulations
	    $httpProvider.interceptors.push(['$q', '$log', '$cookies', function ($q, $log, $cookies) {
	        return {
	            request: function (config) {				
	                // We need to have user with enough privleges to select/update our information.
	                var token = $cookies[".APMADVAUTHTOKEN"] || "";
                    
	                var authorization = 'Basic ' + token;
	                $log.debug("Intercepted Request URL: ", config.url);
	                $log.debug("Request auth: ", authorization);

	                $httpProvider.defaults.cache = false;
	                if (!$httpProvider.defaults.headers.get) {
	                    $httpProvider.defaults.headers.get = {};
	                }
	                $httpProvider.defaults.headers.common['Authorization'] = authorization;
	                // disable IE ajax request caching
	                $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';	                
	                return config || $q.when(config);
	            },
	            response: function (response) {	                
	                //fix JSON dates or things will break. Default JSON dates from MS services
	                // are in the format of /Date(number)/. If we post this back to the REST
	                // API it is invalid.
	                if(response.config.url.indexOf("/rest") > -1)
	                    convertDateStringsToDates(response);

	                $log.debug("Intercepted Response: ", response);
	                return response || $q.when(response);
	            }
	        }
	    }])
	}])
    .factory('Trip', ['$log', '$q', '$sce', 'Util', 'RestApi', 'Modal', function ($log, $q, $sce, Util, RestApi, Modal) {
        var Trip = {
            get: function (bLastTrip) {
                var deferred = $q.defer();

                var className = 'apm.tripinformation';
                var parms = { objecttype: 'customtableitem.', classname: className, where: "IsCurrent=1" };
                
                RestApi.resource.query(parms, function (data) {
                    // get the active trip
                    if (data.length > 0) {
                        return deferred.resolve(data[0]);
                    }
                    else {
                        var htmlMessage = $sce.trustAsHtml('<div class="alert alert-danger fade in">No Current Trip<br /><pre>Start the next trip to initialize this vessel.</pre></div>');
                        var modalOptions = {
                            closeButtonText: '',
                            actionButtonText: 'View Trip Information',
                            headerText: 'Missing Trip Configuration',
                            bodyText: htmlMessage
                        };
                        Modal.showModal({}, modalOptions).then(function (result) {
                            document.location = "/trip-information#/view";
                        });
                        //return deferred.resolve([]);
                    }
                }, function (e) {
                    Util.logError(e);
                    return deferred.reject(e);
                });

                return deferred.promise;
            },
            //Get a new trip number
            getNewTripNumber: function () {
                var deferred = $q.defer();
                var year = new Date().getFullYear();
                //get the previous trip number from this year and start from that
                RestApi.resource.query({ objecttype: 'customtableitem.', classname: 'apm.tripinformation', orderby: "TripNumber DESC", where: "TripNumber Like '%-" + year + "'", topn: 1 }, function (data) {
                    //the default trip number will be 001-YYYY. We assume it's this until we find something different
                    var newTripNum = "001-" + year;

                    //if we found a previous trip then increment the trip number for the year
                    if (data.length > 0) {
                        var lastTripNum = data[0].TripNumber.split("-");
                        //get a 0 padded number that is 1 greater than the last number
                        var newTripNum = ("00" + (parseInt(lastTripNum) + 1).toString()).substr(-3) + "-" + year;
                    }

                    return deferred.resolve(newTripNum);
                }, function (e) {
                    this.logError(e);
                    return deferred.reject(e)
                });

                return deferred.promise;
            }
        }
        return Trip;
    }])
    //Return Vessel Information, Current Trip Number and any other information Required for any Data manipulation
   .factory('Vessel', ['$log', '$q', 'RestApi', 'Modal', function ($log, $q, RestApi, Modal) {

       var Vessel = {
           get: function () {
               var deferred = $q.defer();

               //vessel information
               var vi = { BargeNumber: 0, TugNumber: 0 }
               var modalOptions = {
                   closeButtonText: '',
                   actionButtonText: 'OK',
                   headerText: 'Missing Vessel Information',
                   bodyText: 'This vessel is not setup properly'
               };

               RestApi.resource.query({ objecttype: 'customtableitem.', classname: 'apm.vesselinformation', orderby: "VesselBoatType DESC" }, function (data) {
                   //if we found a decklog for today then set some variables
                   //there should be exactly 2 entries: 1 Tug and 1 Vessel
                   if (data.length > 1) {
                       vi.TripNumber = 0;
                       vi.TugNumber = data[0].VesselNumber;
                       vi.BargeNumber = data[1].VesselNumber;
                       vi.VesselName = data[0].VesselName + '/' + data[1].VesselName;
                       return deferred.resolve(vi);
                   }
                   else {
                       // if we didn't find all the proper data then we need to redirect the user to
                       //the vessel information screen
                       Modal.showModal({}, modalOptions).then(function (result) {
                           document.location = "/Vessel-Information#/view";
                       });

                       return deferred.reject("Vessel Information was not properly configured.")
                   }
               }, function (e) {
                   $log.error(e.message || e.statusText);

                   Modal.showModal({}, modalOptions).then(function (result) {
                       document.location = "/Vessel-Information#/view";
                   });

                   return deferred.reject(e)
               });

               return deferred.promise;
           }
       }

       return Vessel;
   }])
   //Return Vessel Information, Current Trip Number and any other information Required for any Data manipulation
   .factory('LookupTables', ['$log', '$q', '$timeout', '$filter', '$http', 'RestApi', function ($log, $q, $timeout, $filter, $http, RestApi) {
       var tableCache = [
           //tables that don't need to have a Kentico Custom Table can go here
           // example: 
           //{
           //   name: "TestTable", data: [
           //        { id: 'ValueA', text: 'ValueA' }
           //        , { id: 'ValueB', text: 'ValueB' }
           //    ]
           //}
       ];

       var LookupTables = {
           //table cache                  
           //this covers most lookup tables
           get: function (tablename, idField, valueField, where, orderBy, topN) {
               var deferred = $q.defer();

               //the valueField is also the name of the field that has the data
               //so sort on this name ASC and we'll get alphabetical order
               orderBy = orderBy || (valueField || tablename) + " ASC, ItemOrder ASC";

               //we don't use GUID:Name, but Name:Name in most cases, so we'll allow for an override
               idField = idField || tablename;
               valueField = valueField || tablename;               

               //see if our table is already cached
               var table = $.grep(tableCache, function (obj) { return obj.name === tablename; });
               if (!table.length) {
                   table = { name: tablename, data: [] };

                   table.data.length ? null : RestApi.resource.query({ objecttype: 'customtableitem.', classname: 'apm.' + tablename.toLowerCase(), where: where, orderby: orderBy, topn:topN }, function (data) {
                       for (var item in angular.copy(data)) {
                           var i = { id: String(data[item][idField]), text: String(data[item][valueField]) };

                           //if this is the terminal we need the dockname too
                           if (tablename.toLowerCase() === "terminaldockname")
                           {
                               i.id = String(i.id) + String("-") + String(data[item]["Dock"]);
                               i.text = String(i.text) + String("-") + String(data[item]["Dock"]);
                           }
                           
                           table.data.push(i);
                       }
                       tableCache.push(table);
                       return deferred.resolve(table.data);
                   }, function (error) {
                       var message = "No " + tablename + " items found.";
                       $log.debug(message);
                       return deferred.reject(message);
                   });
               }
               else {                   
                   $timeout(function () {
                       return deferred.resolve(table[0].data);
                   }, 250);
               }
               
               return deferred.promise;
           },
           getCurrentUser: function () {
               var deferred = $q.defer();
               var data = {};

               $http({
                   method: 'POST',
                   url: '/CMSPages/WebService.asmx/GetCurrentUser',
                   data: data,
                   headers: {
                       'Accept': 'application/json, text/javascript, */*; q=0.01',
                       'Content-Type': 'application/json; charset=utf-8'
                   }
               }).success(function (data, status, headers, config) {

                   var currentUser = angular.copy(data.d);
                   return deferred.resolve(currentUser);


               }).error(function (data, status, headers, config) {
                   return deferred.reject('');
               });
               return deferred.promise;
           },
           getSystemTable: function (tablename, className, orderBy, where, idField, valueField) {
               var deferred = $q.defer();
              
               //see if our table is already cached
               var table = $.grep(tableCache, function (obj) { return obj.name === tablename; });
               if (!table.length) {
                   table = { name: tablename, data: [] };

                   table.data.length ? null : RestApi.resource.query({ classname: className, orderby: orderBy, where: where }, function (data) {
                       for (var item in angular.copy(data)) {
                           var textVal = String(data[item][valueField]);
                           var idVal = String(data[item][idField]);

                           //roles have a special character we want to remove for display purposes
                           if (className === "cms.role")
                           {
                               idVal = textVal = textVal.replace("* ", "");
                           }

                           var i = { id: idVal, text: textVal };
                           table.data.push(i);
                       }
                       tableCache.push(table);
                       return deferred.resolve(table.data);
                   }, function (error) {
                       var message = "No " + tablename + " items found.";
                       $log.debug(message);
                       return deferred.reject(message);
                   });
               }
               else {                   
                   $timeout(function () {
                       return deferred.resolve(table[0].data);
                   }, 10);
               }
               
               return deferred.promise;
           },
           GetCrewForDate: function (tablename, idField, valueField, date, doRefresh) {
               var deferred = $q.defer();

               //see if our table is already cached
               var table = $.grep(tableCache, function (obj) { return obj.name === tablename; });
               if (!table.length || doRefresh === true) {
                   table = { name: tablename, data: [] };
                   var data = angular.toJson({ 'date': date });
                   if (date === undefined) {
                       data = angular.toJson({ 'date': new Date()});
                   }
                   table.data.length ? null : $http({
                       method: 'POST',
                       url: '/CMSPages/WebService.asmx/GetCrewForDate',
                       data: data,
                       headers: {
                           'Accept': 'application/json, text/javascript, */*; q=0.01',
                           'Content-Type': 'application/json; charset=utf-8'
                       }
                   }).success(function (data, status, headers, config) {

                       var data = data.d;
                       for (var item in angular.copy(data)) {
                           var textVal = String(data[item][valueField]);
                           var idVal = String(data[item][idField]);

                           var i = { id: idVal, text: textVal };
                           table.data.push(i);
                       }
                       tableCache.push(table);                       
                       return deferred.resolve(table.data);

                      
                    }).error(function (data, status, headers, config) {
                        var message = "No Current Crew found.";
                        $log.debug(message);
                        return deferred.reject(message);
                    });
               }
               else {
                   $timeout(function () {                       
                       return deferred.resolve(table[0].data);
                   }, 10);
               }

               return deferred.promise;
           },
           //this covers most lookup tables
           show: function (tablename, data, dataId) {               
               var table = $.grep(tableCache, function (obj) { return obj.name === tablename; });
               if (table.length === 0) {
                   return "Not set";
               }
               else {
                   var selected = $filter('filter')(table[0].data, { id: dataId });
                   if (selected.length === 0)
                       return dataId || "";
                   else
                       return (table.length && dataId && table[0].data.length) ? selected[0].text : (dataId || "");
               }

           }

                    
       }
       return LookupTables;
   }])        
    //Return whether the current user has the ability to edit the current screen
    .factory('Security', ['$log', '$q', '$http', function ($log, $q, $http) {
        var Security = {
            get: function (className) {
                var deferred = $q.defer();
                var data = angular.toJson({ 'className': className });
                //get this user
                $http({
                    method: 'POST',
                    url: '/CMSPages/WebService.asmx/GetUserPermissions',
                    data: data,
                    headers: {
                        'Accept': 'application/json, text/javascript, */*; q=0.01',
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                .success(function (data, status, headers, config) {
                    //if we have a valid list of roles then tell us if we can edit the
                    //current page based on the security roles passed in                    
                    if (data.d != "null") {
                        var json = angular.fromJson(data.d);                       
                        deferred.resolve(json);
                    }
                    else {
                        deferred.resolve(false);
                    }
                })
                .error(function (data, status, headers, config) {
                    $log.error(status);
                    deferred.reject("Error finding user permissions");
                });

                return deferred.promise;
            },
            set: function (userId, roleName) {
                var deferred = $q.defer();
                var data = angular.toJson({ 'userId': userId, 'roleName': roleName });
                //get this user
                $http({
                    method: 'POST',
                    url: '/CMSPages/WebService.asmx/SetUserRole',
                    data: data,
                    headers: {
                        'Accept': 'application/json, text/javascript, */*; q=0.01',
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                })
                .success(function (data, status, headers, config) {
                    //if we have a valid list of roles then tell us if we can edit the
                    //current page based on the security roles passed in                    
                    if (data.d != "null") {
                        var json = angular.fromJson(data.d);                       
                        deferred.resolve(json);
                    }
                    else {
                        deferred.resolve(false);
                    }
                })
                .error(function (data, status, headers, config) {
                    $log.error(status);
                    deferred.reject("Error setting user permissions");
                });

                return deferred.promise;
            },
            // find an the index of an item in a json object
            getObjectKeyIndex: function (obj, keyToFind) {                
                var i = 0, key;
                for (key in obj) {
                    if (key === keyToFind) {
                        return i;
                    }
                    i++;
                }
                return null;
            }
        };
        return Security;
    }])
    .factory('Base', ['$log', '$sce', '$q', '$timeout', 'Modal', 'Vessel', 'Trip', 'Security', function ($log, $sce, $q, $timeout, Modal, Vessel, Trip, Security) {
        var Base = {
            //get the basic information that nearly every screen will requie
            // this is mostly vessel, trip and security information
            get: function (className) {
                var deferred = $q.defer();
                var base = { security: null, trip: null, vessel: null };
                Security.get(className).then(function (result) {                    
                    base.security = result;

                    //get current trip data
                    Trip.get().then(function (result) {
                        base.trip = result;

                        // Fetch Vessel
                        Vessel.get().then(function (result) {
                            base.vessel = result;

                            return deferred.resolve(base);
                        });
                    });
                });

                return deferred.promise;
            },
        }

        return Base;
    }])
    //Utilities that will be used throughout the app
    .factory('Util', ['$log', '$sce', '$q', '$timeout', '$cookies', 'Modal', function ($log, $sce, $q, $timeout, $cookies, Modal) {
        var observerCallbacks = [];

        var Util = {
            pageAlerts: [],
            getDate: function (bDateOnly, date) {
                var now = date || new Date();
                return now.format(this.dateFormat(bDateOnly));
            },
            getTime: function (date) {
                var now = date || new Date();
                return now.format('HH:mm');
            },
            dateFormat: function(bDateOnly)
            {
                return bDateOnly ? 'MM/dd/yyyy' : "MM/dd/yyyy HH:mm";
            },
            dateTimePickerOptions: function (bDateOnly) {
                if (bDateOnly) {
                    return {
                        format: "MM/dd/yyyy",
                        startView: 'month',
                        minView: 'month',
                        maxView: 'month',
                        viewSelect: 'month',
                        viewMeridian: false,
                        minuteStep: 1,
                        autoClose: true
                    };
                }
                else {
                    //options for a date and time picker
                    return {
                        format: "MM/dd/yyyy HH:mm",
                        startView: 'month',
                        minView: 'hour',
                        maxView: 'month',
                        viewSelect: 'month',
                        viewMeridian: false,
                        minuteStep: 1,
                        autoClose: true
                    };
                }
            },
            isEmpty: function(str) {
                return (!str);
            },
            getEmptyGuid: function ()
            {
                return "00000000-0000-0000-0000-000000000000";
            },
            parseFloat: function(num, places) 
            {
                return parseFloat(parseFloat(num).toFixed(places || 2).replace(/\.?0+$/, ""));
            },
            getDateDiffHoursAsFloat: function(earliestDate, latestDate)
            {
                //take two dates, subtract them and return the number of hours.minutes
                return Util.parseFloat((((new Date(latestDate) - new Date(earliestDate)) / 60) / 1000) / 60, 2);
            },
            // log an error and show a modal to the user
            logError: function (error)
            {
                var message;
                if (typeof error === "string") {
                    //attempt to determine if some fields were missing and tell the user;
                    if (error.indexOf("message") > -1 && error.indexOf("data") > -1)
                    {
                        var json = angular.fromJson(error);
                        var invalidFields = new Array();
                        for (var i = 0; i < Object.keys(json.data).length; i++) {
                            // some fields we want to ignore because they are core items that a user 
                            // should never have to fill in, so we don't want to show these when
                            // we guess at what might be missing.
                            if (json.data[Object.keys(json.data)[i]] === null
                                && Object.keys(json.data)[i].indexOf("GUID") === -1
                                && Object.keys(json.data)[i].indexOf("ItemOrder") === -1
                                && Object.keys(json.data)[i].indexOf("ModifiedByName") === -1
                                && Object.keys(json.data)[i].indexOf("SyncDateTime") === -1
                                && Object.keys(json.data)[i].indexOf("IsDeleted") === -1)
                            {
                                // insert a space before all cap letters for nicer names
                                invalidFields.push(Object.keys(json.data)[i]
                                    .replace(/([A-Z])/g, ' $1')
                                    .replace(/^./, function (str) { return str.toUpperCase(); })
                                    .trim()
                                );
                            }
                        }

                        message = json.message;
                        if (invalidFields.length > 0) {
                            message += "<br/>The following fields are missing and may be required: <br/><br/>";
                            message += invalidFields.join(", ");
                        }
                    }
                    else {
                        message = error;
                    }
                }
                else {
                    message = error.message || error.statusText;
                }

                //looks specifically for some access messages
                //base on who the user is and display a custom message
                var htmlMessage;
                var headerText = "System Error";
                

                switch (message) {
                    case "Forbidden":
                        message = "You do not have permission to perform this operation";
                        $log.error(message)
                        htmlMessage = $sce.trustAsHtml('<div class="alert alert-danger fade in">An system level error occurred. Please contact support if this error persists.<br /><pre>' + message + '</pre></div>');
                        break;
                    case "Unauthorized":
                        message = "You are not currently logged in. <a href='/Login'>Login</a> now?";
                        htmlMessage = $sce.trustAsHtml('<div class="alert alert-danger fade in">An system level error occurred. Please contact support if this error persists.<br /><pre>' + message + '</pre></div>');
                        break;
                    default:
                        headerText = "Required fields";
                        htmlMessage = $sce.trustAsHtml('<div class="alert alert-warning fade in">Required fields missing<br /><pre>' + message + '</pre></div>');
                        break;
                };               

                var modalOptions = {
                    closeButtonText: '',
                    actionButtonText: 'OK',
                    headerText: headerText,
                    bodyText: htmlMessage
                };

                //show modal
                Modal.showModal({}, modalOptions);
            },
            notifyEditableSave: function (index, className, bClear)
            {
                //index is the index of the row
                if (!bClear) {
                    angular.element("section." + className.replace(".", "\\.") + " .row-" + index).addClass("editable-unsaved");
                }
                else {
                    //index is the data-pk value                    
                    angular.element("section." + className.replace(".", "\\.") + " .row .cell a[data-pk='" + index + "']").parents('div[ng-repeat]').removeClass("editable-unsaved")
                }
            },
            //register an observer
            registerObserverCallback: function (callback) {
                observerCallbacks.push(callback);
            },
            notifyObservers: function(){
                angular.forEach(observerCallbacks, function (callback) {
                    callback();
                });
            },
            addAlert: function (message, type) {
                type = type || "warning";
                var a = { msg: $sce.trustAsHtml(message), type: type };

                //don't put duplicate messages in the alert array.
                var exists = $.grep(this.pageAlerts, function (obj) { return obj.msg.$$unwrapTrustedValue() === message; });
                if (exists.length === 0) {
                    this.pageAlerts.push(a);

                    this.notifyObservers();

                    //fade out successes
                    if (type === "success" || type === "warning") {

                        var deferred = $q.defer();
                        $timeout(function () {
                            var idx = Util.pageAlerts.indexOf(a);

                            if (idx > -1) {
                                Util.pageAlerts.splice(idx, 1);
                            }

                        }, 3000)
                    }
                }
                return this.pageAlerts;
            },
            closeAlert: function (index) {
                this.pageAlerts.splice(index, 1);
                return this.pageAlerts;
            },           
            validate: function (data, validationType)
            {
                // check for specific types
                if (validationType) {
                    switch (validationType.toLowerCase()) {
                        case "number":
                            if (isNaN(data))
                                return "Field must be a number";
                            break;
                    }
                }

                // check for empty
                if (Util.isEmpty(data)) {
                    return "Field is required.";
                }
                
                

                //else we passed validation
                return null;
            },
            prepForSave: function (data)
            {
                // if some specific objects exist in our data then 
                // set some default values that we want everytime we see these
                // and the data is about to be saved

                //clear the sync times so we know it's not synced
                if (typeof (data.BoatSyncDateTime) === "object")
                    data.BoatSyncDateTime = null;
                if (typeof (data.PortSyncDateTime) === "object")
                    data.PortSyncDateTime = null;
                if (typeof (data.SPSyncDateTime) === "object")
                    data.SPSyncDateTime = null;

                //keep track of who created this item since we're always saving with a single kentico user
                if (typeof (data.ModifiedByName) === "object")
                    data.ModifiedByName = $cookies[".APMCURRENTUSER"];

                //iterate over any object and set empyt and undefined items to null values
                //so we can properly save back to the database.
                var obj = angular.copy(data);
                for (var i in obj) {                    
                    // set empty values to null
                    if (obj[i] === "" || typeof (obj[i]) === "undefined")
                        obj[i] = null;
                };               

                return obj;
            },
            showEditable: function (index //index of the row that is active
                                    , table //find the proper table on the screen if there is more than one
                                    , colIndex //index of the column that we want to auto open an xeditable control on - Default 0
                                   )
            {
                //Account for multiple tables on one screen
                $timeout(function () {
                    if (table) {
                        //escape the period that's in the classname so the selector can search for classnames that have a dot in them
                        angular.element("section." + table.replace(".", "\\.") + " .row-" + index + " div.cell a[editable]:eq(" + (colIndex || 0) + ")").editable('show');
                    }
                    else {
                        angular.element(".row-" + index + " div.cell a[editable]:first").editable('show');
                    }
                }, 25);
            },
            displayName: function (str)
            {
                //make a nice display name
                return str.replace("APM_", "").replace(/([A-Z])/g, ' $1').replace(/^./, function (str) { return str.toUpperCase(); })
            },
            initSelect2: function (elem, initialValue, allowEmpty) {                

                if (allowEmpty === undefined) {
                    allowEmpty = false;
                }

                $timeout(function () {
                    $(elem).select2({
                        allowClear: allowEmpty,
                        theme: 'bootstrap'
                    });         
                });
            }
        };

        return Util;
    }])
    // Modal dialog Service
    .factory('Modal', ['$modal', function ($modal) {
        // http://weblogs.asp.net/dwahlin/building-an-angularjs-modal-service
        return {
            modalDefaults: {
                backdrop: true,
                keyboard: false,
                modalFade: true,
                templateUrl: 'modal.html'
            },

            modalOptions: {
                closeButtonText: 'Close',
                actionButtonText: 'OK',
                headerText: 'Proceed?',
                bodyText: 'Perform this action?',
                callbackOnCancel: false
            },
            showModal: function (customModalDefaults, customModalOptions) {
                if (!customModalDefaults) customModalDefaults = {};
                customModalDefaults.backdrop = 'static';
                return this.show(customModalDefaults, customModalOptions);
            },
            show: function (customModalDefaults, customModalOptions) {
                //Create temp objects to work with since we're in a singleton service
                var tempModalDefaults = {};
                var tempModalOptions = {};

                //Map angular-ui modal custom defaults to modal defaults defined in service
                angular.extend(tempModalDefaults, this.modalDefaults, customModalDefaults);

                //Map modal.html $scope custom properties to defaults defined in service
                angular.extend(tempModalOptions, this.modalOptions, customModalOptions);

                if (!tempModalDefaults.controller) {
                    tempModalDefaults.controller = function ($scope, $modalInstance) {
                        $scope.modalOptions = tempModalOptions;
                        $scope.modalOptions.ok = function (result) {
                            $modalInstance.close(result || 'ok');
                        };
                        $scope.modalOptions.close = function (result) {
                            if (customModalOptions.callbackOnCancel) {
                                $modalInstance.close(result || 'cancel');
                            }
                            else {
                                $modalInstance.dismiss('cancel');
                            }
                        };
                    }
                }

                return $modal.open(tempModalDefaults).result;
            }
        }
    }])
    // A custom resource that defines the basics of a resource request
    .factory('Resource', ['$resource', function ($resource) {
        //Custom Resource 
        //NOTE: so we can globally setup our defaults and do some stuff in once place instead of in every service
        return function (url, params, methods) {
            var defaults = {
                update: { method: 'put', isArray: false },
                delete: { method: 'delete' }
            };
            methods = angular.extend(defaults, methods);
            var resource = $resource(url, params, methods);
            return resource;
        };
    }])
    // Define a generic method for fetching Kentico objects via REST
    .factory('RestApi', ['$q', '$timeout', '$cookies', '$route', 'Util', 'Resource', 'RecordSchemaApi', 'Modal', function ($q, $timeout, $cookies, $route, Util, $resource, RecordSchemaApi, Modal) {
        var RestApi = {
            resource: $resource('/rest/:objecttype:classname/:id', { objecttype: "@objecttype", classname: "@classname", id: "@id", format: "json" },
            {                
                query: {
                    method: 'GET',
                    isArray: true,
                    transformResponse: function (data, headers) {
                        if (data) {
                            var json = angular.fromJson(data);

                            //this return an empty array in the following conditions:
                            //1. No records are returned
                            //2. An error occurred (either from the server or because the format of the request was wrong)                        
                            return RestApi.getData(json);
                        }
                        else return "";
                    }
                },
                get: {
                    method: 'GET',
                    isArray: true,
                    transformResponse: function (data, headers) {
                        var json = angular.fromJson(data);
                        return RestApi.getData(json);
                    }
                }
            }),
            list: function (scopeObj //the scope object we are tied to                                                    
                            , where //where statement to pass to the rest api 
                            , orderby
                            , topn                            
                            , showDeleted
                           )
            {
                where = where || '';
                orderby = orderby || '';
                topn = topn || '';
                showDeleted = showDeleted || false;

                var tablename = scopeObj.customTableName;
                var classname = scopeObj.classname;
                var objecttype = scopeObj.objecttype || 'customtableitem.';

                var deferred = $q.defer();

                //filter out deleted records by default
                if (!showDeleted) {
                    if (!Util.isEmpty(where)) {
                        where += " AND ";
                    }
                    where += "ISNULL(IsDeleted,0)!=1";
                }

                RestApi.resource.query({ objecttype: objecttype, classname: classname, where: where, topn: topn, orderby: orderby }, function (data) {
                    //for processing child objects we need to pass required data
                    //at the root of the object so when we iterate over the child objects
                    // and perform deletes/saves we know the the required kentico stuff.
                    data.objecttype = objecttype;
                    data.tablename = tablename;
                    data.classname = classname;
                    deferred.resolve(data);

                }, function (e) {
                    Util.logError(e);
                    deferred.reject(e);
                });

                return deferred.promise;
            },
            save: function (scopeObj //the scope object we are tied to                                                     
                            , data //data to update                            
                            , overrideName // for marking saves and popup of editable
                           )
            {
                var tablename = scopeObj.customTableName;                
                var classname = scopeObj.classname;
                var objecttype = scopeObj.objecttype || 'customtableitem.';

                var deferred = $q.defer();

                //since we don't want to have to put every field on the form if we don't
                //need it make sure we save the full object and strip any unneeded angular stuff
                var jsonObj = Util.prepForSave(data);

                //if we have an id of 0 then this is an insert
                var id = jsonObj.ItemID;             
                
                //determine if this a save/insert
                if (id === 0) {
                    //then make a new record
                    RestApi.resource.save({ objecttype: objecttype, classname: classname }, jsonObj, function (response) {                        
                        //update the new object IDs from the returned insert.
                        data.ItemID = eval("response." + tablename + ".ItemID");
                        data.ItemGUID = eval("response." + tablename + ".ItemGUID");

                        Util.addAlert("Created new " + Util.displayName(tablename), "success");

                        //mark the row as saved
                        $timeout(function () {
                            Util.notifyEditableSave(data.ItemID, overrideName || classname, true);
                        }, 150);

                        deferred.resolve(data);                        

                    }, function (err) {
                        var message = "{ \"message\": \"Error creating" + Util.displayName(tablename) + "\", \"data\": " + JSON.stringify(jsonObj) + "}"
                        Util.logError(message);
                        deferred.reject(message);
                    });
                }
                else {
                    //otherwise we update an existing record
                    RestApi.resource.update({ objecttype: objecttype, classname: classname, id: id }, jsonObj, function (response) {
                        Util.addAlert("Updated " + Util.displayName(tablename), "success");

                        //mark the row as saved
                        $timeout(function () {
                            Util.notifyEditableSave(data.ItemID, overrideName || classname, true);
                        }, 150);

                        deferred.resolve(data);                        
                    }, function (err) {
                        var message = "{ \"message\": \"Error updating" + Util.displayName(tablename) + "\", \"data\": " + JSON.stringify(jsonObj) + "}"
                        Util.logError(message);
                        deferred.reject(message);
                    });
                }                

                return deferred.promise;
            },
            insert: function (pageScope //page scope
                            , scopeObj //the scope object we are tied to                                                        
                            , parentId // the id of any parent tables this record is associated with
                            , autoOpenIndex // the index of the xeditable elemetn we want to auto open
                            , isSingleItem // is the data a single item of data only?
                            , overrideName // for marking saves and popup of editable
                           )
            {
                var tablename = scopeObj.customTableName;                
                var classname = scopeObj.classname;
                var objecttype = scopeObj.objecttype || 'customtableitem.';

                var deferred = $q.defer();

                //if we don't know what the schema is, go get it.
                if (!scopeObj.schema) {
                    //Fetch the schema for a new record
                    RecordSchemaApi.resource.get({ classname: classname }, function (data) {
                        scopeObj.schema = data.schema;
                        RestApi.newRecord(deferred, pageScope, scopeObj, parentId, autoOpenIndex, isSingleItem, overrideName);

                    }, function (e) {
                        Util.logError(e);
                        deferred.reject(e);
                    })
                }
                else {
                    //make a copy of our schema and add some defaults
                    RestApi.newRecord(deferred, pageScope, scopeObj, parentId, autoOpenIndex, isSingleItem, overrideName);
                }

                return deferred.promise;
            },
            newRecord: function (deferred  // pass the deferred promise
                                , pageScope // the page scope object page.XXX
                                , scopeObj // the scope object                                
                                , parentId // link back to a parent table                                
                                , autoOpenIndex // the index of the xeditable elemetn we want to auto open
                                , isSingleItem // is the data a single item of data only?
                                , overrideName // for marking saves and popup of editable
                            )
            {
                var schema = scopeObj.schema;                
                var classname = scopeObj.classname;

                try {
                    
                    if (!scopeObj.schema)
                        scopeObj.schema = schema;

                    //make a deep copy of our object
                    var newRecord = angular.copy(scopeObj.schema);

                    /* Get Trip Information */
                    if ($route.current.loadedTemplateUrl.toLowerCase() !== 'tripinformation.html') {
                        newRecord.TripNumber = pageScope.trip.TripNumber;
                    }
                    /* Get Vessel Information */
                    if ($route.current.loadedTemplateUrl.toLowerCase() !== 'vesselinformation.html') {
                        newRecord.BargeNumber = pageScope.vessel.BargeNumber;
                        newRecord.TugNumber = pageScope.vessel.TugNumber;
                    }
                    
                    //* BEGIN set some defaults
                    newRecord.ItemID = 0;
                    newRecord.ItemGUID = Util.getEmptyGuid();
                   
                    //* END set defaults
                   
                    //add it a scoped variable so we know to show the input
                    //fields for our newly added row
                    scopeObj.inserted = newRecord;

                    //push a copy of our schema as a new array item
                    var aryln;                   
                    if (isSingleItem)
                    {
                        scopeObj.data = [];
                    }

                    aryln = scopeObj.data.push(newRecord);

                    deferred.resolve({ scopeObj: scopeObj, insertedIndex: aryln-1, parentId: parentId });

                    //show the editable popup for the first item
                    $timeout(function () {
                        if (autoOpenIndex !== -1) {
                            Util.showEditable(aryln - 1, overrideName || classname, autoOpenIndex);
                        }
                        Util.notifyEditableSave(aryln - 1, overrideName||classname);
                    }, 100);
                }
                catch (e) {
                    Util.logError(e);
                    deferred.resolve(e);
                }
            },
            delete: function (scopeObj, index, childObjectsArray, childWhere, deleteWithoutModal) {
                var tablename = scopeObj.customTableName;
                var classname = scopeObj.classname;
                var objecttype = scopeObj.objecttype || 'customtableitem.';

                var deferred = $q.defer();

                var modalOptions = {
                    closeButtonText: 'Cancel',
                    actionButtonText: 'Delete',
                    headerText: 'Confirm Delete',
                    bodyText: 'Are you sure you want to delete this ' + Util.displayName(tablename) + '?',
                    callbackOnCancel: true
                };

                // the actual delete action
                // SMH - 01/29/2015 - implementing faux delete where we set the IsDelete bit to true if the item is already
                //                      in the database. If the item was not yet saved to the database then it we just drop it
                var doDelete = function () {
                    //get the id of the item to delete
                    var id = scopeObj.data[index].ItemID;                    

                    // if this record hasn't been added to the DB remove it from the scope object
                    if (Util.isEmpty(id)) {
                        //remove the item from the array on the scopeObj so angular knows what happened 
                        // even if we didn't need to mark it in the database
                        scopeObj.data.splice(index, 1);
                        deferred.resolve(scopeObj.data);
                    }
                    else {
                        //If this record is in the database then ...                        
                        var jsonObj = Util.prepForSave(scopeObj.data[index]); // prep the record for updating
                        jsonObj.IsDeleted = true; //mark the record as deleted                        

                        //update the record
                        RestApi.resource.update({ objecttype: objecttype, classname: classname, id: id }, jsonObj, function (response) {
                            //remove the item from the array on the scopeObj so angular knows what happened                                            
                            scopeObj.data.splice(index, 1);

                            //track the processed Index for deleting child object
                            scopeObj.data.processedIndex = index;

                            //return the deferred
                            deferred.resolve(scopeObj.data);
                        }, function (e) {
                            Util.logError(e);
                            deferred.reject(e);
                        });
                    }
                }

                // when deleting child objects we don't want a modal to popup for confirmation everytime it attempts a delete
                if (deleteWithoutModal) {
                    doDelete();
                }
                else {
                    //confirm the delete
                    Modal.showModal({}, modalOptions).then(function (result) {
                        if (result === 'ok') {
                            //handle deletes of related objects
                            if (!Util.isEmpty(childObjectsArray) && !Util.isEmpty(childWhere)) {
                                //keep track of whether all the child objects have been deleted
                                // after we know they have then we'll delete the parent
                                var childObjCount = 0;
                                for (var i = 0; i < childObjectsArray.length; i++) {
                                    RestApi.list(childObjectsArray[i], childWhere).then(function (childListResult) {
                                        if (!Util.isEmpty(childListResult) && childListResult.length > 0) {
                                            //manually massage our object so everything is in the right format                                            
                                            var childData = {
                                                'customTableName': childListResult.tablename, classname: childListResult.classname, objecttype: childListResult.objecttype,
                                                'data': angular.copy(childListResult)
                                            };

                                            // delete each of the child objects
                                            for (var j = 0; j < childData.data.length; j++) {                                                
                                                RestApi.delete(childData, j, null, null, true);
                                            }                                            
                                        }
                                    });
                                }                                
                            }

                            // delete the parent object
                            doDelete();
                        }
                        else {
                            deferred.resolve(scopeObj.data);
                        }
                    });                    
                }
                return deferred.promise;
            }
        };

        // parse the object Returned from a Kentico Rest call
        RestApi.getData = function (json) {
            /* sample Kentico json object - we need to parse this */
            /*
            * "{"customtableitem_apm_decklogs": 
                [
                    {"APM_DeckLog": 
                    [
                        { <data> }
                    ]
                    }
                    ,{"TotalRecords": 
                    [
                        {"TotalRecords":"1"}
                    ]
                    }
                ]}
            *
            */
            //get the first level of item
            // customtableitem_apm_decklogs
            var firstProp;
            for (var key in json) {
                if (json.hasOwnProperty(key)) {
                    firstProp = json[key];
                    break;
                }
            }

            //now get the data from this first item
            // APM_DeckLog or if we Didn't return any data we return "Table" which is empty
            var secondprop;
            for (var key in firstProp[0]) {
                if (firstProp[0].hasOwnProperty(key)) {
                    secondprop = firstProp[0][key];
                    break;
                }
            }

            return secondprop || [];
        };
        
        return RestApi;
    }])
    // Get the schmea for an object as JSON so we can add records
    .factory('RecordSchemaApi', ['Resource', function ($resource) {
        //Kentico Record Schema factory - get the definition of a table so we can create a new object
        // based on this definition. The definition is buried as XML inside our object, so we need
        // to dig it out from the json object, then convert the XML to json then parse it to create
        // our json object that we can fill with data and submit back to the server when we add a new empty item
        var RecordSchemaApi = {
            resource: $resource('/rest/:objecttype/:classname', { objecttype: "cms.customtable", classname: "@classname", format: "json" },
            {
                get: {
                    method: 'GET',
                    transformResponse: function (data, headers) {
                        var emptySchema = angular.fromJson('{"schema":{} }');
                        if (!data)
                            return emptySchema;

                        var json = angular.fromJson(data);                        
                        var xmlSchema = $.parseXML(json.data.ClassXmlSchema);

                        //parsing gives us a rogue "undefined"
                        var schema = xml2json(xmlSchema).replace("undefined", "");

                        //turn it back to json
                        json = angular.fromJson(schema);
                        //grab the columns
                        var cols = json["xs:schema"]["xs:element"]["xs:complexType"]["xs:choice"]["xs:element"]["xs:complexType"]["xs:sequence"]["xs:element"];

                        //make or final json string
                        
                        //ignored columns we don't need to pass back to kentico because they are auto filled                        
                        var ignoreArray = new Array("ItemCreatedBy","ItemModifiedBy","ItemCreatedWhen", "ItemModifiedWhen", "ItemOrder");

                        var output = '{"schema":{';
                        for (var col in cols)
                        {
                            if ($.inArray(cols[col]["@name"], ignoreArray) === -1) {
                                output += '"' + cols[col]["@name"] + '":null';
                                if (col < cols.length - 1) output += ',';
                            }
                        }
                        output += '}}';

                        //turn it into real json                        
                        json = angular.fromJson(output);                        
                        return json || emptySchema;
                    }
                }
            })};
    
        return RecordSchemaApi;
    }])
    // Get the schmea for an object as JSON so we can add records
    .factory('VesselForm', ['$http', '$q', '$log', '$timeout', 'Util', function ($http, $q, $log, $timeout, Util) {
        var VesselForm = {
            export: function (formGuid, tripNumber, date, id)
            {
                var deferred = $q.defer();
                var data = {formGuid:formGuid, tripNumber:tripNumber, date:date, id:id};

                $http({
                    method: 'POST',
                    url: '/CMSPages/WebService.asmx/ExportVesselForm',
                    data: data,
                    headers: {
                        'Accept': 'application/json, text/javascript, */*; q=0.01',
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                }).success(function (data, status, headers, config) {
                    var data = data.d;
                    if (data.success) {
                        Util.addAlert("Creating vessel form...");

                        //open the file on the users computer
                        $timeout(function () {
                            Util.addAlert("Vessel form created", "success");
                            if (data.datalist.length > 0) {
                                for (var f = 0; f < data.datalist.length; f++) {
                                    var frm = data.datalist[f];

                                    try {
                                        console.log("Attempting to open file: " + frm);
                                        var wnd = window.open(frm, "Open " + frm.replace(/^.*[\\\/]/, ''), "height=1,width=1,status=no,titlebar=no,toolbar=no,top=0,left=0,menubar=no", false);
                                    }
                                    catch (ex) {
                                        console.log(ex + ". Error trying to open file: " + frm);
                                        //if we can't find the file, wait then try one more time.
                                        $timeout(function () {
                                            try {
                                                console.log("Trying one last time to open file: " + frm);
                                                var wnd = window.open(frm, "Open " + frm.replace(/^.*[\\\/]/, ''), "height=1,width=1,status=no,titlebar=no,toolbar=no,top=0,left=0,menubar=no", false);
                                            }
                                            catch (ex) {                                                
                                                console.log(ex + ". Error trying to open file: " + frm);

                                                //HACK: work around an issue with sometimes timing out when attemting to open a document.
                                                var link = wnd.document.createElement('a');
                                                link.href = frm;
                                                wnd.document.body.appendChild(link);
                                                link.click();
                                            }
                                        }, 3000);
                                    }                                   
                                }
                            }
                        }, 1000);

                        return deferred.resolve(data.message);
                    }
                    else {                        
                        Util.addAlert(data.message, "danger");
                        return deferred.reject(data.message);
                    }
                }).error(function (data, status, headers, config) {
                    var message = "There was an error exporting vessel form.";
                    if (data) {
                        if (data.d) {
                            message += data.d.message;
                        }
                        if (data.Message) {
                            message += data.Message;
                        }
                    }
                    Util.addAlert(message, "danger");
                    return deferred.reject(data.message);
                });

                return deferred.promise;
            }
        }
        
        return VesselForm;
    }])

