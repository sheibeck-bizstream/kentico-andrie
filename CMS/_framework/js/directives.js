'use strict';

  /* Directives */

	angular.module('apm.directives', ['apm.services'])
		.directive('appVersion', ['version', function (version) {
		    return function (scope, elm, attrs) {
		        elm.text(version);
		    };
		}])
        .directive('editable', function ($timeout, $filter) {
            return {
                restrict: 'A',
                require: "ngModel",
                scope: {
                    editable: '@',
                    save: '&',
                    disabled: '@',
                    pk: '@'
                },
                link: function (scope, element, attrs, ngModel) {
                    //set some default attributes
                    attrs.$set("title", (attrs.title || attrs.placeholder) || "Click to edit");
                    attrs.$set("data-emptytext", attrs.emptytext || ". . .");
                    attrs.$set("data-autotext", attrs.autotext || ". . .");
                    
                    //xeditable setup
                    var loadXeditable = function () {
                        if (scope.editable) { 
                            angular.element(element).editable({
                                disabled: (scope.editable === "false" || scope.editable == true) ? true : false,
                                validate: function (value) {                                   
                                    if (element.attr("required") && !value) {
                                        return 'This field is required';
                                    }
                                    if (element.attr("data-pattern"))
                                    {
                                        var re = new RegExp(element.attr("data-pattern"), "i")
                                        if (!re.test(value)) {
                                            return element.attr("data-validate-fail") || "Data is not in the right format";
                                        }
                                    }

                                    //limit a date to a specific month
                                    if (element.attr("data-limit-month"))
                                    {
                                        var requiredMonth = element.attr("data-limit-month");
                                        var currentMonth = value.split('/')[0];
                                        if (requiredMonth !== currentMonth) {
                                            return "Month must match the current month.";
                                        }
                                    }

                                    //limit a date to a specific year
                                    if (element.attr("data-limit-year"))
                                    {
                                        var requiredYear = element.attr("data-limit-year");
                                        var currentYear = value.split('/')[2].split(' ')[0];                                        
                                        if (requiredYear !== currentYear) {
                                            return "Year must match the current year.";
                                        }
                                    }
                                },
                                display: function (value, srcData) {         
                                    if (value || value === "") {
                                        if (value._isAMomentObject) {
                                            value = $filter('moment')(value._d, value._f);
                                        }

                                        // add the colon back into time entries
                                        if (attrs.$attr.apmTime && value.length == 4)
                                        {
                                            value = [value.slice(0, 2), ':', value.slice(2)].join('');
                                        }

                                        // add the colon back into time entries for full date time values
                                        if (attrs.$attr.apmDatetime && value.length == 15) {
                                            value = [value.slice(0, 13), ':', value.slice(13)].join('');
                                        }

                                        ngModel.$setViewValue(value);
                                        scope.$apply();
                                    }                                    
                                },
                                url: function (params) {
                                    var d = new $.Deferred;
                                    
                                    //HACK: the first time we add a new row and save it, params.pk isn't getting
                                    //updated, so make sure we enable auto saving
                                    var new_pk_value = parseInt(attrs.pk);
                                    if (!isNaN(new_pk_value) && new_pk_value > 0)
                                    {                                        
                                        params.pk = new_pk_value;
                                    }

                                    //let the value of our data resolve before we send it off the server with
                                    //our own save method. This basically overrides the default save method
                                    //for xeditable                                     
                                    $timeout(function () {
                                        d.resolve();

                                        //make sure that we only allow saving on the change of an individual item
                                        // after we've saved the entire set once using the save button. This way
                                        // the user can fill out what they want before deciding to commit the row. 
                                        if (params.pk > 0) {
                                            //now save the object
                                            scope.$apply(scope.save);
                                        }
                                    }, 25);                                     
                                    return d.promise();                                                                   
                                }
                            }).on('hidden', function (e, reason) {
                                if (reason === 'save' || reason === 'nochange') {
                                   
                                    //find the next editable
                                    var currentIndex = $('.editable').index($(this));                                    
                                    var $next = $($('.editable')[currentIndex + 1]);
                                   
                                    //auto open the next editable                                    
                                    setTimeout(function () {
                                        $next.editable('show');
                                        //get the incomming editable popup
                                        var $edb = $(".editable-popup.fade.in input, .editable-popup.fade.in textarea, .editable-popup.fade.in select").first();
                                        $edb.focus();
                                        $edb.select();
                                    }, 100);                                    
                                }
                            }).on("focus", function () {
                                 setTimeout(function () {
                                     //get the incomming editable popup
                                     var $edb = $(".editable-popup.fade.in input, .editable-popup.fade.in textarea, .editable-popup.fade.in select").first();

                                     //make sure the popup textbox value matches the model data
                                     // in case we updated the value programmtically when we were running calculations
                                     $edb.val(ngModel.$viewValue);

                                     $edb.focus();
                                     $edb.select();
                                 }, 100);
                            }).on("shown", function () {
                                setTimeout(function () {
                                    var $edb = $(".editable-popup.fade.in input, .editable-popup.fade.in textarea, .editable-popup.fade.in select").first();

                                    var value = ngModel.$viewValue;

                                    // remove the colon from time entries when displaying the popup edit form
                                    if ((attrs.$attr.apmTime && value.length == 5) || (attrs.$attr.apmDatetime && value.length == 16)) {
                                        value = value.replace(/:/g, '');
                                    }                                
                                    
                                    // set a default value in the modal if our value is empty and we specified data-default
                                    if (value === '' && element.attr("data-default") !== '')
                                    {
                                        value = element.attr("data-default");
                                    }

                                    //make sure the popup textbox value matches the model data
                                    // in case we updated the value programmtically when we were running calculations
                                    $edb.val(value);

                                    $edb.focus();
                                    $edb.select();
                                }, 100);
                            })
                        }
                    }
                    $timeout(function () {
                        loadXeditable();
                    }, 10);
                }
            }
        })
        .directive('apmPrint', function () {                        
            function link(scope, element, attrs) {
                element.on('click', function () {
                    //remove the print section if it exists so we don't get duplicates
                    if ($("#printSection").length > 0) {
                        $("#printSection").remove();
                    }
                    var printSection = document.createElement('div');
                    printSection.id = 'printSection';
                    document.body.appendChild(printSection);

                    var elemToPrint = document.getElementById(attrs.printElementId);
                    if (elemToPrint) {
                        printElement(elemToPrint);
                    }
                });
                window.onafterprint = function () {
                    // clean the print section before adding new content
                    $("#printSection").remove();
                }
            }
            function printElement(elem) {
                // clones the element you want to print
                var domClone = elem.cloneNode(true);
                printSection.appendChild(domClone);
                window.print();
            }
            return {
                link: link,
                restrict: 'A'
            };
        })
        .directive('statusmessage', function () {
            return {
                restrict: 'E',
                templateUrl: 'statusmessage.html'
            }
        })
        .directive('apmTime', [function ($parse) {
            return {
                priority: 1000,
                compile: function (telement, attrs) {
                    attrs.$set('validate', true);
                    attrs.$set('data-pattern', '^([01]\\d|2[0-3]):?([0-5]\\d)$');
                    attrs.$set('data-validate-fail', 'Data format is HHMM');
                }
            }
        }])
        .directive('apmDatetime', [function ($parse) {
            return {
                priority: 1000,
                compile: function (telement, attrs) {                    
                    attrs.$set('validate', true);
                    attrs.$set('data-type', 'text');
                    attrs.$set('data-pattern', '^(\\s*|\\d{2}/\\d{2}/\\d{4} ([01]\\d|2[0-3]):?([0-5]\\d))$');
                    attrs.$set('data-validate-fail', 'Data format is MM/DD/YYYY HHMM');                    
                }
            }
        }])
        .directive('apmDate', [function ($parse) {
            return {
                priority: 1000,
                compile: function (telement, attrs) {
                    attrs.$set('validate', true);
                    attrs.$set('data-type', 'text');
                    attrs.$set('data-pattern', '^(\\s*|\\d{2}/\\d{2}/\\d{4})$');
                    attrs.$set('data-validate-fail', 'Data format is MM/DD/YYYY');
                }
            }
        }])
        .directive('apmNumber', [function ($parse) {
            return {
                priority: 1000,
                compile: function (telement, attrs) {
                    attrs.$set('validate', true);
                    attrs.$set('data-type', 'text');
                    attrs.$set('data-pattern', '^(\\s*|[-+]?\\d+(.\\d+)?)$');
                    attrs.$set('data-validate-fail', 'Data format is ##.##');
                }
            }
        }])
        .directive('apmHours', [function ($parse) {
            return {
                priority: 1000,
                compile: function (telement, attrs) {
                    attrs.$set('validate', true);
                    attrs.$set('data-type', 'text');
                    attrs.$set('data-pattern', '^(?:[0-9]|0[1-9]|1[0-9]|2[0-4])$'); //match 0-24
                    attrs.$set('data-validate-fail', 'Must be a number from 0-24');
                }
            }
        }])
        .directive('apmTripnumber', [function ($parse) {
            return {
                priority: 1000,
                compile: function (telement, attrs) {                                      
                    attrs.$set('validate', true);
                    attrs.$set('data-pattern', '^(\\s*|\\d{3}-\\d{4})$');
                    attrs.$set('data-validate-fail', 'Trip number format is ###-YYYY');
                }
            }
        }]);