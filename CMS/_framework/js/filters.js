'use strict';

/* Filters */

angular.module('apm.filters', ['apm.services'])
	.filter('interpolate', ['version', function(version) {
		return function(text) {
			return String(text).replace(/\%VERSION\%/mg, version);
		};
	}])
	.filter('moment', function () {
		return function(dateString, format) {
			return moment(dateString).format(format);
		};
	})
    .filter('viewTags', function () {
        return function (tags) {
            if (typeof tags === "object") {
                return tags.join(", ");
            }
            else {
                return tags
            }
        };
    })
    .filter('inArray', function () {
        return function (array, value) {
            return array.indexOf(value) !== -1;
        };
    })
    .filter('notInArray', function () {
        return function (array, value) {
            return array.indexOf(value) === -1;
        };
    });
        

