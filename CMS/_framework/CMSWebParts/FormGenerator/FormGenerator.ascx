<%@ Control Language="C#" AutoEventWireup="true" Inherits="_framework_CMSWebParts_FormGenerator_FormGenerator" CodeFile="FormGenerator.ascx.cs" %>

<div>
    <div><cms:LocalizedLabel ID="LocalizedLabel2" runat="server">Form:</cms:LocalizedLabel><cms:CMSDropDownList ID="ddlFormList" runat="server" /></div>
    <div><cms:LocalizedLabel runat="server">Date:</cms:LocalizedLabel><cms:DateTimePicker ID="dtDateTime" runat="server"></cms:DateTimePicker> </div>
    <div><cms:LocalizedLabel ID="LocalizedLabel1" runat="server">Trip Number:</cms:LocalizedLabel><asp:TextBox ID="tripNumber" runat="server"></asp:TextBox></div>
    <div><asp:Button runat="server" Text="Generate Form" ID="btnGenerateForm" OnClick="btnGenerateForm_Click" CausesValidation="false" ClientIDMode="Static" /></div>
</div>