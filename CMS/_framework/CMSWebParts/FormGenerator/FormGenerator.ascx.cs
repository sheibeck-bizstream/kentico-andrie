using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.IO;

using CMS.PortalControls;
using CMS.Helpers;
using System.Collections.Generic;
using OfficeOpenXml;

public partial class _framework_CMSWebParts_FormGenerator_FormGenerator : CMSAbstractWebPart
{

    #region "Constants"
    string FormPath = "~/App_Data/APM_Forms/";
    string FormPathOutput = "~/App_Data/APM_Forms_Output/";


    #endregion

    #region "Properties"



    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();

        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
        }
        else
        {
            if (!Page.IsPostBack)
            {
                //get a list of all the files in the Form folder and populate the drop down list
                List<string> formList = CMS.IO.Directory.GetFiles(Server.MapPath(FormPath), "*.xlsx").OrderBy(x => x).ToList();
                foreach (string form in formList)
                {
                    ddlFormList.Items.Add(new ListItem(Path.GetFileNameWithoutExtension(form).Replace("_", " "), form));
                }
            }
        }
    }


    /// <summary>
    /// Handles the Click1 event of the btnGenerateForm control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnGenerateForm_Click(object sender, EventArgs e)
    {
        FileInfo fi = new FileInfo(ddlFormList.SelectedValue);
        using (ExcelPackage pck = new ExcelPackage(fi))
        {
            //Create the worksheet
            var ws = pck.Workbook.Worksheets[1];

            //Find the named range
            ExcelNamedRangeCollection nrc = pck.Workbook.Names;

            foreach (ExcelNamedRange nr in nrc)
            {
                //go get the value we need to fill this named range
                string namedRangeValue = GetNamedRangeValue(nr.Name);

                //Set the value of each cell in the named range. 
                for (int rowIndex = nr.Start.Row; rowIndex <= nr.End.Row; rowIndex++)
                {
                    for (int columnIndex = nr.Start.Column; columnIndex <= nr.End.Column; columnIndex++)
                    {
                        ws.Cells[rowIndex, columnIndex].Value = namedRangeValue;
                    }
                }
            }

            //save the file out as a copy
            string sDate = CMS.Helpers.DateTimeHelper.DateTimeToString(DateTime.Now).Replace("/","").Split(' ')[0];
            FileInfo newFile = new FileInfo(String.Format("{0}{1}{2}{3}", Server.MapPath(FormPathOutput), Path.GetFileNameWithoutExtension(fi.FullName), sDate, Path.GetExtension(fi.FullName)));
            pck.SaveAs(newFile);
        }
    }

    private string GetNamedRangeValue(string p)
    {
        string v = String.Empty;

        switch (p)
        {
            case "BargeName":
            case "TugBargeName":
                v = "Karen-Andrie";
                break;
            case "TodayDate":
                v = DateTime.Now.ToShortDateString();
                break;
            case "CurrentTime":
                v = DateTime.Now.ToString("HHmm");
                break;
            default:
                v = String.Empty;
                break;
        }

        return v;
    }

    /// <summary>
    /// Reloads the control data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        SetupControl();
    }

    #endregion
    
}



