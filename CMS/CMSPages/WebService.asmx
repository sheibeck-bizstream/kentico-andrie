<%@ WebService Language="C#" Class="WebService" %>

using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Empty web service template.
/// </summary>
[WebService(Namespace = "http://www.andrieapm.com/", Description="APM WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class WebService : System.Web.Services.WebService
{
    /// <summary>
    /// Constructor.
    /// </summary>
    public WebService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod(MessageName = "GetUserPermissions")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public APM.UserPermissions GetUserPermissions(string className)
    {
        APM.UserPermissions permissions = new APM.UserPermissions();
        string siteName = CMS.Base.CMSActionContext.CurrentSite.SiteName;
        CMS.Membership.UserInfo user = (CMS.Membership.UserInfo)CMS.Base.CMSActionContext.CurrentUser;
        permissions.read = CMS.Membership.UserInfoProvider.IsAuthorizedPerClass(className, "Read", siteName, user, false);
        permissions.modify = CMS.Membership.UserInfoProvider.IsAuthorizedPerClass(className, "Modify", siteName, user, false);
        permissions.delete = CMS.Membership.UserInfoProvider.IsAuthorizedPerClass(className, "Delete", siteName, user, false);
        permissions.create = CMS.Membership.UserInfoProvider.IsAuthorizedPerClass(className, "Create", siteName, user, false);

        List<int> userRoleList = CMS.Membership.UserRoleInfoProvider.GetUserRoles().WhereEquals("UserID", CMS.Base.CMSActionContext.CurrentUser.UserID).Select( ur => ur.RoleID).ToList();
        permissions.role = CMS.Membership.RoleInfoProvider.GetRoles().WhereIn("RoleID", userRoleList).WhereContains("RoleName", "APM_").Select(r => r["RoleDisplayName"].ToString().Replace("* ", "")).ToList<string>().FirstOrDefault();

        return permissions;
    }

    [WebMethod(MessageName = "SetUserRole")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool SetUserRole(Guid userId, string roleName)
    {
        return APMHelper.SetUserRole(userId, roleName);
    }

    [WebMethod(MessageName = "GetCrewForDate")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<APM.CrewMember> GetCrewForDate(string date)
    {
        return APMHelper.GetCrewForDate(date);
    }

    [WebMethod(MessageName = "GetAllCrew")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<APM.CrewMember> GetAllCrew()
    {
        return APMHelper.GetAllCrew();
    }

    [WebMethod(MessageName = "GetCurrentUser")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetCurrentUser()
    {
        return APMHelper.GetCurrentUsername();
    }

    [WebMethod(MessageName = "ExportVesselForm")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public WebServiceResult ExportVesselForm(string formGuid, string tripNumber = "", string date = "", string id = "")
    {
        WebServiceResult wsr = new WebServiceResult();
        try
        {
            wsr.datalist = APM.VesselForm.Export(formGuid, tripNumber, date, id);
            wsr.success = true;
            wsr.message = "Vessel form saved.";
        }
        catch (Exception ex)
        {
            var msg = String.Format("There was a problem generating the form. {0}", ex.Message);

            wsr.datalist = new List<string>();
            wsr.message = msg;
            wsr.success = false;
        }

        return wsr;
    }
}