<%@ WebService Language="C#" Class="SyncWebService" %>

using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using System.Linq;
using System.Collections.Generic;
using CMS.CustomTables;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CMS.DataEngine;
using CMS.Helpers;
using CMS.EventLog;

/// <summary>
/// Empty web service template.
/// </summary>
[WebService(Namespace = "http://www.andrieapm.com/", Description="APM Sync WebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class SyncWebService : System.Web.Services.WebService
{
    /// <summary>
    /// Constructor.
    /// </summary>
    public SyncWebService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /// <summary>
    /// Upserts the specified item.
    /// </summary>
    /// <param name="classname">The classname.</param>
    /// <param name="row">The row.</param>
    /// <returns></returns>
    [WebMethod(MessageName = "Upsert")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public WebServiceResult Upsert(string className, string data, string type)
    {
        WebServiceResult wsr = new WebServiceResult();
        bool boolResult = false;
        bool isInsert = false;
        const string syncDateField = "PortSyncDateTime";
        var isDebug = SettingsKeyInfoProvider.GetBoolValue("APM_DebugSynchronization");
        
        try
        {            
            // Get information about the type of custom table item we're going to update/insert 
            DataClassInfo dci = DataClassInfoProvider.GetDataClassInfo(className);
                        
            //convert our object back to a dictionary object
            Dictionary<string, string> dcols = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(data);
            
            // Get the custom table item ID
            Guid itemGuid = CMS.Helpers.ValidationHelper.GetGuid(dcols["ItemGUID"], Guid.Empty);

            // see if this item exists already
            CustomTableItem cti = CMS.CustomTables.CustomTableItemProvider.GetItem(itemGuid, className);

            if (isDebug) EventLogProvider.LogInformation("SyncWebService", String.Format("BEGIN UPSERT {0} FROM {1}", className, type), String.Format("Syncing data from vessel: Object: {0}, ItemGuid: {1}", className, itemGuid));

            //make a new item if we didn't find one
            if (cti == null)
            {
                isInsert = true;
                cti = new CustomTableItem(className);
            }

            // get the class definition
            var doc = System.Xml.Linq.XDocument.Parse(dci.ClassFormDefinition);
            // get a list of all the tables properties by column value
            List<CTI> elements = doc.Descendants("field").Select( el => new CTI { colname = el.Attribute("column").Value, coltype = el.Attribute("columntype").Value }).ToList<CTI>();
            
            //update all the column values            
            foreach (CTI c in elements)
            {
                if (dcols.ContainsKey(c.colname))
                {
                    switch (c.coltype.ToLower())
                    {
                        case "integer":
                            int iv;
                            if (int.TryParse(dcols[c.colname], out iv))
                            {
                                cti.SetValue(c.colname, iv);
                            }
                            else
                            {
                                cti.SetValue(c.colname, null);
                            }
                            break;
                            
                        case "numeric":
                        case "double":
                            double nv;
                            if (double.TryParse(dcols[c.colname], out nv))
                            {
                                cti.SetValue(c.colname, nv);
                            }
                            else
                            {
                                cti.SetValue(c.colname, null);
                            }
                            break;

                        case "decimal":
                            decimal dv;
                            if (decimal.TryParse(dcols[c.colname], out dv))
                            {
                                cti.SetValue(c.colname, dv);
                            }
                            else
                            {
                                cti.SetValue(c.colname, null);
                            }
                            break;

                        case "guid":
                            Guid gv;
                            if (Guid.TryParse(dcols[c.colname], out gv))
                            {
                                cti.SetValue(c.colname, gv);
                            }
                            else
                            {
                                cti.SetValue(c.colname, null);
                            }
                            break;

                        case "datetime":
                            DateTime dtv;
                            if (DateTime.TryParse(dcols[c.colname], out dtv))
                            {
                                cti.SetValue(c.colname, dtv);
                            }
                            else
                            {
                                cti.SetValue(c.colname, null);
                            }
                            break;

                        case "boolean":
                            Boolean bv;
                            if(Boolean.TryParse(dcols[c.colname], out bv)){
                                cti.SetValue(c.colname, bv);                                
                            }
                            else{
                                cti.SetValue(c.colname, null);
                            }
                            break;

                        default:
                            cti.SetValue(c.colname, ValidationHelper.GetString(dcols[c.colname], String.Empty));
                            break;
                    }
                }
            }

            //update the port sync date time
            cti.SetValue(syncDateField, DateTime.Now);
                
            //save the data
            if (isInsert)
            {                
                cti.Insert();

                if (isDebug) EventLogProvider.LogInformation("SyncWebService", String.Format("INSERTED {0} FROM {1}", className, type), String.Format("Syncing data from vessel: Object: {0}, ItemGuid: {1}", className, itemGuid));
            }
            else
            {
                cti.Update();
                if (isDebug) EventLogProvider.LogInformation("SyncWebService", String.Format("UPDATED {0} FROM {1}", className, type), String.Format("Syncing data from vessel: Object: {0}, ItemGuid: {1}", className, itemGuid));
            }
            
            boolResult = true;
            wsr.message = "success";
        }
        catch(Exception ex)
        {
            boolResult = false;
            wsr.message = ex.Message;

            EventLogProvider.LogException("SyncWebService", "Upsert", ex, CMS.SiteProvider.SiteContext.CurrentSiteID, ex.StackTrace);
        }
        
        wsr.success = boolResult;
        return wsr;
    }	
    
    class CTI
    {
        public string colname { get; set; }
        public string coltype { get; set; }
    }
}