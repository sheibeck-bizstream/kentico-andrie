﻿ChatSupportWindow = function(opt) {
	var options = {
			roomId: 0,
			pnlChatRoomWindowClientId: '',
			pnlTopClientId: '',
			pnlBottomClientId: '',
			ChatRoomMessagesClientId: '',
			btnCloseClientId: '',
			notificationManagerOptions: null,
			title: '',
			isSupport: true
		};
	    
	$j.extend(options, opt);

	var $pnlChatRoomWindowTop = $j('#' + options.pnlChatRoomWindowClientId),
	    $divTop = $j('#' + options.pnlTopClientId),
	    $divBottom = $j('#' + options.pnlBottomClientId),
	    $sendMessageBtn = $j('.ChatSendAreaButton .SubmitButton'),
	    $txtAreaSendMessage = $j('.ChatSendAreaButton textarea'),
	    $messagesEnvelope = null,
	    messagesWebPart = null;

	$j(init);

	function init() {
		window.top.document.title = options.title;
		ChatManager.Login.IsSupporter = options.isSupport;
		ChatManager.IsPopupWindow = true;
		if ($sendMessageBtn && $txtAreaSendMessage) {
		    $sendMessageBtn.parent().width($sendMessageBtn.outerWidth(true));
		}
		$j('#' + options.btnCloseClientId).click(function() {
			if (ChatManager.ChatGroupManagers[0].RoomID > 0) {
				leaveRoom(leaveConversation);
			} else {
				leaveConversation();
			}

			return false;
		});

		if (!options.isSupport && !$j.browser.msie) {
			// Support room is not leaved when window is closed!
			$j(window).bind("beforeunload", function (event) {
				if (ChatManager.ChatGroupManagers[0].RoomID > 0) {
					leaveRoom();
				}
			});
		}

		if (ChatNotificationManagerSetup) {
			ChatNotificationManagerSetup(options.notificationManagerOptions);
		}
		
		$pnlChatRoomWindowTop.resize(recalculatePositions);
		$divTop.resize(recalculatePositions);
		$divBottom.resize(recalculatePositions);
		for (var i = 0; i < ChatManager.ChatGroupManagers.length; i++) {
			ChatManager.ChatGroupManagers[i].JoinRoom(options.roomId);
		}
	}

	function leaveConversation() {
		if (options.isSupport) {
			window.opener.ChatSupportManager.leaveSupportRoom(options.roomId, closeWindow);
		} else {
			closeWindow();
		}
	}

	function closeWindow() {
		if ($j.browser.safari) {
			setTimeout(window.close, 300);
		}
		else {
			window.close();
		}
	}

	function leaveRoom(fnOk) {
		for (var i = 0; i < ChatManager.ChatGroupManagers.length; i++) {
			ChatManager.ChatGroupManagers[i].LeaveRoom(fnOk);
		}
	}

	function recalculatePositions() {
		if ($messagesEnvelope == null) {
			messagesWebPart = ChatManager.GetWebpart(null, options.ChatRoomMessagesClientId, "ChatMessages", true);
			if (messagesWebPart) {
				$messagesEnvelope = messagesWebPart.GetEnvelope();
			}
		}
		if ($messagesEnvelope) {
			$messagesEnvelope.offset({ top: $pnlChatRoomWindowTop.outerHeight(true) + $divTop.position().top });
			$messagesEnvelope.height($divBottom.position().top - ($divTop.position().top + $pnlChatRoomWindowTop.outerHeight(true)));
			messagesWebPart.AdjustScrollbar();
		}
	}
};