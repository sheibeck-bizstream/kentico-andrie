<%@ Page Title="" Language="C#" MasterPageFile="~/CMSAPIExamples/Pages/APIExamplesPage.Master"
    Theme="Default" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CMSAPIExamples_Code_Documents_Security_Default" %>

<%@ Register Src="~/CMSAPIExamples/Controls/APIExample.ascx" TagName="APIExample" TagPrefix="cms" %>
<asp:Content ID="contentLeft" ContentPlaceHolderID="plcLeftContainer" runat="server">
    <%-- Creating documents --%>
    <cms:LocalizedHeading ID="headCreateDocument" runat="server" Text="Creating documents" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiCreateDocumentStructure" runat="server" ButtonText="Create document structure" InfoMessage="Document structure for the API example created successfully." ErrorMessage="Site root node not found." />
    <%-- Setting document level permissions --%>
    <cms:LocalizedHeading ID="headSetPermissions" runat="server" Text="Setting document level permissions" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiSetUserPermissions" runat="server" ButtonText="Set user permissions" InfoMessage="The 'Modify permissions' permission for document 'API Example' was successfully granted to user 'Andy'." ErrorMessage="Document 'API Example' or user 'Andy' not found." />
    <cms:APIExample ID="apiSetRolePermissions" runat="server" ButtonText="Set role permissions" InfoMessage="The 'Modify' permission for document 'API Example' was successfully granted to role 'CMSDeskAdministrator'." ErrorMessage="Document 'API Example' or role 'CMSDeskAdministrator' not found." />
    <%-- Permission inheritance --%>
    <cms:LocalizedHeading ID="headPermissionInheritance" runat="server" Text="Permission inheritance" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiBreakPermissionInheritance" runat="server" APIExampleType="ManageAdditional" ButtonText="Break inheritance" InfoMessage="Inheritance of permissions on document 'API Example subpage' broken successfully." ErrorMessage="Document 'API Example subpage' not found." />
    <cms:APIExample ID="apiRestorePermissionInheritance" runat="server" APIExampleType="ManageAdditional" ButtonText="Restore inheritance" InfoMessage="Inheritance of permissions on document 'API Example subpage' restored successfully." ErrorMessage="Document 'API Example subpage' not found." />
    <%-- Checking permissions --%>
    <cms:LocalizedHeading ID="headCheckPermissions" runat="server" Text="Checking permissions" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiCheckContentModulePermissions" runat="server" ButtonText="Check module permissions" APIExampleType="ManageAdditional" InfoMessage="User 'Andy' is allowed to read module 'Content'." ErrorMessage="User 'Andy' not found." />
    <cms:APIExample ID="apiCheckDocTypePermissions" runat="server" ButtonText="Check document type permissions" APIExampleType="ManageAdditional" InfoMessage="User 'Andy' is allowed to read document type 'Menu item'." ErrorMessage="User 'Andy' not found." />
    <cms:APIExample ID="apiCheckDocumentPermissions" runat="server" ButtonText="Check document permissions" APIExampleType="ManageAdditional" InfoMessage="User 'Andy' is allowed to modify permissions for document 'API Example'." ErrorMessage="Document 'API Example' or user 'Andy' not found." />
    <cms:APIExample ID="apiFilterDataSet" runat="server" ButtonText="Filter data set" APIExampleType="ManageAdditional" InfoMessage="Data set with all documents filtered successfully by permission 'Modify permissions' for user 'Andy'. Permission inheritance broken for filtered items." ErrorMessage="User 'Andy' not found." />
</asp:Content>
<asp:Content ID="contentRight" ContentPlaceHolderID="plcRightContainer" runat="server">
    <%-- Deleting document level permissions --%>
    <cms:LocalizedHeading ID="headDelelePermissions" runat="server" Text="Deleting document level permissions" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiDeletePermissions" runat="server" APIExampleType="CleanUpMain" ButtonText="Delete permissions" InfoMessage="The document level permissions deleted successfully." ErrorMessage="The document structure not found." />
    <%-- Deleting documents --%>
    <cms:LocalizedHeading ID="headDeleleDocument" runat="server" Text="Deleting documents" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiDeleteDocumentStructure" runat="server" APIExampleType="CleanUpMain" ButtonText="Delete document structure" InfoMessage="The document structure deleted successfully." ErrorMessage="The document structure not found." />
</asp:Content>
