<%@ Page Title="" Language="C#" MasterPageFile="~/CMSAPIExamples/Pages/APIExamplesPage.Master"
    Theme="Default" AutoEventWireup="true" Inherits="CMSAPIExamples_Code_Documents_Advanced_Default" CodeFile="Default.aspx.cs" %>

<%@ Register Src="~/CMSAPIExamples/Controls/APIExample.ascx" TagName="APIExample" TagPrefix="cms" %>
<asp:Content ID="contentLeft" ContentPlaceHolderID="plcLeftContainer" runat="server">
    <%-- Example preparation --%>
    <cms:LocalizedHeading ID="headPreparation" runat="server" Text="Example preparation" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiCreateDocumentStructure" runat="server" ButtonText="Create document structure" InfoMessage="Documents prepared successfully." ErrorMessage="Site root node not found." />
    <%-- Organizing docuemnts --%>
    <cms:LocalizedHeading ID="headCreateDocument" runat="server" Text="Organizing documents" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiMoveDocumentUp" runat="server" APIExampleType="ManageAdditional" ButtonText="Move document up" InfoMessage="Document moved up." ErrorMessage="Document not found." />
    <cms:APIExample ID="apiMoveDocumentDown" runat="server" APIExampleType="ManageAdditional" ButtonText="Move document down" InfoMessage="Document moved down." ErrorMessage="Document not found." />
    <cms:APIExample ID="apiSortDocumentsAlphabetically" runat="server" APIExampleType="ManageAdditional" ButtonText="Sort documents alphabetically" InfoMessage="Documents sorted from A to Z." ErrorMessage="API Example folder not found." />
    <cms:APIExample ID="apiSortDocumentsByDate" runat="server" APIExampleType="ManageAdditional" ButtonText="Sort documents by date" InfoMessage="Documents sorted from oldest to newest." ErrorMessage="API Example folder not found." />
    <%-- Recycle bin --%>
    <cms:LocalizedHeading ID="headRecycleBin" runat="server" Text="Recycle bin" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiMoveToRecycleBin" runat="server" ButtonText="Move document to recycle bin" InfoMessage="Document moved to the recycle bin." ErrorMessage="Document not found." />
    <cms:APIExample ID="apiRestoreFromRecycleBin" runat="server" ButtonText="Restore document" InfoMessage="Document restored successfully." ErrorMessage="Document not found in the recycle bin." />
</asp:Content>
<asp:Content ID="contentRight" ContentPlaceHolderID="plcRightContainer" runat="server">
    <%-- Deleting documents --%>
    <cms:LocalizedHeading ID="pnlDeleleDocumentStructure" runat="server" Text="Document structure" Level="4" EnableViewState="false" />
    <cms:APIExample ID="apiDeleteDocumentStructure" runat="server" ButtonText="Delete document structure" APIExampleType="CleanUpMain" InfoMessage="Document structure successfully deleted." />
</asp:Content>
