//Install dependencies command
//npm install --save-dev gulp gulp-util gulp-jshint gulp-uglify gulp-browserify gulp-rename gulp-concat gulp-angular-templatecache gulp-minify-html gulp-rimraf

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    browserify = require('gulp-browserify'),
	rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    templates = require('gulp-angular-templatecache'),
    minifyHTML = require('gulp-minify-html'),
    rimraf = require('gulp-rimraf');

var basedir = "./CMS/_framework";
var distdir = "./CMS/_framework/dist";
var tempdir = "./CMS/_framework/tmp";

// JSHint task
gulp.task('lint', function() {  
  //gulp.src(basedir + '/js/app/**/*.js')
  //.pipe(jshint())
  // You can look into pretty reporters as well, but that's another story
  //.pipe(jshint.reporter('default'));
});

// Templates
gulp.task('templates', function () {
    // Any other view files from partials
    return gulp.src(basedir + '/partials/**/*')
        .pipe(minifyHTML({
            quotes: true
        }))
        .pipe(templates('templates.js', { module: 'apm.templates', standalone: true }))
        // Will be put in the dist/partials folder
        .pipe(gulp.dest(tempdir));
});

// Browserify task
gulp.task('browserify', function() {
  // Single point of entry (make sure not to src ALL your files, browserify will figure it out for you)
    return gulp.src([basedir + '/js/app.js'], { read: false })
      .pipe(browserify({
          insertGlobals: true,
          debug: true,
          shim: {
              'jquery': { path: './CMS/_framework/js/lib/jquery-1.10.2.min.js', exports: '$' },
              'angular': { path: './CMS/_framework/js/lib/angular.js', exports: 'angular' },
              'moment': { path: './CMS/_framework/js/lib/moment.min.js', exports: 'moment' },
              'xml2json': { path: './CMS/_framework/js/lib/xml2json.js', exports: 'xml2json' },
              'angularRoute': { path: './CMS/_framework/js/lib/angular-route.min.js', exports: 'ngRoute', depends: { angular: 'angular' } },
              'angularResource': { path: './CMS/_framework/js/lib/angular-resource.min.js', exports: 'ngResource', depends: { angular: 'angular' } },
              'angularCookies': { path: './CMS/_framework/js/lib/angular-cookies.min.js', exports: 'ngCookies', depends: { angular: 'angular' } },
              'angularSanitize': { path: './CMS/_framework/js/lib/angular-sanitize.min.js', exports: 'ngSanitize', depends: { angular: 'angular' } },
              'bootstrap': { path: './CMS/_framework/js/lib/bootstrap.min.js', exports: 'bootstrap', depends: { angular: 'angular', jquery: 'jquery' } },
              'ui.bootstrap.datetimepicker': { path: './CMS/_framework/js/lib/bootstrap-datetimepicker.min.js', exports: null, depends: { angular: 'angular' } },
              'ui.bootstrap': { path: './CMS/_framework/js/lib/ui-bootstrap-tpls-0.11.0.min.js', exports: 'uiBootstrap', depends: { angular: 'angular' } },              
              'bootstrap.editable': { path: './CMS/_framework/js/lib/bootstrap-editable.js', exports: null, depends: { bootstrap: 'bootstrap', moment: 'moment' } },
              'select2': { path: './CMS/_framework/js/lib/select2.js', exports: 'select2' }
          }
      }))
      // Bundle to a single file
      .pipe(concat('app.js'))
      .pipe(gulp.dest(tempdir))
});

//combine files into our final output
gulp.task('combine', ['templates', 'browserify'], function (cb) {   
    return gulp.src([tempdir + '/*.js'])    
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest(distdir + '/js'))
    .pipe(uglify({ mangle: false }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(distdir + '/js'));    
});

gulp.task('default', ['lint', 'combine'], function () {
  // Watch our scripts
  gulp.watch([basedir+'/js/**/*.js'],[
    'lint',    
    'combine'
  ]);
  
  gulp.watch([basedir+'/partials/*.html'], [	
    'combine'
  ]);
});

