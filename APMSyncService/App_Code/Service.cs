﻿using CMS.CustomTables;
using CMS.DataEngine;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml.Linq;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service" in code, svc and config file together.
public class Service : IService
{

    /// <summary>
    /// Upserts the specified item.
    /// </summary>
    /// <param name="classname">The classname.</param>
    /// <param name="row">The row.</param>
    /// <returns></returns>
    public bool Upsert(string className, string customTableItem)
	{        
        bool boolResult = false;

        if (String.IsNullOrEmpty(className))
        {
            throw new Exception("Class Name cannot be null.");
        }
        else
        {
            Type ctt = CMS.CustomTables.CustomTableItemProvider.GetTypeInfo(className).GetType();
            object cto = Newtonsoft.Json.JsonConvert.DeserializeObject(customTableItem, ctt);
            CustomTableItem newCustomTableItem = (CustomTableItem)cto;

            //determine if this custom table item exists, if not then insert a new item
            CustomTableItem existingItem = CustomTableItemProvider.GetItem(newCustomTableItem.ItemGUID, className);
            if (existingItem == null)
            {
                newCustomTableItem.Insert();
            }
            else // update the existing item
            {
                newCustomTableItem.Update();
            }

            boolResult = true;
        }

        return boolResult;
	}	
}
