DECLARE @MyCursor CURSOR;
DECLARE @ItemID INT;
DECLARE @IsDebug BIT;

SET @IsDebug = 1;

BEGIN TRAN


--Tug Only
BEGIN
    SET @MyCursor = CURSOR FOR
    select top 1000 ItemID from apm_fosoundings
        WHERE ([FOSoundingsTugConsumed] != 0 AND [FOSoundingsBargeConsumed] = 0) OR ([FOSoundingsTugOnboard] != 0 AND [FOSoundingsBargeOnboard] = 0)
		Order By FOSoundingsDateTime

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @ItemID

    WHILE @@FETCH_STATUS = 0
    BEGIN
	   Update apm_fosoundings
		Set [ItemModifiedWhen] = GetDate()
			,[BoatSyncDateTime] = NULL
			,[PortSyncDateTime] = NULL
			,[SPSyncDateTime] = NULL
			,[FOSoundingsVesselConsumed] = [FOSoundingsTugConsumed]
			,[FOSoundingsVessel] = 'Tug'
			,[FOSoundingsVesselOnboard] = [FOSoundingsTugOnboard]
		WHERE ItemID = @ItemID
   
	     FETCH NEXT FROM @MyCursor 
      INTO @ItemID 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;
END;

--Barge Only
BEGIN
    SET @MyCursor = CURSOR FOR
    select top 1000 ItemID from apm_fosoundings
        WHERE ([FOSoundingsTugConsumed] = 0 AND [FOSoundingsBargeConsumed] != 0) OR ([FOSoundingsTugOnboard] = 0 AND [FOSoundingsBargeOnboard] != 0)
		Order By FOSoundingsDateTime

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @ItemID

    WHILE @@FETCH_STATUS = 0
    BEGIN
    	
		--update barge
		Update apm_fosoundings
		Set [ItemModifiedWhen] = GetDate()
			,[BoatSyncDateTime] = NULL
			,[PortSyncDateTime] = NULL
			,[SPSyncDateTime] = NULL
			,[FOSoundingsVesselConsumed] = [FOSoundingsBargeConsumed]
			,[FOSoundingsVessel] = 'Barge'
			,[FOSoundingsVesselOnboard] = [FOSoundingsBargeOnboard]
		WHERE ItemID = @ItemID

      FETCH NEXT FROM @MyCursor 
      INTO @ItemID 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;
END;

--Barge AND Tug
BEGIN
    SET @MyCursor = CURSOR FOR
    select top 1000 ItemID from apm_fosoundings
        WHERE ([FOSoundingsTugConsumed] != 0 AND [FOSoundingsBargeConsumed] != 0) OR ([FOSoundingsTugOnboard] != 0 AND [FOSoundingsBargeOnboard] != 0)
		Order By FOSoundingsDateTime

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @ItemID

    WHILE @@FETCH_STATUS = 0
    BEGIN
    	
		--convert this row to a barge entry
		Update apm_fosoundings
		Set [ItemModifiedWhen] = GetDate()
			,[BoatSyncDateTime] = NULL
			,[PortSyncDateTime] = NULL
			,[SPSyncDateTime] = NULL
			,[FOSoundingsVesselConsumed] = [FOSoundingsBargeConsumed]
			,[FOSoundingsVessel] = 'Barge'
			,[FOSoundingsVesselOnboard] = [FOSoundingsBargeOnboard]
		WHERE ItemID = @ItemID

		-- insert new tug entry
		INSERT INTO apm_fosoundings
		Select
		  [ItemCreatedBy]
		  ,[ItemCreatedWhen]
		  ,[ItemModifiedBy]
		  ,GetDate() as [ItemModifiedWhen]
		  ,[ItemOrder]
		  ,NewID() as [ItemGUID]
		  ,[TugNumber]
		  ,[BargeNumber]
		  ,[TripNumber]
		  ,[FOSoundingsDateTime]
		  ,[FOSoundingsLocation]
		  ,NULL as [BoatSyncDateTime]
		  ,NULL as [PortSyncDateTime]
		  ,NULL as [SPSyncDateTime]
		  ,[ModifiedByName]
		  ,[FOSoundingsTugConsumed]
		  ,0 as [FOSoundingsBargeConsumed]
		  ,[FOSoundingsTotalConsumed]
		  ,[FOSoundingsTugOnboard]
		  ,0 as [FOSoundingsBargeOnboard]
		  ,[FOSoundingsTotalOnboard]
		  ,[IsDeleted]
		  ,[FOSoundingsTugConsumed] as [FOSoundingsVesselConsumed]
		  ,'Tug' as [FOSoundingsVessel]
		  ,[FOSoundingsTugOnboard] as [FOSoundingsVesselOnboard]
		  FROM apm_fosoundings
	   WHERE ItemID = @ItemID

      FETCH NEXT FROM @MyCursor 
      INTO @ItemID 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;
END;

Select ItemID, FOSoundingsDateTime
	,FOSoundingsTugConsumed, FOSoundingsTugOnboard
	,FOSoundingsBargeConsumed, FOSoundingsBargeOnboard
	,FOSoundingsVessel, FOSoundingsVesselConsumed, FOSoundingsVesselOnboard
From APM_FOSoundings
Order By FOSoundingsDateTime


IF @IsDebug = 1
BEGIN
	ROLLBACK TRAN
END

IF @IsDebug != 1
BEGIN
	COMMIT TRAN
END

