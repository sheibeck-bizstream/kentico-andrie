Select ClassTableName From CMS_Class
WHERE ClassName like 'APM.%' and ClassDisplayName NOT LIKE 'LT %' AND ClassIsDocumentType = 0
ORDER BY ClassDisplayName

Delete FROM APM_BallastLog
Delete FROM APM_Cargo
Delete FROM APM_CargoDockDraft
Delete FROM APM_CargoDelay
Delete FROM APM_CargoDockTime
Delete FROM APM_CargoGeneralInformation
Delete FROM APM_CargoWaterGauge
Delete FROM APM_CrewChange
Delete FROM APM_CrewPayroll
Delete FROM APM_CurrentCrew
Delete FROM APM_DeckLog
Delete FROM APM_DeckLogEntry
Delete FROM APM_DirtyFilter
Delete FROM APM_Downtime
Delete FROM APM_FCCOperatorLog
Delete FROM APM_FOGallonsTransferred
Delete FROM APM_FOSoundings
Delete FROM APM_GarbageLog
Delete FROM APM_GreyWater
Delete FROM APM_LastFOBunker
Delete FROM APM_LOBunker
Delete FROM APM_LubeOilUsage
Delete FROM APM_MonthlyFuel
Delete FROM APM_MonthlyLubeOil
Delete FROM APM_MorningReport
Delete FROM APM_NearMiss
Delete FROM APM_NextCrewChange
Delete FROM APM_SafetyMeeting
Delete FROM APM_Safety
Delete FROM APM_SlopDump
Delete FROM APM_SlopTankSounding
Delete FROM APM_TripInformation
Delete FROM APM_VesselInformation
Delete FROM APM_VisualMonitoring
